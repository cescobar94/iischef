﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iischef.core.Php
{
    public class IniFileNamespace
    {
        public IniFileNamespace(string Value)
        {
            this.Hostname = Value;
        }

        /// <summary>
        /// Valor del namespace.
        /// </summary>
        public string Hostname { get; set; }

        public Dictionary<string, IniFileSection> Sections = new Dictionary<string, IniFileSection>(StringComparer.InvariantCultureIgnoreCase);
    }
}
