﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Win32.TaskScheduler;
using Microsoft.Web.Administration;
using System.IO;
using System.Runtime;
using System.Reflection;
using iischef.utils;

namespace iischef.core.Services
{
    public class ScheduleService : DeployerBase, DeployerInterface
    {
        protected const string CST_TASK_FOLDER_NAME = "\\Chef";

        public void start() { }

        public void stop()
        {
            var settings = this.DeployerSettings.castTo<ScheduleServiceSettings>();

            string cronId = Deployment.installedApplicationSettings.getId() + "_" + settings.id;

            using (TaskService ts = new TaskService())
            {
                stopTask(ts, cronId);
            }
        }

        protected void stopTask(TaskService ts, string name)
        {
            var f = getFolder(ts);

            try
            {
                // If a task with the same name already exists, stop it!
                var ot = ts.GetTask(f.Path + "\\" + name);
                if (ot != null)
                {
                    ot.Stop();
                }
            }
            catch { }
        }

        protected void startTask(TaskService ts, string name)
        {
            var f = getFolder(ts);

            try
            {
                // If a task with the same name already exists, stop it!
                var ot = ts.GetTask(f.Path + "\\" + name);
                if (ot != null)
                {
                    ot.Run();
                }
            }
            catch { }
        }


        public void deploySettings(string jsonSettings,
            string jsonSettingsNested,
            RuntimeSettingsReplacer replacer)
        {

        }

        public void undeploy(bool isUninstall = false)
        {

            if (!isUninstall)
            {
                return;
            }

            var settings = this.DeployerSettings.castTo<ScheduleServiceSettings>();

            string cronId = Deployment.installedApplicationSettings.getId() + "_" + settings.id;

            using (TaskService ts = new TaskService())
            {
                var f = getFolder(ts);
                f.DeleteTask(cronId, false);
            }
        }

        protected TaskFolder getFolder(TaskService ts)
        {
            TaskFolder f = null;

            try
            {
                f = ts.GetFolder(CST_TASK_FOLDER_NAME);
            }
            catch
            {

            }

            if (f == null)
            {
                f = ts.RootFolder.CreateFolder(CST_TASK_FOLDER_NAME);
            }

            return f;
        }

        public void deploy()
        {
            var settings = this.DeployerSettings.castTo<ScheduleServiceSettings>();

            string cronId = Deployment.installedApplicationSettings.getId() + "_" + settings.id;

            string pwfile = UtilsSystem.CombinePaths(Deployment.runtimePath, "cronjobs_" + settings.id + ".ps1");

            // Necesitamos un Bat que llame al powershel, este siempre tiene el mismo aspecto.
            string batfile = UtilsSystem.CombinePaths(Deployment.runtimePath, "cronjobs_" + settings.id + ".bat");
            Encoding enc = Encoding.GetEncoding("Windows-1252");
            File.WriteAllText(batfile, "powershell " + pwfile, enc);

            StringBuilder command = new StringBuilder();

            // Add path to environment.
            command.AppendLine(string.Format("$env:Path = \"{0};\" + $env:Path", UtilsSystem.CombinePaths(Deployment.runtimePath, "include_path")));

            // Move to runtime.
            command.AppendLine(string.Format("cd \"{0}\"", UtilsSystem.CombinePaths(Deployment.appPath)));

            // Whatever deployers wanna do...
            var logger = new logger.NullLogger();
            var deployers = Deployment.grabDeployers(logger);
            foreach (var deployer in deployers)
            {
                deployer.deployConsoleEnvironment(command);
            }

            // Drop the user commands
            if (!string.IsNullOrWhiteSpace(settings.command))
            {
                command.AppendLine(settings.command);
            }

            if (settings.commands != null)
            {
                foreach (var cmd in settings.commands)
                {
                    command.AppendLine(cmd);
                }
            }

            File.WriteAllText(pwfile, command.ToString());

            // Nuestro scheduler tiene un nombre
            // definido.
            using (TaskService ts = new TaskService())
            {
                // Create a new task definition and assign properties
                TaskDefinition td = ts.NewTask();

                // Run with highest level to avoid UAC issues
                // https://www.devopsonwindows.com/create-scheduled-task/
                td.Principal.RunLevel = TaskRunLevel.Highest;

                string password = settings.taskUserPassword;

                if (settings.taskLogonType.HasValue)
                {
                    td.Principal.LogonType = (TaskLogonType)settings.taskLogonType.Value;
                }

                if (settings.taskUserId == "auto")
                {
                    td.Principal.UserId = Deployment.windowsUsername;
                    td.Principal.LogonType = TaskLogonType.Password;
                    password = Deployment.getWindowsPassword();
                    // Make sure that the user has the LogonAsBatchRight
                    UtilsWindowsAccounts.SetRight(Deployment.windowsUsername, "SeBatchLogonRight");
                }
                // Default to the SYSTEM account.
                else if (String.IsNullOrWhiteSpace(settings.taskUserId))
                {
                    td.Principal.UserId = "SYSTEM";
                    td.Principal.LogonType = TaskLogonType.ServiceAccount;
                    password = null;
                }

                td.RegistrationInfo.Description = cronId;

                // Create a trigger that will fire the task every 5 minutes.
                var trigger = new DailyTrigger();

                // Habilitada...
                trigger.Enabled = true;

                // Repetir cada 24 horas.
                trigger.DaysInterval = 1;

                // Repetir durante 24 horas en la frecuencia establecida.
                trigger.Repetition = new RepetitionPattern(new TimeSpan(0, settings.frequency, 0), new TimeSpan(24, 0, 0), true);

                // Para que arranque dos minutos después del deploy.
                trigger.StartBoundary = DateTime.Now.AddMinutes(2);

                // Un solo trigger.
                td.Triggers.Add(trigger);

                // Create an action that will launch the bat launcher.
                td.Actions.Add(new ExecAction(batfile, null, null));

                TaskFolder f = getFolder(ts);

                stopTask(ts, cronId);

                // Register the task in the root folder
                if (!string.IsNullOrWhiteSpace(password) && td.Principal.LogonType == TaskLogonType.Password)
                {
                    f.RegisterTaskDefinition(td.RegistrationInfo.Description, td, TaskCreation.CreateOrUpdate, td.Principal.UserId, Deployment.getWindowsPassword(), td.Principal.LogonType);
                }
                else
                {
                    f.RegisterTaskDefinition(td.RegistrationInfo.Description, td, TaskCreation.CreateOrUpdate, td.Principal.UserId);
                }
            }
        }

        public void sync()
        {

        }
    }
}
