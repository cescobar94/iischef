﻿namespace iischef.utils
{
    public class HashCode
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static unsafe int GetString32(string s)
        {
            fixed (char* str = s)
            {
                char* chPtr = str;
                int num = 0x15051505;
                int num2 = num;
                int* numPtr = (int*)chPtr;
                for (int i = s.Length; i > 0; i -= 4)
                {
                    num = (((num << 5) + num) + (num >> 0x1b)) ^ numPtr[0];
                    if (i <= 2)
                    {
                        break;
                    }
                    num2 = (((num2 << 5) + num2) + (num2 >> 0x1b)) ^ numPtr[1];
                    numPtr += 2;
                }
                return (num + (num2 * 0x5d588b65));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static unsafe int GetString64(string s)
        {
            fixed (char* str = s)
            {
                int num3;
                char* chPtr = str;
                int num = 0x1505;
                int num2 = num;
                for (char* chPtr2 = chPtr; (num3 = chPtr2[0]) != '\0'; chPtr2 += 2)
                {
                    num = ((num << 5) + num) ^ num3;
                    num3 = chPtr2[1];
                    if (num3 == 0)
                    {
                        break;
                    }
                    num2 = ((num2 << 5) + num2) ^ num3;
                }
                return (num + (num2 * 0x5d588b65));
            }
        }
    }
}
