﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iischef.core.Php
{
    public enum IniFileLineType
    {
        Directive = 0,
        Comment = 1,
        Blank = 2
    }
}
