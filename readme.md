# Windows Chef
## What it is

Windows chef is a set of tools that can be run either through command line (powershell cmdlet) or as a windows service that automates the process of deploying applications based on descriptive yml files that are part of the application itself.

Think of this of a cheap self-hosted version of the "infrastructure as code" trend, but it only deals with infrastructure configuration at the application level, not with setting up the infrastructure itself.

You can use this tool to automate application deployment in production environments as well as local development to ensure that the application is deployed in exactly the same way in both places.

It is designed to deploy applications in high density multi-tenant (Shared) resources with as much isolation as possible. So you can host as many applications as you want on a single web server sharing the same IIS, SQL Server and other resources.

The tool can also be used to automate modern devops practices such as "environment-per-branch".

## Is this tool helpful now that Windows Containers are a deployment trend?

Sure. This tool can automate the process of deploying your application inside the container and setting up any required configuration.

## How to get it

You can build the project yourself, or download the ready-to-go binaries from the continuous integration in AppVeyor:

https://ci.appveyor.com/project/David19767/iischef/history

The latest version of the artifacts is always available at the following permalinks

Cmdlet: https://ci.appveyor.com/api/projects/David19767/iischef/artifacts/iischef.cmdlet.zip?branch=1.x

Service: https://ci.appveyor.com/api/projects/David19767/iischef/artifacts/iischef.service.zip?branch=1.x

## Using the console

### Download, extract and import the CMDLET

Use the following commands to make install the Chef cmdlets system wide:

```
$WorkingDir = Convert-Path .
$ZipPath = Join-Path $WorkingDir '\chef_cmdlet.zip'
(New-Object Net.WebClient).DownloadFile('https://ci.appveyor.com/api/projects/David19767/iischef/artifacts/iischef.cmdlet.zip?branch=1.x', $ZipPath)
$DESTINATION= Join-Path $env:ProgramFiles "\WindowsPowerShell\Modules\Chef"
New-Item -ItemType directory -Force -Path $DESTINATION
(new-object -com shell.application).namespace($DESTINATION).CopyHere((new-object -com shell.application).namespace($ZipPath).Items(),16)
Remove-Item $ZipPath
```

an alternate solution:

```
powershell -command "(New-Object Net.WebClient).DownloadFile('https://ci.appveyor.com/api/projects/David19767/iischef/artifacts/iischef.cmdlet.zip?branch=1.x','%CD%\chef_cmdlet.zip')"
set DESTINATION=%ProgramFiles%\WindowsPowerShell\Modules\Chef
mkdir "%DESTINATION%"
powershell -command "(new-object -com shell.application).namespace('%DESTINATION%').CopyHere((new-object -com shell.application).namespace('%CD%\chef_cmdlet.zip').Items(),16)"
del %CD%\chef_cmdlet.zip
```

### Setting the environment file

The first thing you need to do is to tell Chef what are the available services (storage, database servers, etc.) in the current system.

To do so you use a json formatted file that describes your system. You can see an example/reference implementation in:

https://bitbucket.org/david_garcia_garcia/iischef/raw/e57929cbf540d56d6bd76cd62c510debc15f77c5/iischef.tests/samples/server-settings.json

You can place this file any place you like in your system.

To globally install this configuration file, use the following command:

`Set-ChefEnvironmentFile c:\chef\server-settings.json`

### Adding deployment information to your application

The next step is to add to your application information regarding "what" services it needs to run. I.e. to run a Drupal site you will need a
database backend, temporary, permanent and private disk storage, a caching backend such as couchbase, an site in iis, etc..

To describe this setup you create a folder in your application root called "chef". Inside this folder you can place several configuration files
that will be applied by chef depending on the environment, the files in this directory are combined and aggregated into a final 
configuration file according to the following rules:

* Only files that match the following pattern are considered as settings: chef*.yml
* The [scope-environment-regex, scope-branch-regex] are checked against current environment, they all must match for the configuration file to be considedred. (Note that they default to .* that means match all)
* Configuration files are sorted according to the scope-weight attribute
* Configuration files are merged into a single chef.yml file, that will be used to configure the currrent deployment.

There is a full reference sample file here:

[Chef Reference Application File](https://bitbucket.org/david_garcia_garcia/iischef/src/1.x/iischef.core/sample.yml?at=1.x&fileviewer=file-view-default)

### Installing an application

To deploy an application on a system you "install" the application, that is, provide the necessary details to chef so that it can obtain the required artifacts for deployment.

Currently Chef only support deploying artifacts from Appveyor or the Filesystem (a local folder).

To install an application from a path you can use the Invoke-ChefAppDeployPath command:

`Invoke-ChefAppDeployPath c:\applications\mycode myapplicationname`

Once installed, Chef internally stores the information of "where to grab" the artifact from and you can directly use other commands to interact with your application.

For example, to re-deploy an application (if you made any changes to the chef.yml settings file) you can use:

`Invoke-ChefAppRedeploy myapplicationname`

The previous example is a shortcut to deploy applications from local paths (mostly used in development environments).

The way to properly deploy an application is to create an application definition yml file and register it.

Example for an application using a local deployer (local path):

```
id: 'php-test'
mount_strategy: '%MOUNTSTRATEGY%'
# Type of downloader/monitor to use,
# currently only appveyor
downloader:
# Currently only "appveyor" and "localpath" supported
 type: 'localpath'
 path: '%PATH%'
runtime_overrides:
 'deployment.custom.setting': 'my_custom_setting'
 'services.contents.mount.files.path': 'c:\\missingdir'
```

Example for an application using Appveyor to produce artifacts:

```
id: 'sabentisplus'
# Type of downloader/monitor to use,
# currently only appveyor
downloader:
# Currently only "appveyor" and "localpath" supported
 type: 'appveyor'
 project: 'zzzz'
 username: 'yyyy'
 apitoken: 'xxxx'
 branch: 'trunk'
 publish_regex_filter: ''
```

Depending on the type of downloader, you might need to specify additional information. I.e. for the AppVeyor downloader you must provide a branch (plus the security credentials).

To install an application from such a configuration file use:

`Invoke-ChefAppDeploy c:\myapplications\myappsettings.yml`

### Deploying a specific version of an application and doing rollbacks

Shit happens. If a faulty application has passed your CI and QA processes and was deployed, yet you need to rollback to a previous version Chef has the tools needed to do so.

By default when calling Invoke-ChefAppRedeploy the downloader will look for the latest available succesful build (if the downloader has this capability, currently only AppVeyorDownloader).

You can specify a specific buildId:

`Invoke-ChefAppRedeploy myaplicationid -Force -BuildId "1.0.233"`

Once you have forced a deployment with a specific buildId, *only doing specific buildId deployments with the -Force option will work*. That means that automatic deployments will be locked until you release the verson constraint.

To release this constraint specify the literal "latest" as the buildId:

`Invoke-ChefAppRedeploy myaplicationid -Force -BuildId "latest"`

After doing so, automatic deployments to latest successful builds will be restored.

You must understand that going back to previous builds of an application can completely break it (i.e. a newer version alters a database schema and the old version is not prepared to deal with it).

### Managing applications

To see a list of deployed application in your system use:

`Invoke-ChefAppFind`

This will list (and also provide a powershell array) all the applications installed.

You can trigger redeployment of a single application like this:

`Invoke-ChefAppRedeploy myapplication`

Note that downloaders (such as Appveyor) will NOT redeploy an application unless a new artifact is available.

To force deployment use:

`Invoke-ChefAppRedeploy myapplication -Force`

If you don't specify an application name, the *Invoke-ChefAppRedeploy* command attempt to redeploy all installed applications.

### Uninstalling an application

To remove an application from the system, use the following command:

`Invoke-ChefAppRemove myapplicationname`

Note that removing an application will DELETE any resources allocated to it, including
database, disk storage and others.

## Installing the windows service

The Chef core is designed to work as a windows service that will attempt to redeploy all installed applications periodically.

To install the service download an extract the binaries to a location in your computer,
and run the installer:

`iischef.service.exe -install`

you can remove the service at any time using:

`iischef.service.exe -uninstall`

Service startup issues will be logged to system as an Application Event type.

## Merging Chef configuration files or handling settings per environment

In the same repository you can have multiple chef configuration files, and have them combined or applied depending on the deployment environment or branch.

At the application level you can limit the scope of a chef file using regular expressions, use these options at the root of your configuration file:

```
scope-branch-regex: '.*'
scope-environment-regex: '.*'
scope-tags: 'mytag1, mytag2, mytag3'
scope-weight: 0
inherit: default
```

Internally what the Chef deployer will do is scan all chef configuration files, grab all the ones that either match the current environment or the current branch, sort them by scope weight
and the merge them using an additive strategy.

In example, to add debugging support to a PHP development environment we simply need to add a few extra lines to an environment specific chef file:

```
# Only use this for environment with name "local"
scope-environment-regex: 'local.*'
inherit: default
deployers:
  php:
    runtime:
      - {type: 'ini', multivalue: true, 'key':zend_extension, 'value':php_xdebug.dll}
      - {type: 'ini','key':opcache.revalidate_freq , 'value': 5}
      - {type: 'ini','key':opcache.validate_timestamps , 'value': On}
```

The available configuration options are:

* **scope-branch-regex (Default to all):** What branches this configuration is applied to.
* **scope-environment-regex (Default to all):** What environments this configuration is applied to. An environment is the ID of a Chef Installation on a server set on the global chef configuration file.
* **scope-tags (Leave empty to not match): ** When deploying environments, these can have "tags" that can be matched here with a comma delimited list. All tags must exist in the environment for the criteria to  match. Tags are declared in the InstalledApplication settings. The default available tags are:
  * **app-id-{applicationid}:** The unique ID of the application
* **scope-weight:** Allows you to control what order are the chef settings files merged
* **inherit:** Let's you break the inheritance behavior, letting a specific chef configuration file override any other less specific configuration. Currently only supports the "break" keyword. When used, any higher level configuration will be dismissed (use scope-weight to control levels).

**NOTES:**

* There is no way to "clear" parent configurations partially.
* All scope options (scope-branch-regex, scope-environment, scope-tags) are additive. If one of them does not match the whole of the configuration file will not be considered for the environment.

## Application settings and environment settings

Once your application is deployed, it needs to know "where" and "how" to access the requested services. I.e. if you requested an SQL Server database, you need to know the
credentials, host and other relevant information to make the connection.

Besides the runtime settings defined by chef itself (to represent the information related to the requested environment services) you can add
any relevant information for your application (AKA application settings) in the "app_settings" attribute:

```
app_settings:
  mail:
    host: 'mail.google.com'
    username: 'username'
    pwd: 'password'
```

IMPORTANT: By using Chef's inheritance and configuration combination rules you can use this to have different configuration
files per environment, branch or other criteria that chef supports.

All this information is stored in a JSON key-value pair file that can be accessed in several ways at application runtime.

A file is written to the web root with the name "chef-runtime.path". This is a plain text file that points to a JSON file on the file system containing 
the deployment settings, such as:

```
{
  "environment.enable_mail_verification": "true",
  "environment.mail_send_enable": "true",
  "environment.mail_send_redirect": null,
  "iis.local.bindings.default.url": "http://xxx",
  "iis.public.bindings.default.url": "http://xxx",
  "iis.cdn.bindings.cdn.cdn.url": "http://xxx",
  "iis.cdnssl.bindings.cdn.cdnssl.url": "http://xxx",
  "iis.local.bindings.cdn.local.url": "http://xx",
  "deployment.shortId": "chf_upcplus_sfk",
  "deployment.appPath": "D:\\_Webs\\chf_upcplus_sfk\\app",
  "deployment.logPath": "E:\\ChefApplicationData\\Logs\\upcplus",
  "deployment.tempPath": "E:\\ChefApplicationData\\Temporary\\upcplus",
  "installedApp.id": "upcplus",
  "installedApp.branch": null,
  "services.disk.contents.mount.files.path": "E:\\ChefApplicationData\\Contents\\store_upcplus\\files",
  "services.disk.contents.mount.private.path": "E:\\ChefApplicationData\\Contents\\store_upcplus\\private",
  "services.disk.contents.mount.temp.path": "E:\\ChefApplicationData\\Contents\\store_upcplus\\temporary",
  "services.sqlsrv.default.username": "chf_upcplus_default",
  "services.sqlsrv.default.password": "5e603bd3-f4e6-4fac-9734-0d0fa2cb4576",
  "services.sqlsrv.default.database": "chf_upcplus_default",
  "services.sqlsrv.default.host": "192.168.3.2",
  "services.couchbase.default.uri": "couchbase://192.168.3.2",
  "services.couchbase.default.bucket-name": "couch",
  "services.couchbase.default.bucket-password": "couch"
}
```

You can parse this file from within your application and use the information as needed.

### Dump configuration files to a specific location

Use the configuration_dump_paths attribute to define a list of locations where the full runtime configuration files
will be copied to relative to the source path of the artifact.

```
configuration_dump_paths:
  path0: 'ETG.SABENTISpro.IIS\Settings'
```

The above example will generate two JSON files:

* ETG.SABENTISpro.IIS\Settings\chef-settings.json
* ETG.SABENTISpro.IIS\Settings\chef-settings-nested.json

They both contain exactly the same configuration, but structured diferently (key-value pairs vs nested json objects).

### Replacements in regular configurations files

If you have an application that already has configuration files, you can instruct chef to perform string based replacements
when deploying. The available replacements are the ones that are written to the chef-configuration.json file as stated
in the previous section.

You can for example have a config.inc.template.php file in your project such as:

```
;;;;;;;;;;;;;;;;;;;;;
; Database Settings ;
;;;;;;;;;;;;;;;;;;;;;

[database]

driver = mysql
host = "{orpjournal.database.host}"
username = "{orpjournal.database.username}"
password = "{orpjournal.database.password}"
name = "{orpjournal.database.database}"
```

Then on the your chef settings file instruct the app deployer to perform the replacements and rename the file:

```
- type: 'app'
  configuration_replacement_files:
   'config.inc.template.php': 'config.inc.php'
```

This will load the contents of "/config.inc.template.php", replace any matching values from the runtime settings and save the resulting
file to "/config.inc.php"

## Mirroring applications

One of the advantages of fully automated deployment processes is that it makes it easy to spin up cloned environments that can easily sync contents and databases between environments.

To spin up an application instance that "inherits" from another application, use the inherit keyword and specify the parent application id:

```
inherit: 'upcplus-production'
id: 'upcplus-dev'
mount_strategy: 'move'
downloader:
 type: 'appveyor'
 project: 'drupal7'
 username: '---'
 apitoken: '---'
 branch: 'b-upcplus-dev'
```

In this case, we are creating a mirror application of "upcplus-production". To all effects, these are independent applications (you need to ensure there are no hostname or other resource collisions by using specific chef configuration files). The only "relationship" between both applications is that
persistent storage (database and disk) will by "syncronized". This synchronization takes place automatically when first installing the application. Any future synchronizations must be manually triggered through a command:

```
Invoke-ChefAppSync upcplus-dev
```

# Command reference

## Invoke-ChefAppCleanup

Helps application deployers get rid of unused resources, left overs from deployments that did not succeed or got stuck.

## Invoke-ChefAppCron

Runs the maintenance loop:

* Clean-up log directories (zip big files and deletes very old ones)
* Deletes old files in the temp folder

## Invoke-ChefAppRemove -Id={applicationId) -Force

Removes an application

## Invoke-ChefAppDeploy

// UNDOCUMENTED

## Invoke-ChefAppDeployPath -Path={artifact_path} -Id={applicationid} -MountStrategy={link, copy, original}

Deploys an artifact from the specified path.

## Invoke-ChefAppFind -Id={applicationId}

Finds the deployment object for the application, or all the applications if Id is empty.

## Invoke-ChefSelfInstall -path={destination_path}

Does self initialization of Chef configuration files into the specified destination path.

## Invoke-ChefAppRedeploy -Id={applicationid} -BuildId={buildId} -Force

Redeploy the given application. If build ID is specified, the deployer will look for that artifact version and that application will be stuck into that version until redeploy is invoked again.

## Invoke-ChefAppRemoveExpired -Id={applicationid/optional}

Runs a cleanup loop to remove expired environments.