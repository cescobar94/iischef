﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Net;
using iischef.utils;
using iischef.core;
using iischef.core.SystemConfiguration;
using System.Data.SqlClient;
using Microsoft.Web.Administration;
using Application = iischef.core.Application;
using Newtonsoft.Json.Linq;
using Xunit;

namespace healthmonitortests
{
    public class FunctionalTests
    {
        [Fact]
        [Trait("Category", "Functional")]
        public void TestJsonKVConverter()
        {
            var converter = new iischef.core.Configuration.JObjectToKeyValueConverter();

            var original = JObject.FromObject(
                new
                {
                    mailSettings = new
                    {
                        host = "myhost",
                        port = "myport"
                    },
                    myArray = new List<Object>() {
                        "a",
                        "b",
                        "c",
                        new  {
                            pro1 = "prop1",
                            pro2 = "prop2",
                        }
                    }
                }
           );

            var c1 = converter.NestedToKeyValue(original);

            var serialized = Newtonsoft.Json.JsonConvert.SerializeObject(c1);
            Assert.Equal("{\"mailSettings.host\":\"myhost\",\"mailSettings.port\":\"myport\",\"myArray.0\":\"a\",\"myArray.1\":\"b\",\"myArray.2\":\"c\",\"myArray.3.pro1\":\"prop1\",\"myArray.3.pro2\":\"prop2\"}",
                serialized);

            // Now the other way round...
            JObject reverted = converter.keyValueToNested(c1);

            Assert.Equal(
                Newtonsoft.Json.JsonConvert.SerializeObject(original),
                Newtonsoft.Json.JsonConvert.SerializeObject(reverted)
                );
        }

        /// <summary>
        /// Only for manual testing to debug the service.
        /// </summary>
        [Fact]
        [Trait("Category", "Functional")]
        public void TestServiceCanStop()
        {
            var service = new ApplicationService();
            service.Start();

            var start = DateTime.Now;

            while (true)
            {
                System.Threading.Thread.Sleep(400);

                if ((DateTime.Now - start).TotalSeconds > 2)
                {
                    service.Stop();
                    break;
                }
            }
        }

        [Fact]
        [Trait("Category", "Functional")]
        public void GenerateSampleApplicationSettings()
        {

            var settings = new iischef.core.SystemConfiguration.EnvironmentSettings();

            settings.couchbaseServers = new List<CouchbaseServer>();
            settings.couchbaseServers.Add(new CouchbaseServer()
            {
                id = "default",
                uri = "couchbase://127.0.0.1",
                bucketName = "couch",
                bucketPassword = "couch"
            });

            settings.primaryCouchbaseServer = "default";

            settings.sqlServers = new List<SQLServer>();
            settings.sqlServers.Add(new SQLServer()
            {
                id = "default",
                connectionString = "Server=localhost;"
            });

            settings.primarySqlServer = "default";

            settings.contentStorages = new List<StorageLocation>();
            settings.contentStorages.Add(
                new StorageLocation()
                {
                    id = "default",
                    path = "D:\\_contents"
                }
            );

            settings.primaryContentStorage = "default";

            settings.applicationStorages = new List<StorageLocation>();
            settings.applicationStorages.Add(
                new StorageLocation()
                {
                    id = "default",
                    path = "D:\\_webs"
                }
            );

            settings.primaryApplicationStorage = "default";

            string serialized = Newtonsoft.Json.JsonConvert.SerializeObject(settings, Newtonsoft.Json.Formatting.Indented);
        }
    }
}
