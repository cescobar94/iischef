﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using iischef.utils;

namespace iischef.core
{
    public class ArtifactSettings
    {
        /// <summary>
        /// The commit branch.
        /// </summary>
        public string branch { get; set; }

        /// <summary>
        /// The commit hash.
        /// </summary>
        public string commit_sha { get; set; }

        /// <summary>
        /// Try to grab from environment variables (appveyor!!!)
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public void populateFromEnvironment()
        {
            string tempBranch = Environment.GetEnvironmentVariable("APPVEYOR_REPO_BRANCH");
            if (string.IsNullOrWhiteSpace(this.branch) && !string.IsNullOrWhiteSpace(tempBranch))
            {
                this.branch = tempBranch;
            }

            string tempCommit = Environment.GetEnvironmentVariable("APPVEYOR_REPO_COMMIT");
            if (string.IsNullOrWhiteSpace(this.commit_sha) && !string.IsNullOrWhiteSpace(tempCommit))
            {
                this.commit_sha = tempCommit;
            }
        }

        /// <summary>
        /// Grab from a settings file.
        /// </summary>
        /// <param name="path"></param>
        public void populateFromSettingsFile(string path, logger.LoggerInterface logger)
        {
            string file = UtilsSystem.CombinePaths(path, "artifact-settings.yml");

            if (!File.Exists(file))
            {
                return;
            }

            var configfile = new Configuration.YamlConfigurationFile();

            try
            {
                // This file might be malformed, do not crash and let other
                // environment information sources have their chance
                configfile.parseFromFile(file);
            }
            catch (Exception e)
            {
                logger.LogException(new Exception("Error parsing file: " + file, e));
                return;
            }

            // Parse the artifact settings...
            branch = configfile.getStringValue("repo-branch", null);
            commit_sha = configfile.getStringValue("repo-commit", null);
        }

        /// <summary>
        /// Grab from local GIT repo.
        /// </summary>
        /// <param name="path"></param>
        public void populateFromGit(string path)
        {
            // Crawl up to find the first directory covered by GIT. There might be a difference
            // between the artifact folder structure and the repository (local working copy) itself...
            DirectoryInfo difo = new DirectoryInfo(path);
            string gitpath = null;
            while (difo != null && difo.Exists)
            {
                if (Directory.Exists(iischef.utils.UtilsSystem.CombinePaths(difo.FullName, ".git")))
                {
                    gitpath = difo.FullName;
                    break;
                }

                difo = difo.Parent;
            }

            if (gitpath == null)
            {
                return;
            }

            try
            {
                // Try to get information directly from GIT??
                var repo = new LibGit2Sharp.Repository(gitpath, new LibGit2Sharp.RepositoryOptions() { });

                branch = repo.Head.FriendlyName;
                commit_sha = repo.Commits.First().Sha;
            }
            catch (Exception e)
            {
                // Trying to read settings from GIT can be delicate. Such as...
                // https://github.com/GitTools/GitVersion/issues/1043
            }
        }
    }
}
