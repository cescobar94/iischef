﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iischef.core.Configuration
{
    public class DeploymentSettings
    {
        /// <summary>
        /// The deployment windows.
        /// </summary>
        public Dictionary<string, DeploymentWindow> deployment_windows { get; set; }
    }
}
