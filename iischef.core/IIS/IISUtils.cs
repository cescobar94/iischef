﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Web.Administration;
using System.IO;
using System.Security.AccessControl;
using System.Xml.Linq;
using System.Net;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Diagnostics;
using System.Net.Mail;

using System.Linq.Expressions;
using System.Text.RegularExpressions;
using iischef.utils;

namespace iischef.core.IIS
{
    public class IISUtils
    {
        /// <summary>
        /// Set the location for the IIS logs
        /// </summary>
        /// <param name="id">Site ID</param>
        /// <param name="path"></param>
        public static void SetSiteLogFolder(long id, string path)
        {
            using (ServerManager manager = new ServerManager())
            {
                Site s = manager.Sites.Where(i => i.Id == id).First();
                path = path + "\\" + s.Name;
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                s.LogFile.SetAttributeValue("Directory", path);
                manager.CommitChanges();
            }
        }

        public static List<string> validIdentityTypesForPool = new List<string>() {
            "SpecificUser",
            "LocalSystem",
            "LocalService",
            "NetworkService",
            "ApplicationPoolIdentity"
        };

        /// <summary>
        /// 
        /// </summary>
        /// <param name="serverManager"></param>
        /// <param name="poolName"></param>
        /// <returns>SpecificUser || LocalSystem || LocalService || NetworkService || ApplicationPoolIdentity</returns>
        public static string findIdentityTypeForPool(ServerManager serverManager, string poolName)
        {

            Microsoft.Web.Administration.Configuration config = serverManager.GetApplicationHostConfiguration();

            ConfigurationSection applicationPoolsSection = config.GetSection("system.applicationHost/applicationPools");

            ConfigurationElementCollection applicationPoolsCollection = applicationPoolsSection.GetCollection();

            ConfigurationElement addElement = FindElement(applicationPoolsCollection, "add", "name", poolName);
            if (addElement == null) throw new InvalidOperationException("Element not found!");


            ConfigurationElement processModelElement = addElement.GetChildElement("processModel");
            return (string)processModelElement["identityType"];

        }

        /// <summary>
        /// Busca la carpeta de user settings para application pools
        /// que están configurados como "ApplicationPoolIdentity".
        /// </summary>
        /// <param name="serverManager"></param>
        /// <param name="poolName"></param>
        /// <returns></returns>
        public static string findStorageFolderForAppPoolWithDefaultIdentity(ApplicationPool pool)
        {
            // Peligroso... este método solo para las identidades de defecto.
            if (pool.ProcessModel.IdentityType != ProcessModelIdentityType.ApplicationPoolIdentity)
            {
                return null;
            }

            // If the user profile is not being loaded, then no need
            // to handle this.
            if (pool.ProcessModel.LoadUserProfile == false)
            {
                return null;
            }

            var suffix = "\\" + Environment.UserName;
            var pathWithEnv = Environment.ExpandEnvironmentVariables(@"%USERPROFILE%");
            if (!pathWithEnv.EndsWith(suffix))
            {
                return null;
            }

            var subpath = pathWithEnv.Substring(0, pathWithEnv.Length - suffix.Length);
            var userpath = UtilsSystem.CombinePaths(subpath, pool.Name + ".IIS APPPOOL");

            if (Directory.Exists(userpath))
            {
                return userpath;
            }

            userpath = UtilsSystem.CombinePaths(subpath, pool.Name);
            if (Directory.Exists(userpath))
            {
                return userpath;
            }

            return null;

        }

        protected static ConfigurationElement FindElement(ConfigurationElementCollection collection, string elementTagName, params string[] keyValues)
        {
            foreach (ConfigurationElement element in collection)
            {
                if (String.Equals(element.ElementTagName, elementTagName, StringComparison.OrdinalIgnoreCase))
                {
                    bool matches = true;

                    for (int i = 0; i < keyValues.Length; i += 2)
                    {
                        object o = element.GetAttributeValue(keyValues[i]);
                        string value = null;
                        if (o != null)
                        {
                            value = o.ToString();
                        }

                        if (!String.Equals(value, keyValues[i + 1], StringComparison.OrdinalIgnoreCase))
                        {
                            matches = false;
                            break;
                        }
                    }
                    if (matches)
                    {
                        return element;
                    }
                }
            }
            return null;
        }

    }
}
