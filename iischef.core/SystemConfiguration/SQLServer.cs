﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace iischef.core.SystemConfiguration
{
    /// <summary>
    /// Information to access a local SQL Server for deployments
    /// </summary>
    public class SQLServer
    {
        public string id { get; set; }

        public string connectionString { get; set; }
    }
}
