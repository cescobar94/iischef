﻿using System.IO;
using System.Management.Automation;

namespace iischef.cmdlet
{
    [Cmdlet(VerbsLifecycle.Invoke, "ChefSelfInstall")]
    public class ChefAppInstall : Cmdlet
    {
        /// <summary>
        /// Drive letter to install chef into...
        /// </summary>
        [Parameter(ValueFromPipelineByPropertyName = true, Mandatory = true)]
        public string path { get; set; }

        protected override void ProcessRecord()
        {
            // Try to create the directory.
            var difo = new DirectoryInfo(path);
            if (!difo.Exists)
                Directory.CreateDirectory(path);

            var app = ConsoleUtils.getApplicationForConsole(false);
            app.selfInstall(path, null);
        }
    }
}
