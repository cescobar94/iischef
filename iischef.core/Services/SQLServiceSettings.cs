﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iischef.core.Services
{
    public class SQLServiceSettings : DeployerSettingsBase
    {
        /// <summary>
        /// Service type
        /// </summary>
        public string type { get; set; }

        /// <summary>
        /// Service id
        /// </summary>
        public string id { get; set; }

        /// <summary>
        /// Recovery mode
        /// </summary>
        public string recoveryModel { get; set; }

    }

}
