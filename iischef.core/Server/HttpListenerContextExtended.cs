﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Threading;
using System.Text.RegularExpressions;
using System.Collections;
using System.Collections.Concurrent;
using System.IO;

namespace iischef.core.Server
{
    public class HttpListenerContextExtended
    {
        private Hashtable data = new Hashtable();

        public HttpListenerContext ctx;

        public HttpListenerContextExtended(HttpListenerContext ctx)
        {
            this.ctx = ctx;
        }

        /// <summary>
        /// Send server event to client.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="data"></param>
        /// <param name="ctx"></param>
        public void SendServerEvent(string id, string data, bool flush = false)
        {
            this.WriteToStream(string.Format("id: {0}" + Environment.NewLine, id));
            this.WriteToStream(string.Format("data: {0}" + Environment.NewLine, data));
            this.WriteToStream(Environment.NewLine);

            if (flush)
            {
                ctx.Response.OutputStream.Flush();
            }
        }


        /// <summary>
        /// Write the string to the response stream.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="ctx"></param>
        public void WriteToStream(string data)
        {
            byte[] buf = Encoding.UTF8.GetBytes(data);
            ctx.Response.OutputStream.Write(buf, 0, buf.Length);
        }

        public void ReturnAccessDenied()
        {
            ctx.Response.StatusCode = 403;
        }

        private bool ResponseSent = false;

        private void ResponseOnlySentOnce()
        {
            if (this.ResponseSent)
            {
                throw new Exception("Can only send response once.");
            }

            this.ResponseSent = true;
        }

        public void ReturnObject(object result)
        {
            ResponseOnlySentOnce();

            var response = new Response();
            response.result = result;

            var data = Newtonsoft.Json.JsonConvert.SerializeObject(response);
            this.WriteToStream(data);
        }

        public void ReturnException(Exception ex, bool clear_output = false)
        {
            ResponseOnlySentOnce();

            var response = new Response();
            response.result = null;
            response.unhandled_exception = ex.Message;

            var data = Newtonsoft.Json.JsonConvert.SerializeObject(response);
            this.WriteToStream(data);
        }

        public HttpListenerRequest Request
        {
            get
            {
                return this.ctx.Request;
            }
        }

        public HttpListenerResponse Response
        {
            get
            {
                return this.ctx.Response;
            }
        }

        public string GetRequestIPidentifier()
        {
            string proxy = string.Empty;

            // Proxy
            if (ctx.Request.Headers["X-Forwarded-For"] != null)
            {
                proxy = ctx.Request.Headers["X-Forwarded-For"];
            }

            // Proxy
            if (ctx.Request.Headers["X-Client-IP"] != null)
            {
                proxy = ctx.Request.Headers["X-Client-IP"];
            }

            // Unique ID is combination of Proxy and original source address.
            return proxy + ":" + ctx.Request.RemoteEndPoint.Address.ToString();
        }

        public System.Collections.Specialized.NameValueCollection GetInput()
        {
            if (!data.ContainsKey("GetData"))
            {
                // Read the iniput (for example POST data)
                var request = ctx.Request;
                string text;
                using (var reader = new StreamReader(request.InputStream,
                                                     request.ContentEncoding))
                {
                    text = reader.ReadToEnd();
                    data["GetData"] = System.Web.HttpUtility.ParseQueryString(text);
                }
            }




            return (System.Collections.Specialized.NameValueCollection)data["GetData"];
        }
    }
}
