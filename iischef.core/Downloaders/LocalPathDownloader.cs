﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using iischef.utils;

namespace iischef.core.Downloaders
{
    /// <summary>
    /// Downlader to be used for projects
    /// that exist in the local filesystem
    /// usually development checkouts.
    /// </summary>
    public class LocalPathDownloader : DownloaderInterface
    {
        /// <summary>
        /// Downloader settings.
        /// </summary>
        protected LocalPathDownloaderSettings settings;

        /// <summary>
        /// Global settings.
        /// </summary>
        protected SystemConfiguration.EnvironmentSettings globalSettings;

        /// <summary>
        /// The logger service.
        /// </summary>
        protected logger.LoggerInterface logger;

        /// <summary>
        /// Get an instance of LocalPathDownloader.
        /// </summary>
        /// <param name="settings"></param>
        /// <param name="globalSettings"></param>
        /// <param name="logger"></param>
        public LocalPathDownloader(LocalPathDownloaderSettings settings,
            SystemConfiguration.EnvironmentSettings globalSettings,
            logger.LoggerInterface logger)
        {
            this.settings = settings;
            this.globalSettings = globalSettings;
            this.logger = logger;
        }

        /// <summary>
        /// Pull the next available artifact id.
        /// </summary>
        /// <returns></returns>
        public string GetNextId(string buildId = null)
        {
            // We won't limit build id's for local path downloader during
            // testing process.
            var istest = globalSettings.options.Contains("testenvironment");

            if (!String.IsNullOrWhiteSpace(buildId) && !istest)
            {
                throw new Exception("LocalPathDownloader does not support deploying from a specific buildId.");
            }

            if (settings.monitorChangesTo != null && settings.monitorChangesTo.Any())
            {
                StringBuilder signature = new StringBuilder();

                var difo = new DirectoryInfo(settings.path);

                foreach (var p in settings.monitorChangesTo)
                {
                    var files = difo.EnumerateFiles(p);

                    foreach (var f in files)
                    {
                        signature.AppendLine(f.FullName + ":" + f.LastWriteTimeUtc.ToUnixTimestamp().ToString());
                    }
                }

                // Si cambia alguno de los ficheros esta firma cambiará.
                return "monitorchanges:" + UtilsEncryption.GetMD5(signature.ToString());
            }

            // Por defecto usa el lastwritetime del directorio... esto en Windows es una mierda
            // porque no hay propagación vertical de esta información (i.e. si modificar un fichero
            // dentro del directorio la fecha del directorio no cambia).
            return "lastwritetime:" + (new DirectoryInfo(settings.path)).LastWriteTimeUtc.ToUnixTimestamp().ToString();
        }

        /// <summary>
        /// Pull an artifact using it's ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Artifact PullFromId(string id)
        {
            Artifact artifact = new Artifact();

            artifact.obtainedAt = DateTime.UtcNow;
            artifact.id = id;
            artifact.localPath = settings.path;

            artifact.artifactSettings = new ArtifactSettings();

            // We will merge data from both git and settings file, local settigns file
            // will override anything from GIT (if available).
            artifact.artifactSettings.populateFromGit(artifact.localPath);
            artifact.artifactSettings.populateFromSettingsFile(artifact.localPath, this.logger);
            artifact.artifactSettings.populateFromEnvironment();

            // Branch name is critical to some deployment... populate with a no-branch-found....
            if (String.IsNullOrEmpty(artifact.artifactSettings.branch)) {
                artifact.artifactSettings.branch = "no-branch-found";
                logger.LogInfo(true, "Could not identify git branch for artifact. Using default: 'no-branch-found'");
            }

            return artifact;
        }
    }
}
