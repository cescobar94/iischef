﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Threading;
using iischef.logger;
using System.Timers;

namespace iischef.core.Server
{
    /// <summary>
    /// We have an embedded web server to receive control
    /// commands.
    /// </summary>
    public class Server
    {
        private readonly HttpListener _listener = new HttpListener();
        private readonly Action<HttpListenerContextExtended> _responderMethod;

        protected SystemLogger logger;

        System.Timers.Timer cronTimer;


        public Server(string[] prefixes, SystemLogger logger, Action<HttpListenerContextExtended> method)
        {
            this.logger = logger;

            if (!HttpListener.IsSupported)
            {
                throw new NotSupportedException(
                    "Needs Windows XP SP2, Server 2003 or later.");
            }

            // URI prefixes are required, for example 
            // "http://localhost:8080/index/".
            if (prefixes == null || prefixes.Length == 0)
                throw new ArgumentException("prefixes");

            // A responder method is required
            if (method == null)
                throw new ArgumentException("method");

            foreach (string s in prefixes)
                _listener.Prefixes.Add(s);

            _responderMethod = method;
            _listener.Start();

            cronTimer = new System.Timers.Timer();
            cronTimer.Elapsed += CronTimer_Elapsed;

            // Cron que se ejecuta cada hora.
            cronTimer = new System.Timers.Timer(1000 * 3600);
            cronTimer.Start();
        }

        private void CronTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            cronTimer.Stop();

            cronTimer.Start();
        }

        public Server(Action<HttpListenerContextExtended> method, SystemLogger logger, params string[] prefixes)
            : this(prefixes, logger, method)
        {

        }

        public void Run()
        {

            ThreadPool.QueueUserWorkItem((o) =>
            {
                try
                {
                    while (_listener.IsListening)
                    {
                        ThreadPool.QueueUserWorkItem((c) =>
                        {
                            var ctx = new HttpListenerContextExtended(c as HttpListenerContext);
                            try
                            {
                                _responderMethod(ctx);

                            }
                            catch (Exception e)
                            {
                                ctx.ReturnException(e, true);
                            }
                            finally
                            {
                                try
                                {
                                    // always close the stream
                                    ctx.Response.OutputStream.Close();
                                }
                                catch (Exception ex2)
                                {
                                    logger.LogError("Unhandled exception while closing response stream: " + ex2.Message);
                                }
                            }

                        }, _listener.GetContext());
                    }
                }
                catch (Exception ex3)
                {
                    logger.LogError("Unhandled exception while running server loop: " + ex3.Message);
                }
            });
        }

        public void Stop()
        {
            cronTimer.Stop();
            _listener.Stop();
            _listener.Close();
        }
    }
}
