﻿using FileLock;
using iischef.core.Configuration;
using iischef.core.SystemConfiguration;
using iischef.logger;
using iischef.utils;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace iischef.core
{
    /// <summary>
    /// Component that manages full application deployments
    /// </summary>
    public class ApplicationDeployer
    {
        /// <summary>
        /// Environment settings
        /// </summary>
        protected EnvironmentSettings globalSettings;

        /// <summary>
        /// Current active deployment for this application
        /// </summary>
        public Deployment deploymentActive;

        /// <summary>
        /// Where to store the active deployment settings
        /// </summary>
        string activeDeploymentPathStorage;

        /// <summary>
        /// Installed application settings.
        /// </summary>
        InstalledApplication installedAppSettings;

        protected LoggerInterface logger;

        /// <summary>
        /// Return a list of active deployments in the system.
        /// </summary>
        /// <returns></returns>
        public static List<Deployment> getActiveDeployments(SystemConfiguration.EnvironmentSettings globalSettings)
        {
            List<Deployment> result = new List<Deployment>();
            DirectoryInfo dir = new DirectoryInfo(UtilsSystem.CombinePaths(globalSettings.settingsDir, "deployments"));
            foreach (var f in dir.EnumerateFiles("active.*.json"))
            {
                result.Add(Deployment.InstanceFromPath(f.FullName, globalSettings));
            }
            return result;
        }

        public static Deployment getActiveDeploymentById(SystemConfiguration.EnvironmentSettings globalSettings, string id)
        {
            List<Deployment> result = new List<Deployment>();
            DirectoryInfo dir = new DirectoryInfo(UtilsSystem.CombinePaths(globalSettings.settingsDir, "deployments"));
            foreach (var f in dir.EnumerateFiles("active." + id + ".json"))
            {
                return Deployment.InstanceFromPath(f.FullName, globalSettings);
            }
            return null;
        }

        /// <summary>
        /// Get a deployer for the installed application.
        /// </summary>
        /// <param name="globalSettings">The global settings.</param>
        /// <param name="installedApplicationSettings">The installed application settings.</param>
        /// <param name="logger">The logger.</param>
        public ApplicationDeployer(EnvironmentSettings globalSettings,
            InstalledApplication installedApplicationSettings,
            LoggerInterface logger)
        {
            this.globalSettings = globalSettings;
            this.installedAppSettings = installedApplicationSettings;
            this.logger = logger;

            if (this.globalSettings == null)
            {
                throw new InvalidDataException("settings argument cannot be null.");
            }

            if (installedAppSettings == null)
            {
                throw new Exception("installedApplicationSettings argument cannot be null.");
            }

            // Try to grab previous deployment...
            activeDeploymentPathStorage = UtilsSystem.CombinePaths(this.globalSettings.settingsDir, "deployments", "active." + installedAppSettings.getId() + ".json");
            if (File.Exists(activeDeploymentPathStorage))
            {
                deploymentActive = Deployment.InstanceFromPath(activeDeploymentPathStorage, globalSettings);
            }
        }

        /// <summary>
        /// Uninstalls and APP and removes all services and deployments.
        /// </summary>
        /// <param name="force">If set to true, will force removal even if there are partial failures.</param>
        public void UninstallApp(bool force = true)
        {
            logger.LogInfo(false, "UninstallApp: " + deploymentActive.getShortId());

            DeployerCollection deployers = null;
            DeployerCollection services = null;

            try
            {
                deployers = deploymentActive.grabDeployers(logger);
            }
            catch (Exception e0)
            {
                if (!force)
                {
                    throw;
                }
            }

            try
            {
                services = deploymentActive.grabServices(logger);
            }
            catch (Exception e0)
            {
                if (!force)
                {
                    throw;
                }
            }

            try
            {
                // We need to stop prior to removing...
                if (deployers != null)
                {
                    deployers.StopAll();
                }

                if (services != null)
                {
                    services.StopAll();
                }

                // Cleanup previous environments
                if (deployers != null)
                {
                    deployers.UndeployAll(true, true);
                }

                if (services != null)
                {
                    services.UndeployAll(true, true);
                }

                File.Delete(activeDeploymentPathStorage);
            }
            catch (Exception e)
            {
                if (force)
                {
                    logger.LogException(new Exception("An issue was found during application removal. But still application has been uninstalled by using the -force option.", e));
                    File.Delete(activeDeploymentPathStorage);
                }
                else
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Build the final chef.yml configuration file
        /// </summary>
        /// <param name="appPath">Path to the chef folder</param>
        /// <param name="environmentId">Environment id</param>
        /// <param name="branch">Branch name</param>
        /// <param name="usedConfigFiles">List of configuration files merged to generate the final configuration</param>
        /// <returns></returns>
        protected ApplicationSettings loadApplicationSettings(
            string appPath,
            string environmentId,
            string branch,
            out List<string> usedConfigFiles)
        {
            // Ensure environment and branch are not null, to prevent
            // Regex.Match from crashing.
            if (environmentId == null)
            {
                environmentId = "";
            }

            if (branch == null)
            {
                branch = "";
            }

            // A little bit shitty... but we need to parse ALL configuration files
            // to figure out which one applies to current branch and/or environment.
            DirectoryInfo difo = new DirectoryInfo(appPath);

            if (!difo.Exists)
            {
                throw new Exception("The provided application path is missing a chef folder: " + appPath);
            }

            var files = difo.EnumerateFiles("chef*.yml");

            List<ApplicationSettings> configurationFiles = new List<ApplicationSettings>();

            var environmentTags = globalSettings.getOptions();
            environmentTags.AddRange(this.installedAppSettings.getTags());

            this.logger.LogInfo(true, "Effective environment tags: " + String.Join(", ", environmentTags));

            // We look for all matching configuration files, and do a weight based merge.
            foreach (var f in files)
            {
                var app = new ApplicationSettings();
                app.parseFromFile(f.FullName);

                bool environment_match = Regex.IsMatch(environmentId, app.getScopeEnvironmentRegex());
                bool branch_match = Regex.IsMatch(branch, app.getScopeBranchRegex());
                bool options_match = (!app.getScopeTags().Any())
                    || environmentTags.Intersect(app.getScopeTags()).Any();

                if (environment_match && branch_match && options_match)
                {
                    configurationFiles.Add(app);
                }
            }

            if (!configurationFiles.Any())
            {
                throw new Exception("Could not find a suitable chef*.yml file for the current environment in: " + difo.FullName);
            }

            usedConfigFiles = new List<string>();

            // We now are going to MERGE all settings. Careful here, because order does MATTER when doing overrides.
            // Ideally this would be done with regex specifity, but that is to tough. Just use weights :)
            // http://stackoverflow.com/questions/3611860/determine-regular-expressions-specificity
            configurationFiles = configurationFiles.OrderBy((i) => i.getScopeWeight()).ToList();

            // Now merge all into the first one...
            var currentConfiguration = configurationFiles.First();
            logger.LogInfo(false, "Base settings file: " + currentConfiguration.getSourcePath());
            usedConfigFiles.Add(Path.GetFileName(currentConfiguration.getSourcePath()));
            configurationFiles.RemoveAt(0);

            foreach (var s in configurationFiles)
            {
                logger.LogInfo(false, "Merging settings file: " + s.getSourcePath());

                // Check inheritance
                string inheritance = s.getInherit();
                if (inheritance == "break")
                {
                    usedConfigFiles.Clear();
                    logger.LogInfo(false, "Settings file requested an inheritance break. Loosing all previuos settings.");
                    currentConfiguration = s;
                }
                else
                {
                    usedConfigFiles.Add(Path.GetFileName(s.getSourcePath()));
                    currentConfiguration.merge(s);
                }
            }

            return currentConfiguration;
        }

        /// <summary>
        /// Do not deploy applications if we are short on disk space.
        ///
        /// Throws an exception if size is below minSize
        /// </summary>
        /// <param name="minSize">Defaults to 500Mb</param>
        protected void CheckDiskSpace(long minSize = 524288000)
        {
            var applicationPath = this.globalSettings.GetDefaultApplicationStorage().path;

            long freeSpaceBytes = UtilsSystem.GetTotalFreeSpace(applicationPath);

            if (freeSpaceBytes < minSize)
            {
                throw new Exception($"Insuficient storage [{UtilsSystem.BytesToString(freeSpaceBytes)}] to run deployments in: {applicationPath}");
            }
        }

        /// <summary>
        /// Deploy an application
        /// </summary>
        /// <param name="app"></param>
        /// <param name="force"></param>
        /// <param name="buildId"></param>
        /// <param name="sync"></param>
        /// <returns></returns>
        public Deployment DeployApp(Application app, bool force = false, string buildId = null, bool sync = false)
        {
            this.CheckDiskSpace();

            // Don't let an application be deployed in parallel, could lead to breaks!
            string lockPath =
                UtilsSystem.ensureDirectoryExists(
                    UtilsSystem.CombinePaths(globalSettings.GetDefaultTempStorage().path, "__chef_locks", "application." + this.installedAppSettings.getId() + ".lock")
                    );

            // TODO: This lock only works INTER-PROCESS but what about INTER-THREAD???
            var fileLock = SimpleFileLock.Create(lockPath, TimeSpan.FromMinutes(3));

            if (fileLock.TryAcquireLock() || System.Diagnostics.Debugger.IsAttached)
            {
                try
                {
                    return _DeployApp(app, force, buildId, sync);
                }
                finally
                {
                    fileLock.ReleaseLock();
                }
            }
            else
            {
                throw new Exception(
                    $"Could not deploy application {installedAppSettings.getId()}. Deployment already running on another process.");
            }
        }


        public Deployment SyncApp()
        {
            deploymentActive.grabDeployers(logger).SyncAll();
            deploymentActive.grabServices(logger).SyncAll();
            return deploymentActive;
        }

        /// <summary>
        /// Deploys an installed app.
        /// </summary>
        public void RestartApp()
        {
            if (deploymentActive == null)
            {
                throw new Exception("Cannot restart an undeployed application.");
            }

            logger.LogInfo(false, "Restarting application: '{0}'", installedAppSettings.getId());

            var deployersActive = deploymentActive.grabDeployers(logger);

            logger.LogInfo(false, "Deployers and services gathered. Starting restart...");

            try
            {
                // Deploy the application base storage (logs, runtime, etc.)
                deployersActive.StopAll();
                deployersActive.StartAll();
            }
            catch (Exception e)
            {
                // Just in case.... log this ASAP
                logger.LogException(new Exception("Error restarting APP: " + deploymentActive.installedApplicationSettings.getId(), e));
                throw;
            }
        }

        /// <summary>
        /// Application cleanup
        /// </summary>
        public void CleanupApp()
        {
            if (deploymentActive == null)
            {
                throw new Exception("Cannot cleanup an undeployed application.");
            }

            logger.LogInfo(false, "Cleanning up application: '{0}'", installedAppSettings.getId());

            var deployersActive = deploymentActive.grabDeployers(logger);
            deployersActive.CleanupAll();
        }

        public void RunCron()
        {
            if (deploymentActive == null)
            {
                throw new Exception("Cannot cleanup an undeployed application.");
            }

            logger.LogInfo(true, "Running cron for application: '{0}'", installedAppSettings.getId());

            var deployersActive = deploymentActive.grabDeployers(logger);
            deployersActive.Cron();
        }

        /// <summary>
        /// Deploys an installed app.
        /// </summary>
        /// <param name="app"></param>
        /// <param name="force"></param>
        /// <param name="buildId"></param>
        /// <param name="sync"></param>
        protected Deployment _DeployApp(
            Application app,
            bool force = false,
            string buildId = null,
            bool sync = false)
        {
            DateTime start = DateTime.Now;

            // The parent application to inherit from (if needed)
            InstalledApplication parentApp = null;

            // Lo primero es descargar ver si hay algo nuevo...
            var downloader = installedAppSettings.getDownloader(this.globalSettings, logger);

            if (!string.IsNullOrWhiteSpace(buildId))
            {
                logger.LogInfo(true, $"Deploying specific version build: " + buildId);
            }

            string nextArtifactId = downloader.GetNextId(buildId == "latest" ? null : buildId);
            string currentArtifactId = deploymentActive != null ? deploymentActive.artifact.id : "";

            bool isNew = deploymentActive == null || deploymentActive.artifact.id != nextArtifactId;

            // Check that Inherit application exists
            if (!String.IsNullOrEmpty(installedAppSettings.getInherit()))
            {
                parentApp = app.getInstalledApp(installedAppSettings.getInherit());
                if (parentApp == null)
                {
                    throw new Exception(
                        $"Application from inheritation: {installedAppSettings.getInherit()}, can not be found");
                }

                logger.LogInfo(true, "Application configured to inherit from parent application '{0}'. Sync:{1}", parentApp.getId(), sync ? "Yes" : "No");
            }

            // Si no es nuevo y no estamos forzando, no hacer deploy.
            if (!isNew && !force)
            {
                logger.LogInfo(true, "No new version found for Application {0}", this.installedAppSettings.getId());
                return deploymentActive;
            }

            if (deploymentActive != null)
            {
                if (!string.IsNullOrEmpty(deploymentActive.enforceBuildId) && buildId != deploymentActive.enforceBuildId
                    && buildId != "latest")
                {
                    throw new Exception(
                        $"Deployment was skipped because previous deployment was a version-specific deployment. Previous buildId='{deploymentActive.enforceBuildId}'. Requested buildId='{buildId}'. Use buildId='latest' to force deploying the latest succesful build.");
                }
            }

            logger.LogInfo(false, "@@ Starting deployment for app: '{0}'", installedAppSettings.getId());
            logger.LogInfo(false, "Current artifact: '{0}' || Previous artifact: '{1}'", nextArtifactId, currentArtifactId);

            // Get from the ID...
            Artifact artifact = downloader.PullFromId(nextArtifactId);

            if (string.IsNullOrWhiteSpace(globalSettings.id))
            {
                throw new Exception("Environment settings cannot have an empty ID.");
            }

            logger.LogInfo(false, "Environment id: '{0}'", globalSettings.id);
            logger.LogInfo(false, "Environment options/tags: '{0}'", String.Join(",", globalSettings.getOptions()));
            logger.LogInfo(false, "Pull artifact lapsed: {0}s", (DateTime.Now - start).TotalSeconds);

            start = DateTime.Now;

            // Look for a configuration file that fits this environment.
            string chefsettingsdir = UtilsSystem.CombinePaths(artifact.localPath, "chef");

            List<string> loadedConfigurationFiles;

            // The final chef configuration files is a combination of Chef files
            var appSettings = loadApplicationSettings(chefsettingsdir,
                globalSettings.id,
                artifact.artifactSettings.branch,
                out loadedConfigurationFiles);

            // Storage for current deployment. Includes all possible environment data
            // in order to provide traceability + rollback capabilities.
            Deployment deployment = new Deployment(
                appSettings,
                globalSettings,
                artifact,
                installedAppSettings,
                parentApp,
                app);

            deployment.setPreviousDeployment(deploymentActive);

            // Check the deployment windows!
            var deploymentSettings = deployment.appSettings.getDeploymentSettings();
            if (deploymentSettings != null)
            {
                if (deploymentSettings.deployment_windows != null
                    && deploymentSettings.deployment_windows.Any())
                {
                    bool canDeploy = false;

                    foreach (var deploymentWindow in deploymentSettings.deployment_windows)
                    {
                        TimeZoneInfo info = TimeZoneInfo.FindSystemTimeZoneById(deploymentWindow.Value.timezone);

                        TimeSpan dtStart = TimeSpan.Parse(deploymentWindow.Value.start);
                        TimeSpan dtEnd = TimeSpan.Parse(deploymentWindow.Value.end);

                        DateTimeOffset localServerTime = DateTimeOffset.Now;
                        DateTimeOffset windowTimeZone = TimeZoneInfo.ConvertTime(localServerTime, info);
                        TimeSpan dtNow = windowTimeZone.TimeOfDay;

                        if (dtStart <= dtEnd)
                        {
                            // start and stop times are in the same day
                            if (dtNow >= dtStart && dtNow <= dtEnd)
                            {
                                // current time is between start and stop
                                canDeploy = true;
                                break;
                            }
                        }
                        else
                        {
                            // start and stop times are in different days
                            if (dtNow >= dtStart || dtNow <= dtEnd)
                            {
                                // current time is between start and stop
                                canDeploy = true;
                                break;
                            }
                        }
                    }

                    // Even if we are not in a deployment windows,
                    // if we are forcing the deployment continue.
                    if (!canDeploy && !force)
                    {
                        logger.LogInfo(false, "Application deployment skipped. Current time not within allowed publishing windows.");
                        return deploymentActive;
                    }
                }
            }

            // Inform about the confiugration files that where used for loading
            deployment.setRuntimeSetting("deployment.loaded_configuration_files", String.Join(",", loadedConfigurationFiles));

            deployment.enforceBuildId = buildId == "latest" ? null : buildId;

            var deployersActive = deploymentActive != null ? deploymentActive.grabDeployers(logger) : new DeployerCollection(globalSettings, null, logger, parentApp);
            var deployers = deployment.grabDeployers(logger);

            var services = deployment.grabServices(logger);

            logger.LogInfo(false, "Deployers and services gathered. Starting installation...");

            var settingsConverter = new Configuration.JObjectToKeyValueConverter();

            try
            {
                // Deploy the application base storage (logs, runtime, etc.)
                deployers.DeployAll();

                // Move the application settings to runtime settings
                var userApplicationSettings = appSettings.getApplicationSettings();
                foreach (var k in settingsConverter.NestedToKeyValue(userApplicationSettings))
                {
                    deployment.setRuntimeSetting("app_settings." + k.Key, k.Value);
                }

                services.DeployAll();

                if (sync)
                {
                    services.SyncAll();
                }

                // Time to hot switch the sites... we need to waitPauseMs for all
                // current requests to finish... because that way we ensure
                // that underlying storage updates will not collide if updates
                // are being deployed.
                deployersActive.StopAll();

                // Some stuff requires the old services to be stopped in order to be deployed, such as IIS bindings and certificates
                deployers.BeforeDoneAll();

                var settingsToDeploy = deployment.getRuntimeSettingsToDeploy();

                // Store Key-Value settings in a JSON object (with keys as 
                var jsonSettings = JsonConvert.SerializeObject(
                    settingsToDeploy,
                    Formatting.Indented);

                var jsonSettingsNested = JsonConvert.SerializeObject(
                    settingsConverter.keyValueToNested(settingsToDeploy),
                    Formatting.Indented);

                // Make sure we persist the settings AFTER all deployers have finished thri job
                deployers.DeploySettingsAll(jsonSettings, jsonSettingsNested);

                // Time to start!
                deployers.StartAll();

                DateTime dtStart = DateTime.Now;

                // Replace active configuration settings
                deployment.StoreInPath(activeDeploymentPathStorage);

                // Quitar el deployment anterior y si hay error seguir,
                // ya que los datos del deployment actual YA están guardados!
                deployersActive.UndeployAll(true);

                // The done "event" is called on deployers
                // once everything is completed correctly. Called
                // for cleanup purposes or non critical operations
                // (such as friendly names on IIS site names)
                deployers.DoneAll(true);
                services.DoneAll(true);

                // Make sure that at least 2 seconds pass after deployment before
                // doing an OK to let IIS reconfigure.
                while ((DateTime.Now - dtStart).TotalSeconds < 1)
                {
                    System.Threading.Thread.Sleep(500);
                }

            }
            catch (Exception e)
            {
                // Just in case.... log this ASAP
                logger.LogException(new Exception("Error deploying APP: " + deployment.installedApplicationSettings.getId(), e));

                deployers.StopAll(true);
                deployers.UndeployAll(true);

                // Aquí hacemos un continue on error porque... estamos repescando algo que ya funcionaba
                // a toda costa queremos levantarlo!
                deployersActive.StartAll(true);

                throw;
            }

            logger.LogInfo(false, "Deployment lapsed: {0}s", (DateTime.Now - start).TotalSeconds);

            return deployment;
        }
    }
}
