﻿using ACMESharp;
using ACMESharp.ACME;
using ACMESharp.JOSE;
using ACMESharp.PKI;
using ACMESharp.PKI.RSA;
using iischef.logger;
using iischef.utils;
using iischef.utils.Certificates;
using Microsoft.Web.Administration;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.AccessControl;
using System.Security.Cryptography.X509Certificates;
using System.Security.Principal;

namespace iischef.core.IIS
{
    /// <summary>
    /// Utility class to provision certificates for IIS
    /// </summary>
    public class SslCertificateProviderService
    {
        /// <summary>
        /// 
        /// </summary>
        protected const string AcmeStaging = "https://acme-staging.api.letsencrypt.org/";

        /// <summary>
        /// 
        /// </summary>
        protected const string AcmeLive = "https://acme-v01.api.letsencrypt.org/";

        /// <summary>
        /// The URI used to provision let's encrypt certificates
        /// </summary>
        protected string AcmeUri = null;

        /// <summary>
        /// The certificate password
        /// </summary>
        public string CertPassword;

        /// <summary>
        /// The logging service
        /// </summary>
        protected LoggerInterface Logger;

        /// <summary>
        /// The prefix that will be used for all the friendly names
        /// used in the certificates provisioned for this application
        /// </summary>
        protected string CertificateAppPrefix;

        /// <summary>
        /// The chef applicaiton id
        /// </summary>
        protected string AppId;

        /// <summary>
        /// Central store path for certificates
        /// </summary>
        protected string CentralStorePath
        {
            get
            {
                // We need to read the registry to check that this is enabled
                // in IIS and that the path is properly configured
                int enabled = Convert.ToInt32(UtilsProcess.GetRegistryKeyValue64(
                    RegistryHive.LocalMachine,
                    "SOFTWARE\\Microsoft\\IIS\\CentralCertProvider",
                    "Enabled",
                    0));

                if (enabled != 1)
                {
                    throw new Exception("IIS Central store path not enabled or installed. Please check https://blogs.msdn.microsoft.com/kaushal/2012/10/11/central-certificate-store-ccs-with-iis-8-windows-server-2012/");
                }

                string certStoreLocation = Convert.ToString(UtilsProcess.GetRegistryKeyValue64(
                    RegistryHive.LocalMachine,
                    "SOFTWARE\\Microsoft\\IIS\\CentralCertProvider",
                    "CertStoreLocation",
                    ""));

                if (string.IsNullOrWhiteSpace(certStoreLocation))
                {
                    throw new Exception("IIS Central store location not configured");
                }

                return certStoreLocation;
            }
        }

        /// <summary>
        /// If this is a mock environment (AppVeyor, tests, etc.) the provisioning
        /// emits self-signed certificates.
        /// </summary>
        protected bool MockEnvironment;

        /// <summary>
        /// Get an instance of SslCertificateProviderService
        /// </summary>
        /// <param name="logger">The logger service</param>
        /// <param name="appId">The applicaiton Id</param>
        /// <param name="certificatePassword">A unique password used to manage the certificates</param>
        public SslCertificateProviderService(
            LoggerInterface logger,
            string appId,
            string certificatePassword)
        {
            this.MockEnvironment = UtilsSystem.RunningInContinuousIntegration() || UnitTestDetector.IsRunningInTests;

            // Configure the ACME uri depending on the current environment settings
            if (this.MockEnvironment)
            {
                this.AcmeUri = AcmeStaging;
            }
            else
            {
                this.AcmeUri = AcmeLive;
            }

            this.Logger = logger;
            this.AppId = appId;
            this.CertPassword = certificatePassword;
        }

        /// <summary>
        /// Load a certificate signer from a file
        /// </summary>
        /// <param name="signer"></param>
        /// <param name="signerPath"></param>
        protected void LoadSignerFromFile(RS256Signer signer, string signerPath)
        {
            Logger.LogInfo(true, $"Loading Signer from {signerPath}");
            using (var signerStream = File.OpenRead(signerPath))
            {
                signer.Load(signerStream);
            }
        }

        /// <summary>
        /// Provisions a certificate in the central store
        /// </summary>
        /// <param name="storagePath"></param>
        /// <param name="mainDomain"></param>
        /// <param name="email"></param>
        /// <param name="bindingInfo"></param>
        /// <returns>The certificate's friendly name, ready to be bound in IIS</returns>
        public void ProvisionCertificateInIis(
            string storagePath,
            string mainDomain,
            string email,
            string bindingInfo)
        {
            // Make sure that we have a valid certificate...
            var certPath = Path.Combine(this.CentralStorePath, mainDomain + ".pfx");

            X509Certificate2 originalCert = null;
            X509Certificate2Collection collection = new X509Certificate2Collection();

            if (File.Exists(certPath))
            {
                collection.Import(certPath, null, X509KeyStorageFlags.PersistKeySet);
                originalCert = collection[0];
            }

            // If we have more than 60 days left for this certificate, do not try to renew
            if ((originalCert?.NotAfter - DateTime.Now)?.TotalDays > 30)
            {
                return;
            }

            if ((originalCert?.NotAfter - DateTime.Now)?.TotalDays < 2)
            {
                originalCert = null;
            }

            // Ssl configuration is global for all applications (signer, registrar, etc..)
            string configPath = UtilsSystem.ensureDirectoryExists(
                UtilsSystem.CombinePaths(storagePath, "ssl_config", mainDomain), true);

            // Make sure the vault is initialized...
            var vault = ACMESharp.POSH.Util.VaultHelper.GetVault(":user");

            if (!vault.TestStorage())
            {
                try
                {
                    vault.InitStorage();
                }
                catch
                {
                    // ignored
                }
            }

            using (var signer = new RS256Signer())
            {
                signer.Init();

                // (2)Start by initializing a Vault to store your Certificates and related artifacts.
                var signerPath = Path.Combine(configPath, "Signer");

                if (File.Exists(signerPath))
                {
                    LoadSignerFromFile(signer, signerPath);
                }

                using (var acmeClient = new AcmeClient(new Uri(AcmeUri), new AcmeServerDirectory(), signer: signer))
                {
                    acmeClient.Init();
                    acmeClient.GetDirectory(true);

                    var registrationPath = Path.Combine(configPath, "Registration");

                    if (File.Exists(registrationPath))
                    {
                        using (var registrationStream = File.OpenRead(registrationPath))
                        {
                            acmeClient.Registration = AcmeRegistration.Load(registrationStream);
                        }
                    }
                    else
                    {
                        Logger.LogInfo(true, "Calling Register");

                        AcmeRegistration registration = acmeClient.Register(new string[] { "mailto:" + email });

                        Logger.LogInfo(true, "Updating Registration");

                        acmeClient.UpdateRegistration(useRootUrl: true, agreeToTos: true);

                        Logger.LogInfo(true, "Saving Registration");

                        using (var registrationStream = File.OpenWrite(registrationPath))
                        {
                            acmeClient.Registration.Save(registrationStream);
                        }

                        Logger.LogInfo(true, "Saving Signer");

                        using (var signerStream = File.OpenWrite(signerPath))
                        {
                            signer.Save(signerStream);
                        }
                    }

                    ////// Grab the intermediate certificate...
                    ////string intermediatecertificateX3 = null;

                    ////// Bajar el certificado intermedio.
                    ////using (WebClient clientCrtItermediate = new WebClient())
                    ////{
                    ////    intermediatecertificateX3 = clientCrtItermediate.DownloadString("https://letsencrypt.org/certs/lets-encrypt-x3-cross-signed.pem.txt");
                    ////}

                    var authzState = acmeClient.AuthorizeIdentifier(mainDomain);
                    var challenge = acmeClient.DecodeChallenge(authzState, AcmeProtocol.CHALLENGE_TYPE_HTTP);
                    var httpChallenge = (HttpChallenge)challenge.Challenge;

                    // This is a little bit inconvenient but... the most reliable and compatible
                    // way to do this is to setup a custom IIS website that uses the binding during
                    // provisioning.

                    // We could add a global HTTPModule in IIS but that would require extra setup
                    // and a more complex maintenance...

                    long tempSiteId;
                    List<Site> haltedSites = new List<Site>();

                    // Find a site with our binding and stop it.
                    using (ServerManager sm = new ServerManager())
                    {
                        foreach (var site in sm.Sites)
                        {
                            foreach (var binding in site.Bindings)
                            {
                                if (binding.Host == mainDomain)
                                {
                                    haltedSites.Add(site);
                                    break;
                                }
                            }
                        }

                        // Stop the sites (should only be one...)
                        foreach (var site in haltedSites)
                        {
                            UtilsAppPool.WebsiteAction(site.Name, AppPoolActionType.Stop);
                        }

                        // Create a phantom website only to serve the file...
                        var tempDir = Path.Combine(storagePath, "webroot_" + Guid.NewGuid());
                        UtilsSystem.DirectoryCreateIfNotExists(tempDir);

                        // Grant access to everyone
                        DirectorySecurity ds = Directory.GetAccessControl(tempDir);
                        var id = new SecurityIdentifier("S-1-1-0");
                        ds.AddAccessRule(new FileSystemAccessRule(id, FileSystemRights.FullControl, AccessControlType.Allow));
                        Directory.SetAccessControl(tempDir, ds);

                        var tempSite = sm.Sites.Add("letsencrypt-" + this.AppId, tempDir, 80);
                        tempSite.Bindings.RemoveAt(0);
                        tempSite.Bindings.Add(bindingInfo, "http");
                        tempSiteId = tempSite.Id;

                        // Prepare the website contents
                        var sourceDir = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "IIS");
                        UtilsSystem.CopyFilesRecursively(new DirectoryInfo(sourceDir), new DirectoryInfo(tempDir), false);

                        File.WriteAllText(Path.Combine(tempDir, httpChallenge.FilePath), httpChallenge.FileContent);

                        sm.CommitChanges();
                    }

                    try
                    {
                        // Validate that we can actually access the challenge ourselves!
                        var contents = UtilsSystem.DownloadUriAsText(httpChallenge.FileUrl);
                        if (!string.Equals(contents, httpChallenge.FileContent))
                        {
                            throw new Exception("Could not validate ACME challenge.");
                        }

                        // If we are running in continuous integration or during tests,
                        // we cannot complete the process. Just use a self-signed certificate
                        // and assume that the rest of the provisining process does not have
                        // test coverage :(
                        if (this.MockEnvironment)
                        {
                            // Creating the self-signed certificate already enrolls it in the local certificate store :)
                            var cert = UtilsCertificate.CreateSelfSignedCertificate(mainDomain, mainDomain);
                            var tmpPfx = Path.GetTempFileName();
                            File.WriteAllBytes(tmpPfx, cert.Export(X509ContentType.Pfx, ""));
                            File.Copy(tmpPfx, certPath, true);
                            File.Delete(tmpPfx);
                            return;
                        }

                        // (6) Submit the Challenge Response to Prove Domain Ownership
                        authzState.Challenges = new AuthorizeChallenge[] { challenge };

                        // Submit-ACMEChallenge dns1 -ChallengeType http-01
                        var authchallenge =
                            acmeClient.SubmitChallengeAnswer(authzState, AcmeProtocol.CHALLENGE_TYPE_HTTP, true);

                        // Esperar a que Let's Encrypt confirme que has superado el challange
                        while ("pending".Equals(authzState.Status, StringComparison.CurrentCultureIgnoreCase))
                        {
                            System.Threading.Thread.Sleep(3000);

                            var newAuthzState = acmeClient.RefreshIdentifierAuthorization(authzState);

                            if (newAuthzState.Status != "pending")
                            {
                                authzState = newAuthzState;
                            }
                        }

                        if (authzState.Status != "valid")
                        {
                            Logger.LogWarning(true,
                                $"Could not authorize domain {mainDomain} (status:{authzState.Status})");

                            // If we have a valid certificate.. return it
                            if (originalCert != null)
                            {
                                return;
                            }

                            throw (new Exception(
                                $"Could not authorize domain {mainDomain} (status:{authzState.Status})"));
                        }

                        // Download the certificates to this temp location
                        string temporaryCertificatePath = UtilsSystem.ensureDirectoryExists(
                            UtilsSystem.CombinePaths(storagePath, AppId, "ssl_certificates", mainDomain), true);

                        var certificatepaths =
                            DownloadCertificate(
                                UtilsIis.CalculateMD5Hash(mainDomain),
                                mainDomain,
                                acmeClient,
                                temporaryCertificatePath);

                        // Save this!
                        File.Copy(certificatepaths.pfxPemFile, certPath, true);

                        // Once installed, we can delete the original file path to the certificate
                        UtilsSystem.deleteDirectory(temporaryCertificatePath, false);

                        return;
                    }
                    finally
                    {
                        // Restore the original state of IIS!!!
                        using (ServerManager sm = new ServerManager())
                        {
                            sm.Sites.Remove(sm.Sites.Where((i) => i.Id == tempSiteId).Single());
                            sm.CommitChanges();
                        }

                        // Start the sites
                        foreach (var site in haltedSites)
                        {
                            UtilsAppPool.WebsiteAction(site.Name, AppPoolActionType.Start);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Generate a certificate request
        /// </summary>
        /// <param name="certificatename"></param>
        /// <param name="mainhost">Main host: www.google.com</param>
        /// <param name="acmeClient"></param>
        /// <param name="certificatePath">Path to store the generated certificates</param>
        /// <param name="alternatehosts">Alterante hosts: list of alterante hosts for this certificate</param>
        /// <returns></returns>
        private CertificatePaths DownloadCertificate(
            string certificatename,
            string mainhost,
            AcmeClient acmeClient,
            string certificatePath,
            List<string> alternatehosts = null)
        {
            if (alternatehosts != null && alternatehosts.Any())
            {
                throw new NotSupportedException("Alternate host provisioning not supported yet.");
            }

            List<string> allDnsIdentifiers = new List<string>() { mainhost };

            if (alternatehosts != null)
            {
                allDnsIdentifiers.AddRange(alternatehosts);
            }

            // Tomado de app.config
            var RSAKeyBits = 2048; // 1024;//

            if (Environment.Is64BitProcess)
            {
                CertificateProvider.RegisterProvider(typeof(ACMESharp.PKI.Providers.OpenSslLib64Provider));
            }
            else
            {
                CertificateProvider.RegisterProvider(typeof(ACMESharp.PKI.Providers.OpenSslLib32Provider));
            }

            var cp = CertificateProvider.GetProvider();
            var rsaPkp = new RsaPrivateKeyParams();
            try
            {
                if (RSAKeyBits >= 1024)
                {
                    rsaPkp.NumBits = RSAKeyBits;
                    //Log.Debug("RSAKeyBits: {RSAKeyBits}", Properties.Settings.Default.RSAKeyBits);
                }
                else
                {
                    //Log.Warning(
                    //    "RSA Key Bits less than 1024 is not secure. Letting ACMESharp default key bits. http://openssl.org/docs/manmaster/crypto/RSA_generate_key_ex.html");
                }
            }
            catch (Exception ex)
            {
                Logger.LogInfo(true,
                    $"Unable to set RSA Key Bits, Letting ACMESharp default key bits, Error: {ex.Message.ToString()}");
            }

            var rsaKeys = cp.GeneratePrivateKey(rsaPkp);
            var csrDetails = new CsrDetails
            {
                CommonName = allDnsIdentifiers[0]
            };

            if (alternatehosts != null)
            {
                if (alternatehosts.Count > 0)
                {
                    csrDetails.AlternativeNames = alternatehosts;
                }
            }

            var csrParams = new CsrParams
            {
                Details = csrDetails,
            };

            var csr = cp.GenerateCsr(csrParams, rsaKeys, Crt.MessageDigest.SHA256);

            byte[] derRaw;
            using (var bs = new MemoryStream())
            {
                cp.ExportCsr(csr, EncodingFormat.DER, bs);
                derRaw = bs.ToArray();
            }

            var derB64U = JwsHelper.Base64UrlEncode(derRaw);

            Logger.LogInfo(true, $"Requesting Certificate");

            //Log.Information("Requesting Certificate");
            var certRequ = acmeClient.RequestCertificate(derB64U);

            //Log.Debug("certRequ {@certRequ}", certRequ);

            Logger.LogInfo(true, $"Request Status: {certRequ.StatusCode}");

            if (certRequ.StatusCode != System.Net.HttpStatusCode.Created)
            {
                throw new Exception($"Could not create certificate request, response code: = {certRequ.StatusCode}");
            }

            var keyGenFile = Path.Combine(certificatePath, $"{certificatename}-gen-key.json");
            var keyPemFile = Path.Combine(certificatePath, $"{certificatename}-key.pem");
            var csrGenFile = Path.Combine(certificatePath, $"{certificatename}-gen-csr.json");
            var csrPemFile = Path.Combine(certificatePath, $"{certificatename}-csr.pem");
            var crtDerFile = Path.Combine(certificatePath, $"{certificatename}-crt.der");
            var crtPemFile = Path.Combine(certificatePath, $"{certificatename}-crt.pem");
            var chainPemFile = Path.Combine(certificatePath, $"{certificatename}-chain.pem");
            var pfxPemFile = Path.Combine(certificatePath, $"{certificatename}.pfx");

            using (var fs = new FileStream(keyGenFile, FileMode.Create))
            {
                cp.SavePrivateKey(rsaKeys, fs);
            }

            using (var fs = new FileStream(keyPemFile, FileMode.Create))
            {
                cp.ExportPrivateKey(rsaKeys, EncodingFormat.PEM, fs);
            }

            using (var fs = new FileStream(csrGenFile, FileMode.Create))
            {
                cp.SaveCsr(csr, fs);
            }

            using (var fs = new FileStream(csrPemFile, FileMode.Create))
            {
                cp.ExportCsr(csr, EncodingFormat.PEM, fs);
            }

            Logger.LogInfo(true, $"Saving Certificate to {crtDerFile}");

            //Log.Information("Saving Certificate to {crtDerFile}", crtDerFile);
            using (var file = File.Create(crtDerFile))
            {
                certRequ.SaveCertificate(file);
            }

            Crt crt;

            using (FileStream source = new FileStream(crtDerFile, FileMode.Open),
                target = new FileStream(crtPemFile, FileMode.Create))
            {
                crt = cp.ImportCertificate(EncodingFormat.DER, source);
                cp.ExportCertificate(crt, EncodingFormat.PEM, target);
            }

            cp.Dispose();

            var ret = new CertificatePaths()
            {
                chainPemFile = chainPemFile,
                crtDerFile = crtDerFile,
                crtPemFile = crtPemFile,
                csrGenFile = csrGenFile,
                csrPemFile = csrPemFile,
                keyGenFile = keyGenFile,
                keyPemFile = keyPemFile,
                name = certificatename,
                pfxPemFile = pfxPemFile
            };

            UtilsCertificate.CreatePfXfromPem(ret, CertPassword);

            return ret;
        }
    }
}
