﻿using System.Collections.Generic;
using System.Management.Automation;

namespace iischef.cmdlet
{
    [Cmdlet(VerbsCommon.Set, "ChefEnvironmentFile")]
    [OutputType(typeof(List<core.Configuration.InstalledApplication>))]
    public class EnvironmentPathSet : Cmdlet
    {
        [Parameter(Position = 1, ValueFromPipelineByPropertyName = true, Mandatory = true)]
        public string Path { get; set; }

        protected override void ProcessRecord()
        {
            var logger = new logger.ConsoleLogger();
            logger.setVerbose(true);
            var app = new core.Application(logger);
            app.useParentLogger();
            app.setGlobalEnvironmentFilePath(Path);
        }
    }
}
