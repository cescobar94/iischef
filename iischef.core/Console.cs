﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Management.Automation;
using System.Collections.ObjectModel;

namespace iischef.core
{

    public class Console : IDisposable
    {

        protected PowerShell ps;

        public Console()
        {
            ps = PowerShell.Create();
        }

        /// <summary>
        /// Runs a script synchronously
        /// </summary>
        /// <param name="command"></param>
        public void RunCommand(string command)
        {
            ps.AddScript(command);
            ps.Invoke();

            if (ps.Streams.Error.Count > 0)
            {
                throw new Exception("Error while running console commands.");
            }
        }

        public void Dispose()
        {
            if (ps != null)
            {
                ps.Dispose();
                ps = null;
            }
        }
    }


}
