﻿using System.Collections.Generic;
using System.Management.Automation;

namespace iischef.cmdlet
{
    [Cmdlet(VerbsLifecycle.Invoke, "ChefAppRemove")]
    [OutputType(typeof(List<iischef.core.Configuration.InstalledApplication>))]
    public class ChefAppDelete : Cmdlet
    {
        [Parameter(Position = 1, ValueFromPipelineByPropertyName = true, Mandatory = true)]
        public string Id { get; set; }

        [Parameter]
        public SwitchParameter Force { get; set; }

        protected override void ProcessRecord()
        {
            var app = ConsoleUtils.getApplicationForConsole();
            app.RemoveAppById(Id, Force);
        }
    }
}
