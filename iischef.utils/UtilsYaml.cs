﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YamlDotNet.Serialization;
using Newtonsoft.Json.Linq;
using System.IO;

namespace iischef.utils
{
    public class UtilsYaml
    {
        /// <summary>
        /// Converts a YAML configuration file to a JTOKEN
        /// compatible JSON representation
        /// </summary>
        /// <typeparam name="TTokenType"></typeparam>
        /// <param name="source"></param>
        /// <returns></returns>
        public static TTokenType yamlToJtoken<TTokenType>(string source)
            where TTokenType : JToken
        {
            using (TextWriter writer = new StringWriter())
            {
                // convert string/file to YAML object
                var r = new StringReader(source);
                var deserializer = new Deserializer();
                var yamlObject = deserializer.Deserialize(r);
                var serializer = new Serializer(SerializationOptions.JsonCompatible);
                serializer.Serialize(writer, yamlObject);
                string jsonsource = writer.ToString();
                // Use our custom deserialization that converts arrays to keyed dictionaries ??
                return UtilsJson.DeserializeObject<TTokenType>(jsonsource);
            }
        }
    }
}
