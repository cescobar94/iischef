﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iischef.core.IIS
{
    public class Mount
    {
        /// <summary>
        /// Alias
        /// </summary>
        public string id { get; set; }

        /// <summary>
        /// Relative path from artifact
        /// </summary>
        public string path { get; set; }

        /// <summary>
        /// Only valid for NON root mounts. Tells the name
        /// of the virtual directory.
        /// </summary>
        public string mountpath { get; set; }

        /// <summary>
        /// If this is the root mount
        /// </summary>
        public bool root { get; set; }

        /// <summary>
        /// The pool to use. Use empty for default.
        /// </summary>
        public string pool { get; set; }

        /// <summary>
        /// Preload the application. For faster first use
        /// </summary>
        public bool preloadEnabled { get; set; }
    }
}
