﻿using iischef.utils;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;

namespace iischef.core.Storage
{
    public class AppBaseStorageDeployer : DeployerBase, DeployerInterface
    {
        public enum AppBaseStorageType
        {
            /// <summary>
            /// Points to original code
            /// </summary>
            Original = 0,

            /// <summary>
            /// Is a copy
            /// </summary>
            Transient = 1,

            /// <summary>
            /// Is a symlink
            /// </summary>
            Symlink = 2
        }

        /// <summary>
        /// List of configuration files were the runtime
        /// settings will be replaced.
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, string> getConfigurationReplacementFiles()
        {
            if (this.DeployerSettings["configuration_replacement_files"] == null)
            {
                return new Dictionary<string, string>();
            }

            return this.DeployerSettings["configuration_replacement_files"].castTo<Dictionary<string, string>>();
        }

        /// <summary>
        /// List of paths to dump the full configuration files to.
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, string> getConfigurationDumpPaths()
        {
            if (this.DeployerSettings["configuration_dump_paths"] == null)
            {
                return new Dictionary<string, string>();
            }

            return this.DeployerSettings["configuration_dump_paths"].castTo<Dictionary<string, string>>();
        }

        /// <summary>
        /// Convert a JSON string document to YAML
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        protected string JsonToYaml(string source)
        {
            var expConverter = new ExpandoObjectConverter();
            var serializer = new YamlDotNet.Serialization.Serializer();

            object data = Newtonsoft.Json.JsonConvert.DeserializeObject<ExpandoObject>(source, expConverter);

            using (var writer = new StringWriter())
            {
                serializer.Serialize(writer, data);
                var yaml = writer.ToString();
                return yaml;
            }
        }

        public void deploy()
        {
            var oldDeployment = Deployment.getPreviousDeployment();

            if (oldDeployment?.windowsUsername != null)
            {
                Deployment.windowsUsername = oldDeployment.windowsUsername;
            }
            else
            {
                Deployment.windowsUsername = "chf_" + Deployment.installedApplicationSettings.getId();

                if (Deployment.windowsUsername.Length > 20)
                {
                    Deployment.windowsUsername = "chf_" + UtilsEncryption
                                                     .GetMD5(Deployment.installedApplicationSettings.getId())
                                                     .Substring(0, 20 - 7);
                }
            }

            // On Linux all users have - by default - the right to create symlinks.
            // To reproduce this on Windows we will create a group for the identities
            // managed by chef, and grant this group the right to create symlinks.
            // TODO: Ideally we should be able to assign policies to the account through the config file...
            UtilsWindowsAccounts.CreateUser(Deployment.windowsUsername, Deployment.getWindowsPassword(), this.Logger);
            UtilsWindowsAccounts.EnsureGroupExists("chef_users");
            UtilsWindowsAccounts.SetRight("chef_users", "SeCreateSymbolicLinkPrivilege");
            UtilsWindowsAccounts.EnsureUserInGroup(Deployment.windowsUsername, "chef_users");

            // We need a system user account for the application....
            string basePath = UtilsSystem.CombinePaths(GlobalSettings.GetDefaultApplicationStorage().path, Deployment.getShortId());

            // Store this in the application storage location.
            Deployment.runtimePath = UtilsSystem.CombinePaths(basePath, "runtime");

            // These should be persisted through deployments
            Deployment.logPath = UtilsSystem.CombinePaths(GlobalSettings.GetDefaultLogStorage().path, Deployment.installedApplicationSettings.getId());
            Deployment.tempPath = UtilsSystem.CombinePaths(GlobalSettings.GetDefaultTempStorage().path, Deployment.installedApplicationSettings.getId());

            Deployment.setSetting("appstorage.base", basePath);
            Deployment.setSetting("appstorage.temp", Deployment.tempPath);
            Deployment.setSetting("appstorage.log", Deployment.logPath);

            UtilsSystem.ensureDirectoryExists(Deployment.runtimePath, true);
            UtilsSystem.ensureDirectoryExists(Deployment.logPath, true);
            UtilsSystem.ensureDirectoryExists(Deployment.tempPath, true);

            // We use this flag to detect transient storage
            // that must be removed when the deployer is "undeployed".
            AppBaseStorageType appBaseStorageType = AppBaseStorageType.Original;

            // TODO: Make this configurable through the chef.yml settings file.
            string ignoreOnDeployPattern = "^\\.git\\\\|^chef\\\\|^\\.vs\\\\";

            switch (Deployment.installedApplicationSettings.getApplicationMountStrategy())
            {
                case ApplicationMountStrategy.Copy:
                    Deployment.appPath = UtilsSystem.CombinePaths(basePath, "app");
                    // TODO: We should consider the ability to symlink the code here, or to point/mount directly
                    // to the original source path. This would probably require delegating this step to the artifact downloader
                    // (artifact.getDownloader()) or having the downloader tell us how to deal with this (symlinks, direct, whatever)
                    UtilsSystem.CopyFilesRecursivelyFast(
                        new DirectoryInfo(Deployment.artifact.localPath),
                        new DirectoryInfo(Deployment.appPath),
                        false, ignoreOnDeployPattern);
                    Logger.LogInfo(true, "Ensure app has proper user permissions for account '{0}'", Deployment.windowsUsername);
                    UtilsWindowsAccounts.AddPermissionToDirectoryIfMissing(Deployment.windowsUsername, Deployment.appPath);
                    Deployment.artifact.DeleteIfRemote();
                    appBaseStorageType = AppBaseStorageType.Transient;
                    break;
                case ApplicationMountStrategy.Move:
                    Deployment.appPath = utils.UtilsSystem.CombinePaths(basePath, "app");
                    // TODO: We should consider the ability to symlink the code here, or to point/mount directly
                    // to the original source path. This would probably require delegating this step to the artifact downloader
                    // (artifact.getDownloader()) or having the downloader tell us how to deal with this (symlinks, direct, whatever)
                    UtilsSystem.moveDirectory(Deployment.artifact.localPath, Deployment.appPath, ignoreOnDeployPattern);
                    // We had issues in appveyor where _webs location is in C drive and thus not giving
                    // permissions here would make tests fail.
                    Logger.LogInfo(true, "Ensure app has proper user permissions for account '{0}'", Deployment.windowsUsername);
                    UtilsWindowsAccounts.AddPermissionToDirectoryIfMissing(Deployment.windowsUsername, Deployment.appPath);
                    Deployment.artifact.DeleteIfRemote();
                    appBaseStorageType = AppBaseStorageType.Transient;
                    break;
                case ApplicationMountStrategy.Link:
                    Deployment.appPath = UtilsSystem.CombinePaths(basePath, "app");
                    var linkManager = NCode.ReparsePoints.ReparsePointFactory.Create();
                    var mounted = UtilsJunction.EnsureJuction(Deployment.appPath, Deployment.artifact.localPath, this.Logger, false);
                    if (!mounted)
                    {
                        throw new Exception("Could not mount junction: " + Deployment.appPath);
                    }
                    Logger.LogInfo(true, "Ensure app has proper user permissions for account '{0}'", Deployment.windowsUsername);
                    UtilsWindowsAccounts.AddPermissionToDirectoryIfMissing(Deployment.windowsUsername, Deployment.artifact.localPath);
                    appBaseStorageType = AppBaseStorageType.Symlink;
                    break;
                case ApplicationMountStrategy.Original:
                    Logger.LogInfo(true, "Ensure app has proper user permissions for account '{0}'", Deployment.windowsUsername);
                    UtilsWindowsAccounts.AddPermissionToDirectoryIfMissing(Deployment.windowsUsername, Deployment.artifact.localPath);
                    Deployment.appPath = UtilsSystem.CombinePaths(Deployment.artifact.localPath);
                    appBaseStorageType = AppBaseStorageType.Original;
                    break;
                default:
                    throw new NotImplementedException("The requested mount strategy for the application is not available: " + Deployment.installedApplicationSettings.getApplicationMountStrategy());
            }

            Deployment.setRuntimeSetting("deployment.appPath", Deployment.appPath);
            Deployment.setRuntimeSetting("deployment.logPath", Deployment.logPath);
            Deployment.setRuntimeSetting("deployment.tempPath", Deployment.tempPath);

            Deployment.setSetting("appstorage.appBaseStorageType", appBaseStorageType);

            UtilsWindowsAccounts.AddPermissionToDirectoryIfMissing(Deployment.windowsUsername, Deployment.tempPath);
            UtilsWindowsAccounts.AddPermissionToDirectoryIfMissing(Deployment.windowsUsername, Deployment.logPath);
            UtilsWindowsAccounts.AddPermissionToDirectoryIfMissing(Deployment.windowsUsername, Deployment.runtimePath);
        }

        public void undeploy(bool isUninstall = false)
        {
            if (Deployment == null)
            {
                return;
            }

            AppBaseStorageType appBaseStorageType = Deployment.getSetting<AppBaseStorageType>(
                "appstorage.appBaseStorageType",
                AppBaseStorageType.Original,
                this.Logger,
                true);

            string appStorageBasePath = Deployment.getSetting<string>("appstorage.base", null, this.Logger);

            switch (appBaseStorageType)
            {
                case AppBaseStorageType.Transient:
                    UtilsSystem.deleteDirectory(appStorageBasePath, false, 20);
                    break;
                case AppBaseStorageType.Symlink:
                    UtilsJunction.RemoveJunction(Deployment.appPath);
                    UtilsSystem.deleteDirectory(appStorageBasePath, false, 20);
                    break;
                case AppBaseStorageType.Original:
                    // Do nothing.
                    break;
                default:
                    throw new Exception("Option not supported.");
            }

            if (!isUninstall)
            {
                return;
            }

            // Usually the IIS site has been closed a few fractions of a second
            // before this is called, so the folders have probably not yet
            // been released, waitPauseMs at least 10 seconds.
            UtilsSystem.deleteDirectory(Deployment.getSetting<string>("appstorage.temp", null, this.Logger), true, 10);
            UtilsSystem.deleteDirectory(Deployment.getSetting<string>("appstorage.log", null, this.Logger), true, 10);

            UtilsWindowsAccounts.DeleteUser(Deployment.windowsUsername);
        }

        /// <summary>
        /// Deploy the application runtime settings.
        /// </summary>
        /// <param name="jsonSettings"></param>
        /// <param name="jsonSettingsArray"></param>
        /// <param name="replacer"></param>
        public void deploySettings(string jsonSettings,
            string jsonSettingsArray,
            RuntimeSettingsReplacer replacer)
        {
            // Write the settings in a directory in the application folder itself.
            // When deployed as web app or similar, the other deployers must hide this directory...
            var settingsFile = UtilsSystem.ensureDirectoryExists(
                Path.Combine(Deployment.runtimePath, "chef-settings.json"));

            File.WriteAllText(settingsFile, jsonSettings);

            var settingsFileNested = UtilsSystem.ensureDirectoryExists(
                Path.Combine(Deployment.runtimePath, "chef-settings-nested.json"));

            File.WriteAllText(settingsFileNested, jsonSettingsArray);

            // Why don't we write the settings directly to the AppRoot? Because it might
            // be exposed to the public if the application is mounted as a web application...
            // So we just hint to the location of the runtime, and the application
            // must implement the code needed to load the settings.
            var hintFile = UtilsSystem.ensureDirectoryExists(
                UtilsSystem.CombinePaths(Deployment.appPath, "chef-runtime.path"));

            // We hint to the runtime path, not the specific file
            File.WriteAllText(hintFile, Deployment.runtimePath);

            // Dump the configuration files if requested to do so...
            foreach (var kvp in getConfigurationDumpPaths())
            {
                var destinationDir = UtilsSystem.CombinePaths(Deployment.appPath, kvp.Value);
                if (!Directory.Exists(destinationDir))
                {
                    Directory.CreateDirectory(destinationDir);
                }

                var settingsFileDump = UtilsSystem.ensureDirectoryExists(
                Path.Combine(destinationDir, "chef-settings.json"));

                File.WriteAllText(settingsFileDump, jsonSettings);

                var settingsFileNestedDump = UtilsSystem.ensureDirectoryExists(
                    Path.Combine(destinationDir, "chef-settings-nested.json"));

                File.WriteAllText(settingsFileNestedDump, jsonSettingsArray);

                var settingsFileNestedYaml = UtilsSystem.ensureDirectoryExists(
                    Path.Combine(destinationDir, "chef-settings-nested.yml"));

                File.WriteAllText(settingsFileNestedYaml, JsonToYaml(jsonSettingsArray));
            }

            // Now replace the settings in the configuration templates
            foreach (var kvp in getConfigurationReplacementFiles())
            {
                var sourcePath = UtilsSystem.CombinePaths(Deployment.appPath, kvp.Key);
                var destinationPath = UtilsSystem.CombinePaths(Deployment.appPath, kvp.Value);

                var contents = File.ReadAllText(sourcePath);

                if (destinationPath == sourcePath)
                {
                    throw new Exception("Destination and source for configuration settings replacements cannot be the same.");
                }

                contents = replacer.doReplace(contents);

                File.WriteAllText(destinationPath, contents);
            }
        }

        public void start()
        {

        }

        public void stop()
        {

            if (Deployment == null)
            {
                return;
            }
        }

        public void sync()
        {

        }
    }
}
