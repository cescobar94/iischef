﻿using Microsoft.SqlServer.Management.Common;
using Microsoft.SqlServer.Management.Smo;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace iischef.utils
{
    public class UtilsSQLServer
    {
        /// <summary>
        /// The logger service
        /// </summary>
        protected logger.LoggerInterface logger;

        /// <summary>
        /// Get an instance of UtilsSQLServer
        /// </summary>
        public UtilsSQLServer(logger.LoggerInterface logger)
        {
            this.logger = logger;
        }

        public ServerConnection getServerConnection(string connectionString)
        {
            SqlConnection conn = new SqlConnection(connectionString);
            ServerConnection connection = new ServerConnection(conn);

            // Test the connection
            var s = findServer(connection);

            if (s == null)
            {
                return null;
            }

            return connection;
        }

        /// <summary>
        /// Find a server using connection settings
        /// </summary>
        /// <param name="connection"></param>
        /// <returns></returns>
        public Server findServer(ServerConnection connection)
        {
            var instance = new Server(connection);

            try
            {
                var a = instance.Version.Major;
            }
            catch (Exception e)
            {
                logger.LogException(e);
                return null;
            }

            return instance;
        }

        public void deleteDatabase(ServerConnection connection, string databasename)
        {
            if (string.IsNullOrWhiteSpace(databasename))
            {
                return;
            }

            var instance = findServer(connection);
            if (instance == null)
            {
                return;
            }


            Database database = null;


            foreach (Database d in instance.Databases)
            {
                if (d.Name == databasename)
                {
                    database = d;
                    break;
                }
            }

            // Creamos la base de datos
            if (database != null)
            {
                instance.KillDatabase(databasename);
            }

            return;
        }

        /// <summary>
        /// Find or create a database
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="databasename"></param>
        /// <param name="create"></param>
        /// <returns></returns>
        public Database findDatabase(ServerConnection connection, string databasename, bool create)
        {
            var instance = findServer(connection);
            if (instance == null)
            {
                return null;
            }

            Database database = null;

            foreach (Database d in instance.Databases)
            {
                if (d.Name == databasename)
                {
                    database = d;
                    break;
                }
            }

            // Creamos la base de datos
            if (database == null && create)
            {
                // Default to simple recovery model.
                database = new Database(instance, databasename);
                database.RecoveryModel = RecoveryModel.Simple;
                database.Create();
            }

            return database;
        }

        /// <summary>
        /// Create or update a login
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="databasename"></param>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="create"></param>
        /// <returns></returns>
        public Login findLogin(ServerConnection connection, string databasename, string username, string password, bool create)
        {
            var instance = findServer(connection);

            if (instance == null)
            {
                throw new Exception("SQL Server not found with connection: " + connection);
            }

            if (instance.LoginMode != ServerLoginMode.Mixed)
            {
                throw new Exception("Please, configure your SQL Server for Mixed Mode authentication: https://stackoverflow.com/questions/1393654/how-can-i-change-from-sql-server-windows-mode-to-mixed-mode-sql-server-2008");
            }

            Login login = null;

            foreach (Login l in instance.Logins)
            {
                if (l.Name == username)
                {
                    login = l;
                    break;
                }
            }

            if (login == null && create)
            {
                login = new Login(instance, username);
                login.LoginType = LoginType.SqlLogin;
                login.PasswordExpirationEnabled = false;
                login.PasswordPolicyEnforced = false;
                login.Create(password);
            }

            else if (login != null)
            {
                login.ChangePassword(password);
            }

            return login;
        }

        public void deleteLogin(ServerConnection connection, string username)
        {
            var instance = findServer(connection);

            Login login = null;

            foreach (Login l in instance.Logins)
            {
                if (l.Name == username)
                {
                    login = l;
                    break;
                }
            }

            if (login != null)
            {
                login.Drop();
            }

        }

        /// <summary>
        /// Find or create a user
        /// </summary>
        /// <param name="database"></param>
        /// <param name="login"></param>
        /// <param name="create"></param>
        /// <param name="removeWithPrefix"></param>
        /// <returns></returns>
        public User BindUser(Database database, Login login, bool create)
        {
            User result = null;
            bool mapped = false;

            foreach (User user in database.Users)
            {
                if (login.Name == user.Name
                    && user.LoginType == LoginType.SqlLogin)
                {
                    result = user;
                    mapped = !string.IsNullOrWhiteSpace(user.Login);
                    break;
                }
            }

            // If the user is not mapped... delete and recreate, it probably
            // comes from an external backup or similar.
            // A null value here represents an empty mapping.
            if (!mapped && result != null)
            {
                // Big chance this is an invalid user (i.e. a restore
                // between servers or similar) so we drop
                // and recreate.
                result.Drop();
                result = null;
            }

            if (result == null && create)
            {
                result = new User(database, login.Name);
                result.Login = login.Name;
                result.Name = login.Name;
                result.DefaultSchema = "dbo";
                result.Create();
            }

            List<string> roles = new List<string>() {
                 "db_datawriter",
                 "db_datareader",
                 "db_ddladmin",
                 // TODO: Find how to deal with the exec permission issue...
                 "db_owner"
                };

            // Add the roles!!
            foreach (var r in roles)
            {
                result.AddToRole(r);
            }

            return result;
        }

    }


}
