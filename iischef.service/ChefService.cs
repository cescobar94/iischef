﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Web.Administration;
using Microsoft.Web.Management;
using System.Security.Cryptography;
using iischef.core;
using Console = System.Console;

namespace iischef.service
{
    /// <summary>
    /// Main chef service
    /// </summary>
    public partial class ChefService : ServiceBase
    {
        /// <summary>
        /// Debug mode.
        /// </summary>
        protected bool Debug = false;

        /// <summary>
        /// Monitor instance.
        /// </summary>
        protected ApplicationService App;

        /// <summary>
        /// Get an instance of ChefService
        /// </summary>
        public ChefService()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Test startup
        /// </summary>
        /// <param name="args"></param>
        public void TestStartupAndStop(string[] args)
        {
            this.OnStart(args);
            Console.ReadLine();
            this.OnStop();
        }

        protected override void OnStart(string[] args)
        {
            // El método OnStart debe volver al sistema operativo después 
            // de que haya comenzado el funcionamiento del servicio.No debe bloquearse ni ejecutar un bucle infinito.
            App = new ApplicationService();
            App.Start();
        }

        protected override void OnStop()
        {
            // Stop the service!
            App?.Stop();
        }
    }
}
