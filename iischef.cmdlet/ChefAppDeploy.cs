﻿using System.Management.Automation;

namespace iischef.cmdlet
{
    [Cmdlet(VerbsLifecycle.Invoke, "ChefAppDeploy")]
    public class CheafAppDeploy : Cmdlet
    {
        [Parameter(Position = 1, ValueFromPipelineByPropertyName = true, Mandatory = true)]
        public string Path { get; set; }

        protected override void ProcessRecord()
        {
            var app = ConsoleUtils.getApplicationForConsole();
            app.DeploySingleAppFromFile(Path, true);
        }
    }
}
