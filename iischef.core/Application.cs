﻿using iischef.core.SystemConfiguration;
using iischef.logger;
using iischef.utils;
using System;
using System.Collections.Generic;
using System.Data.Sql;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;

namespace iischef.core
{
    /// <summary>
    /// This is the main application class for the service.
    /// 
    /// Designed to be run in state-less mode. Starts, does what it has to do,
    /// and get's killed.
    /// </summary>
    public class Application
    {
        public const string AutoDeployApplicationIdPrefix = "auto-";

        /// <summary>
        /// The global settings
        /// </summary>
        protected EnvironmentSettings globalSettings;

        /// <summary>
        /// The logger
        /// </summary>
        protected LoggerInterface logger;

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public EnvironmentSettings getGlobalSettings()
        {
            return globalSettings;
        }

        public LoggerInterface getLogger()
        {
            return logger;
        }

        /// <summary>
        /// Get an instance of Application
        /// </summary>
        /// <param name="parentLogger">Logger implementation</param>
        public Application(LoggerInterface parentLogger)
        {
            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                    | SecurityProtocolType.Tls11
                    | SecurityProtocolType.Tls12
                    | SecurityProtocolType.Ssl3;

            if (!UtilsSystem.isAdministrator())
            {
                parentLogger.LogError("Cannot start chef application without admin privileges.");
                throw new Exception("You must run the deployer with full privileges.");
            }

            // Use the parent logger, at least until we can build a file based one...
            logger = parentLogger;
        }

        public ApplicationDeployer getDeployer(Configuration.InstalledApplication app)
        {
            return new ApplicationDeployer(globalSettings, app, logger);
        }

        public Deployment DeploySingleAppFromInstalledApplication(Configuration.InstalledApplication installedApplication, bool force = false, string buildId = null, bool sync = false)
        {
            var deployer = getDeployer(installedApplication);
            var deployment = deployer.DeployApp(this, force, buildId, sync);
            return deployment;
        }

        public Deployment SyncSingleAppFromInstalledApplication(Configuration.InstalledApplication installedApplication)
        {
            var deployer = getDeployer(installedApplication);
            var deployment = deployer.SyncApp();
            return deployment;
        }

        /// <summary>
        /// Deploy a single app from it's YAML settings. For testing purposes only.
        /// </summary>
        /// <param name="settings"></param>
        public Deployment DeploySingleAppFromTextSettings(string settings, bool force = false, string buildId = null)
        {
            Configuration.InstalledApplication installedApplication = new Configuration.InstalledApplication();
            installedApplication.parseFromString(settings);
            return DeploySingleAppFromInstalledApplication(installedApplication, force, buildId, true);
        }

        /// <summary>
        /// Undeploy an application by it's id...
        /// </summary>
        /// <param name="id"></param>
        public void RemoveAppById(string id, bool force = false)
        {
            var deployment = ApplicationDeployer.getActiveDeploymentById(globalSettings, id);
            if (deployment == null)
            {
                logger.LogWarning(false, "Application not installed: " + id);
                return;
            }

            var appdeployer = new ApplicationDeployer(globalSettings, deployment.installedApplicationSettings, logger);
            appdeployer.UninstallApp(force);
        }

        public void RestartAppById(string id)
        {
            var deployment = ApplicationDeployer.getActiveDeploymentById(globalSettings, id);
            if (deployment == null)
            {
                logger.LogWarning(false, "Application not installed: " + id);
                return;
            }

            var appdeployer = new ApplicationDeployer(globalSettings, deployment.installedApplicationSettings, logger);
            appdeployer.RestartApp();

        }

        /// <summary>
        /// Deploy a single app from it's YAML settings. For testing purposes only.
        /// </summary>
        /// <param name="settings"></param>
        public void DeploySingleAppFromFile(string path, bool force = false)
        {
            DeploySingleAppFromTextSettings(File.ReadAllText(path), force);
        }

        /// <summary>
        /// Undeploy a single app from it's YAML settings. For testing purposes only.
        /// </summary>
        /// <param name="settings"></param>
        public void UndeploySingleApp(string settings)
        {
            Configuration.InstalledApplication app = new Configuration.InstalledApplication();
            app.parseFromString(settings);

            iischef.core.ApplicationDeployer deployer = new ApplicationDeployer(globalSettings, app, logger);
            deployer.UninstallApp();
        }

        public Configuration.InstalledApplication getInstalledApp(string Id)
        {
            var apps = getInstalledApps();
            return apps.Where((i) => i.getId() == Id).FirstOrDefault();
        }

        /// <summary>
        /// Get a list of all currently installed applications.
        /// </summary>
        /// <returns></returns>
        public List<Configuration.InstalledApplication> getInstalledApps()
        {
            List<Configuration.InstalledApplication> apps = new List<Configuration.InstalledApplication>();

            string activeDeploymentPathStorage = UtilsSystem.CombinePaths(globalSettings.settingsDir, "deployments");
            if (!Directory.Exists(activeDeploymentPathStorage))
                Directory.CreateDirectory(activeDeploymentPathStorage);


            if (!Directory.Exists(activeDeploymentPathStorage))
            {
                throw new Exception("Active deployment path not found: " + activeDeploymentPathStorage);
            }

            foreach (var f in (new DirectoryInfo(activeDeploymentPathStorage)).EnumerateFiles("active.*.json"))
            {
                Deployment deployment = Deployment.InstanceFromPath(f.FullName, globalSettings);
                apps.Add(deployment.installedApplicationSettings);
            }

            return apps;
        }

        /// <summary>
        /// Get list of installed application templates.
        /// </summary>
        /// <returns></returns>
        public List<Configuration.InstalledApplication> getInstalledApplicationTemplates()
        {
            List<Configuration.InstalledApplication> apps = new List<Configuration.InstalledApplication>();

            var installedAppsFolder = this.GetInstalledApplicationTemplatesFolder();
            var difo = new DirectoryInfo(installedAppsFolder);

            if (!difo.Exists)
            {
                throw new Exception(String.Format("Non existent installed applications folder: {0}", installedAppsFolder));
            }

            foreach (var file in difo.EnumerateFiles())
            {
                var installedApp = new Configuration.InstalledApplication();
                installedApp.parseFromString(File.ReadAllText(file.FullName));
                apps.Add(installedApp);
            }

            return apps;
        }

        public List<Deployment> RedeployInstalledApplication(
            bool fromtemplate = false,
            string id = null,
            bool force = false,
            string buildId = null,
            bool sync = false,
            string tags = null)
        {
            if (id == null && buildId != null)
            {
                throw new Exception("In order to specify a build id, you must also specify an application Id to deploy.");
            }

            if (id == null && !string.IsNullOrWhiteSpace(tags))
            {
                throw new Exception("Tags are only supported for single app deployments. Please, specify an applicaiton id.");
            }

            List<Configuration.InstalledApplication> installedApplications;

            if (fromtemplate)
            {
                installedApplications = this.getInstalledApplicationTemplates();
            }
            else
            {
                installedApplications = this.getInstalledApps();
            }

            if (!string.IsNullOrWhiteSpace(id))
            {
                installedApplications = installedApplications.Where((i) => i.getId() == id).Take(1).ToList();
            }

            logger.LogInfo(true, "Deploying {0} applications: {1}", installedApplications.Count(), String.Join(",", installedApplications.Select((i) => i.getId())));

            List<Deployment> result = new List<Deployment>();

            foreach (var iap in installedApplications)
            {
                try
                {
                    // Appends tags if any...
                    if (!string.IsNullOrWhiteSpace(tags))
                    {
                        iap.mergeTags(tags);
                    }

                    result.Add(this.DeploySingleAppFromInstalledApplication(iap, force, buildId, sync));
                }
                catch (Exception e)
                {
                    logger.LogException(new Exception(
                        $"Error deploying application {iap.getId()}", e));
                }
            }

            return result;
        }

        /// <summary>
        /// Execute cleanup for all installed
        /// applications.
        /// </summary>
        public void ExecuteCleanup()
        {
            var installedApplications = this.getInstalledApps();

            logger.LogInfo(false, "Cleaning up {0} applications: {1}", installedApplications.Count(), String.Join(",", installedApplications.Select((i) => i.getId())));

            List<Deployment> result = new List<Deployment>();

            foreach (var iap in installedApplications)
            {
                try
                {
                    var deployer = getDeployer(iap);
                    deployer.CleanupApp();
                }
                catch (Exception e)
                {
                    logger.LogException(new Exception(
                        $"Error cleanning up application {iap.getId()}", e));
                }
            }
        }

        public void RunCron()
        {
            var installedApplications = this.getInstalledApps();

            logger.LogInfo(true, "Running cron for {0} applications: {1}", installedApplications.Count(), String.Join(",", installedApplications.Select((i) => i.getId())));

            List<Deployment> result = new List<Deployment>();

            foreach (var iap in installedApplications)
            {
                try
                {
                    var deployer = getDeployer(iap);
                    deployer.RunCron();
                }
                catch (Exception e)
                {
                    logger.LogException(new Exception(
                        $"Error cleanning up application {iap.getId()}", e));
                }
            }
        }

        public List<Deployment> SyncInstalledApplication(string Id = null)
        {
            List<Configuration.InstalledApplication> installedApplications;
            installedApplications = this.getInstalledApps();

            if (!string.IsNullOrWhiteSpace(Id))
            {
                installedApplications = installedApplications.Where((i) => i.getId() == Id).Take(1).ToList();
            }

            logger.LogInfo(false, "Sync {0} applications: {1}", installedApplications.Count(), String.Join(",", installedApplications.Select((i) => i.getId())));

            List<Deployment> result = new List<Deployment>();

            foreach (var iap in installedApplications)
            {
                try
                {
                    result.Add(this.SyncSingleAppFromInstalledApplication(iap));
                }
                catch (Exception e)
                {
                    logger.LogException(new Exception(
                        String.Format("Error synchronize application {0}", iap.getId()), e));
                }
            }

            return result;
        }

        /// <summary>
        /// Get the folder where application templates are stored.
        /// </summary>
        /// <returns></returns>
        public string GetInstalledApplicationTemplatesFolder()
        {
            return UtilsSystem.CombinePaths(globalSettings.settingsDir, "installed_apps");
        }

        protected string getGlobalStoragePath(string filename)
        {
            var environmentSettingsFile =
                UtilsSystem.ensureDirectoryExists(
                    UtilsSystem.CombinePaths(
                Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData),
                "iischef",
                "config",
                filename));

            return environmentSettingsFile;
        }


        /// <summary>
        /// Delete unused temp files...
        /// </summary>
        protected void CleanupTempDirectories()
        {
            var logPath = this.globalSettings.GetDefaultTempStorage().path;

            var files = Directory.EnumerateFiles(logPath, "*", SearchOption.AllDirectories);

            foreach (string f in files)
            {
                try
                {
                    FileInfo info = new FileInfo(f);

                    // Delete files that have not been touched in the last 6 months
                    if ((DateTime.Now - info.LastWriteTime).TotalDays > (30 * 6))
                    {
                        System.IO.File.Delete(f);
                        this.logger.LogInfo(true, "Deleted temp file: {0}", info.FullName);
                    }
                }
                catch (Exception e)
                {
                    logger.LogException(e);
                }
            }
        }

        /// <summary>
        /// It was extremely difficult to control this at an application level
        /// so we do a global cleanup. On environments with many deployments,
        /// this can grow big very fast.
        /// </summary>
        protected void CleanupNetFrameworkTempFiles()
        {
            ////string netBase = Path.GetFullPath(Path.Combine(RuntimeEnvironment.GetRuntimeDirectory(), @"..\.."));
            ////string strTemp32 = string.Concat(netBase, @"\Framework\", RuntimeEnvironment.GetSystemVersion(), @"\Temporary ASP.NET Files");
            ////string strTemp64 = string.Concat(netBase, @"\Framework64\", RuntimeEnvironment.GetSystemVersion(), @"\Temporary ASP.NET Files");

            // This has been disabled because
            // deleting based on a timestamp is NOT reliable, and these files might belong
            // to active applications. IIS will fail to re-compile if these files start missing
            // all of a sudden unless you reset IIS.

            ////this.CleanUpDir(strTemp64);
            ////this.CleanUpDir(strTemp32);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dir"></param>
        public void CleanUpDir(string dir)
        {
            if (!Directory.Exists(dir))
            {
                return;
            }

            foreach (var difo in new DirectoryInfo(dir).EnumerateDirectories())
            {
                foreach (var appDifo in difo.EnumerateDirectories())
                {
                    try
                    {
                        // Delete folders older than 1 month
                        if ((DateTime.Now - appDifo.LastWriteTime).TotalDays > 30)
                        {
                            UtilsSystem.deleteDirectory(appDifo.FullName, false);
                            this.logger.LogInfo(true, "Deleted temp dir: {0}", appDifo.FullName);
                        }
                    }
                    catch (Exception e)
                    {
                        logger.LogException(e);
                    }
                }
            }
        }

        /// <summary>
        /// Archive and cleanup the log directories...
        /// 
        /// TODO: This is hardcoded here... should be a true cron
        /// that each deployer implements the way they want...
        /// </summary>
        protected void CleanupLogDirectories()
        {
            var logPath = this.globalSettings.GetDefaultLogStorage().path;

            var files = System.IO.Directory.EnumerateFiles(logPath, "*", SearchOption.AllDirectories);

            foreach (string f in files)
            {
                try
                {
                    FileInfo info = new FileInfo(f);

                    // Delete any zipped logs not touched in the last six months
                    if ((DateTime.Now - info.LastWriteTime).TotalDays > (30 * 6)
                        && info.FullName.EndsWith("_bak.zip"))
                    {
                        System.IO.File.Delete(f);
                        this.logger.LogInfo(true, "Deleted archived log file: {0}", info.FullName);
                        continue;
                    }

                    // Zip any log files that are larger than 100Mb or
                    // have not been writen into in the last 30 days.
                    bool extensionCriteria = (info.Extension.ToLower() == ".log"
                        || info.Extension.ToLower() == ".txt");

                    bool timeCriteria = (DateTime.UtcNow - info.LastWriteTimeUtc).TotalDays > 30;
                    bool sizeCriteria = info.Length > 1024 * 1024 * 100;

                    if (extensionCriteria && (timeCriteria || sizeCriteria))
                    {
                        string name = info.FullName;
                        string no_extension = name.Substring(0, name.Length - info.Extension.Length);
                        string folder_temp = no_extension;

                        System.IO.Directory.CreateDirectory(no_extension);

                        info.MoveTo(utils.UtilsSystem.CombinePaths(no_extension, info.Name));
                        string diff = info.CreationTime.ToString("yyyyMMddHHmmss");
                        ZipFile.CreateFromDirectory(folder_temp, no_extension + "_" + diff + "_bak.zip");

                        System.IO.Directory.Delete(folder_temp, true);

                        this.logger.LogInfo(true, "Archived log file: {0}", info.FullName);
                    }
                }
                catch (Exception e)
                {
                    logger.LogException(e);
                }
            }
        }

        protected string getGlobalStorageVariable(string key)
        {
            var path = getGlobalStoragePath(key);
            if (!File.Exists(path))
            {
                return null;
            }

            return File.ReadAllText(path);
        }

        protected void setGlobalStorageVariable(string key, string value)
        {
            var path = getGlobalStoragePath(key);
            File.WriteAllText(path, value);
        }

        /// <summary>
        /// Set the location of the global environment file path.
        /// </summary>
        /// <param name="path"></param>
        public void setGlobalEnvironmentFilePath(string path)
        {
            if (!File.Exists(path))
            {
                throw new Exception("Environment file does not exist: " + path);
            }

            setGlobalStorageVariable("environment-file-path", path);
        }

        protected LoggerInterface parentLogger;

        public void useParentLogger()
        {
            logger = parentLogger;
        }



        /// <summary>
        /// Install chef to the specified drive
        /// </summary>
        /// <param name="installDir"></param>
        public void selfInstall(string installDir, string environmentId)
        {
            if (string.IsNullOrWhiteSpace(environmentId))
                environmentId = Environment.MachineName;

            string settingsFile = null;

            // If chef is already installed, don't let us install!!
            var environmentSettingsFile = getGlobalStorageVariable("environment-file-path");

            if (settingsFile != null)
            {
                environmentSettingsFile = settingsFile;
            }

            if (File.Exists(environmentSettingsFile))
            {
                logger.LogWarning(false, "Chef already installed. Current config file in: " + environmentSettingsFile);
                return;
            }

            var autosettings = new EnvironmentSettings();

            autosettings.id = environmentId;

            autosettings.contentStorages = new List<StorageLocation>();
            autosettings.contentStorages.Add(new StorageLocation()
            {
                id = "default",
                path = Path.Combine(installDir, "_contents")
            });
            autosettings.primaryContentStorage = "default";

            autosettings.tempStorages = new List<StorageLocation>();
            autosettings.tempStorages.Add(new StorageLocation()
            {
                id = "default",
                path = Path.Combine(installDir, "_temp")
            });
            autosettings.primaryTempStorage = "default";

            autosettings.applicationStorages = new List<StorageLocation>();
            autosettings.applicationStorages.Add(new StorageLocation()
            {
                id = "default",
                path = Path.Combine(installDir, "_apps")
            });
            autosettings.primaryApplicationStorage = "default";

            autosettings.logStorages = new List<StorageLocation>();
            autosettings.logStorages.Add(new StorageLocation()
            {
                id = "default",
                path = Path.Combine(installDir, "_log")
            });
            autosettings.primaryLogStorage = "default";

            foreach (var p in autosettings.logStorages)
                UtilsSystem.ensureDir(p.path);

            foreach (var p in autosettings.applicationStorages)
                UtilsSystem.ensureDir(p.path);

            foreach (var p in autosettings.tempStorages)
                UtilsSystem.ensureDir(p.path);

            foreach (var p in autosettings.contentStorages)
                UtilsSystem.ensureDir(p.path);

            autosettings.artifactTempPath = Path.Combine(installDir, "_artifact_temp");
            UtilsSystem.ensureDir(autosettings.artifactTempPath);

            var settingsDir = Path.Combine(installDir, "_configuration");
            var settingsPath = Path.Combine(settingsDir, "config.json");

            autosettings.endpoints = new List<NetworkInterface>();
            autosettings.endpoints.Add(new NetworkInterface()
            {
                forcehosts = true,
                id = "local",
                ip = "127.0.0.1"
            });

            // Retrieve the enumerator instance and then the data.
            SqlDataSourceEnumerator instance = SqlDataSourceEnumerator.Instance;
            logger.LogInfo(false, "Searching for local SQL Server instances...");
            System.Data.DataTable table = instance.GetDataSources();

            string sqlserver_suffix = "";

            foreach (System.Data.DataRow t in table.Rows)
            {
                string id = "default" + sqlserver_suffix;

                string serverName = t["ServerName"] as string;
                string instanceName = t["InstanceName"] as string;

                string serverString = serverName;
                if (!string.IsNullOrWhiteSpace(instanceName))
                {
                    serverString += "\\" + instanceName;
                }

                var connectionString = string.Format("Server={0};Integrated Security = true;",
                        serverString);

                autosettings.sqlServers = new List<SQLServer>();
                autosettings.sqlServers.Add(new SQLServer()
                {
                    id = id,
                    connectionString = connectionString
                });

                logger.LogInfo(false, "Added server: {0}", connectionString);

                autosettings.primarySqlServer = id;
                sqlserver_suffix = autosettings.sqlServers.Count.ToString();
            }

            UtilsSystem.ensureDir(settingsDir);
            string serialized = Newtonsoft.Json.JsonConvert.SerializeObject(autosettings, Newtonsoft.Json.Formatting.Indented);
            File.WriteAllText(settingsPath, serialized);

            setGlobalEnvironmentFilePath(settingsPath);

            logger.LogInfo(true, "Find chef configuration file at: {0}", settingsPath);
        }

        /// <summary>
        /// Path to the environment settings folder
        /// </summary>
        /// <param name="settingsFile"></param>
        public void initialize(string settingsFile = null, string options = null)
        {
            var environmentSettingsFile = getGlobalStorageVariable("environment-file-path");

            if (settingsFile == null && !File.Exists(environmentSettingsFile))
            {
                throw new Exception("To start the deployer you need to provide a valid environment configuration file. The default location is: " + environmentSettingsFile);
            }

            if (settingsFile != null)
            {
                environmentSettingsFile = settingsFile;
            }

            var serverSettingsContent = File.ReadAllText(environmentSettingsFile);

            globalSettings = Newtonsoft.Json.JsonConvert.DeserializeObject<EnvironmentSettings>(serverSettingsContent);

            // If the settings do not have a settings directory, use the same one where the environment settings are stored.
            if (String.IsNullOrEmpty(globalSettings.settingsDir))
            {
                logger.LogInfo(true, "No settings directory specified. Using default:" + environmentSettingsFile);
                globalSettings.settingsDir = Path.GetDirectoryName(environmentSettingsFile);
            }

            if (globalSettings.options == null)
            {
                globalSettings.options = new List<string>();
            }

            if (options != null)
            {
                foreach (var option in options.Split(",".ToCharArray()))
                {
                    if (!globalSettings.options.Contains(option))
                    {
                        globalSettings.options.Add(option);
                    }
                }
            }

            // Now move to a file based logger
            // and keep track of original logger.
            parentLogger = logger;
            logger = new FileLogger(UtilsSystem.CombinePaths(globalSettings.GetDefaultLogStorage().path, "chef-application.log"));
        }

        /// <summary>
        /// Run the AppVeyor monitor.
        /// </summary>
        public void RunAppVeyorMonitor()
        {
            // Run all the monitors
            if (this.globalSettings.appVeyorMonitors == null)
            {
                return;
            }

            foreach (var monitorSettings in this.globalSettings.appVeyorMonitors)
            {
                var monitor = new AppVeyorMonitor.AppVeyorMonitor(monitorSettings, this, this.logger);
                monitor.FindNewDeployments();
            }
        }

        /// <summary>
        /// Uninstall and delete any expired applications.
        /// </summary>
        public void RemoveExpiredApplications(double currentDateUtcUnixTime, string id = null)
        {
            string timespanFormat = "d\\d\\ hh\\h\\ mm\\m\\ ss\\s";

            var installedApplications = this.getInstalledApps();

            if (!string.IsNullOrWhiteSpace(id))
            {
                installedApplications = installedApplications.Where((i) => i.getId() == id).ToList();
            }

            logger.LogInfo(true, "Total installed applications: {0}", installedApplications.Count);

            foreach (var iap in installedApplications)
            {
                var deployer = getDeployer(iap);

                // Max 30 days ttl allowed for ad-hoc environments.
                long maxTtl = 24 * 30;

                double ttl = deployer.deploymentActive?.installedApplicationSettings?.getExpires() ?? 0;

                if (ttl <= 0)
                {
                    logger.LogInfo(true, "Environment {0} does not expire", iap.getId());
                    continue;
                }

                if (ttl > maxTtl)
                {
                    logger.LogInfo(true, "Environment '{0}' ttl exceeds maximum allowed.", iap.getId());
                    ttl = maxTtl;
                }

                var ttlTimeSpan = new TimeSpan(0, (int)ttl, 0, 0);

                logger.LogInfo(true, "Environment '{0}' has a total Ttl of '{1}'", iap.getId(), ttlTimeSpan.ToString(timespanFormat));

                // Just a double check and safeguard... make sure the appid starts "auto-" that
                // is the prefix given by the autodeployer.
                if (!iap.getId().StartsWith(Application.AutoDeployApplicationIdPrefix))
                {
                    logger.LogWarning(false, $"Skipping removal of expired application because of missing prefix '{Application.AutoDeployApplicationIdPrefix}'");
                    continue;
                }

                long deploymentDate = deployer.deploymentActive.DeploymentUnixTime ?? 0;

                var remainingTime = new TimeSpan(0, 0, 0, (int)(deploymentDate + ttlTimeSpan.TotalSeconds - currentDateUtcUnixTime));

                logger.LogInfo(true, "Environment expires in {0}[{1}]", (remainingTime < TimeSpan.Zero) ? "-" : "", remainingTime.ToString(timespanFormat));

                if (remainingTime.TotalSeconds > 0)
                {
                    continue;
                }

                deployer.UninstallApp(true);
            }
        }

        /// <summary>
        /// This is called by the Chef service at periodic intervals.
        /// 
        /// Use this as a sort of "cron" trigger.
        /// </summary>
        public void RunServiceLoop()
        {
            this.RunOperationWithLog(() =>
            {
                this.RunAppVeyorMonitor();
            });

            this.RunOperationWithLog(() =>
            {
                this.RemoveExpiredApplications(DateTime.UtcNow.ToUnixTimestamp());
            });

            this.RunOperationWithLog(() =>
            {
                this.RedeployInstalledApplication(false, null, false, null);
            });

            this.RunOperationWithLog(() =>
            {
                this.RunMaintenance();
            });

            this.RunOperationWithLog(() =>
            {
                this.RunCron();
            });
        }

        public void RunMaintenance()
        {
            this.CleanupLogDirectories();
            this.CleanupTempDirectories();
            this.CleanupNetFrameworkTempFiles();
        }

        public void RunOperationWithLog(Action operation)
        {
            try
            {
                operation();
            }
            catch (Exception e)
            {
                this.logger.LogException(e);
            }
        }
    }
}
