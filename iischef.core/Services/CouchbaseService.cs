﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iischef.utils;

namespace iischef.core.Services
{
    class CouchbaseService : DeployerBase, DeployerInterface
    {
        public void deploy()
        {
            var couchbaseSettings = DeployerSettings.castTo<CouchbaseServiceSettings>();

            // There is nothing really here. You simply request
            // an URI, username and password for a bucket. The application
            // is responsible for prefixing it's keys with something
            // unique...
            var couchbaseServer = GlobalSettings.GetDefaultCouchbaseServer();

            Deployment.setRuntimeSetting(String.Format("services.{0}.uri", couchbaseSettings.id), couchbaseServer.uri);
            Deployment.setRuntimeSetting(String.Format("services.{0}.bucket-name", couchbaseSettings.id), couchbaseServer.bucketName);
            Deployment.setRuntimeSetting(String.Format("services.{0}.bucket-password", couchbaseSettings.id), couchbaseServer.bucketPassword);
        }

        public void undeploy(bool isUninstall = false)
        {

        }

        public void start()
        {

        }

        public void stop()
        {

        }


        public void deploySettings(string jsonSettings,
            string jsonSettingsNested,
            RuntimeSettingsReplacer replacer)
        {

        }

        public void sync()
        {

        }
    }
}
