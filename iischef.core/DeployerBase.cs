﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.IO;

using iischef.utils;

namespace iischef.core
{
    public class DeployerBase
    {
        /// <summary>
        /// 
        /// </summary>
        protected SystemConfiguration.EnvironmentSettings GlobalSettings;

        /// <summary>
        /// 
        /// </summary>
        protected Deployment Deployment;

        /// <summary>
        /// 
        /// </summary>
        protected JObject DeployerSettings;

        /// <summary>
        /// 
        /// </summary>
        protected logger.LoggerInterface Logger;

        /// <summary>
        /// 
        /// </summary>
        protected Configuration.InstalledApplication ParentApp;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="globalSettings"></param>
        /// <param name="deployerSettings"></param>
        /// <param name="deployment"></param>
        /// <param name="logger"></param>
        /// <param name="inhertApp"></param>
        public virtual void initialize(SystemConfiguration.EnvironmentSettings globalSettings,
                JObject deployerSettings,
                Deployment deployment,
                logger.LoggerInterface logger,
                Configuration.InstalledApplication inhertApp
                )
        {
            this.Deployment = deployment;
            this.GlobalSettings = globalSettings;
            this.DeployerSettings = deployerSettings;
            this.Logger = logger;
            this.ParentApp = inhertApp;

            if (this.ParentApp != null)
            {
                DeployerSettingsBase settings = deployerSettings.castTo<DeployerSettingsBase>();
            }

        }

        public int weight { get; set; }

        public void deployConsoleEnvironment(StringBuilder command)
        {
        }

        /// <inheritdoc cref="DeployerInterface"/>
        public virtual void done()
        {
        }

        /// <inheritdoc cref="DeployerInterface"/>
        public virtual void beforeDone()
        {
        }

        /// <inheritdoc cref="DeployerInterface"/>
        public virtual void cleanup()
        {
        }

        /// <inheritdoc cref="DeployerInterface"/>
        public virtual void cron()
        {
        }

        /// <summary>
        /// Find the matching deployer of the parent application when
        /// inheritance is configured for this application.
        /// </summary>
        /// <typeparam name="TType"></typeparam>
        /// <returns></returns>
        public TType getDeployerFromParentApp<TType>() where TType : DeployerBase
        {
            // We need a parent application for this to work.
            if (ParentApp == null)
            {
                return null;
            }

            // Try to grab parent deployment...
            Deployment parentDeployment;
            string activeDeploymentPathStorage = UtilsSystem.CombinePaths(GlobalSettings.settingsDir, "deployments", "active." + ParentApp.getId() + ".json");
            if (File.Exists(activeDeploymentPathStorage))
            {
                parentDeployment = Deployment.InstanceFromPath(activeDeploymentPathStorage, GlobalSettings);
                DeployerSettingsBase ourSettings = DeployerSettings.castTo<DeployerSettingsBase>();
                List<DeployerInterface> deployersAndServices = new List<DeployerInterface>();
                deployersAndServices.AddRange(parentDeployment.grabServices(Logger));
                deployersAndServices.AddRange(parentDeployment.grabDeployers(Logger));

                // Only keep those that match our type
                deployersAndServices = deployersAndServices.Where(s => s.GetType() == typeof(TType)).ToList();

                // Filter by ID
                foreach (TType t in deployersAndServices)
                {
                    if (t.DeployerSettings.castTo<DeployerSettingsBase>().id == ourSettings.id)
                    {
                        return t;
                    }
                }
                return null;
            }
            else
            {
                return null;
            }
        }

        public virtual void _sync(Object other)
        {
        }

        public void syncCommon<TType>() where TType : DeployerBase
        {
            TType other = this.getDeployerFromParentApp<TType>();

            if (other != null)
            {
                _sync(other);
            }
            else
            {
                Logger.LogError("No parent or parent does not have same deployer.");
            }
        }

    }
}
