﻿using System;
using System.Diagnostics;
using System.Text;

namespace iischef.logger
{
    public abstract class BaseLogger : LoggerInterface
    {
        protected bool verbose = false;

        /// <summary>
        /// Enabel or disable verbose mode for the logger.
        /// </summary>
        /// <param name="verbose"></param>
        public void setVerbose(bool verbose)
        {
            this.verbose = verbose;
        }

        protected void WriteEntry(bool verbose, string content, object[] replacements, EventLogEntryType type)
        {
            if (verbose && !this.verbose)
            {
                return;
            }

            string data = FormatEntry(content, replacements);

            doWrite(data, type);
        }

        protected string FormatEntry(string content, object[] replacements)
        {
            if (replacements == null)
            {
                return content;
            }

            return string.Format(content, replacements);
        }

        protected abstract void doWrite(string content, EventLogEntryType type);

        /// <summary>
        /// Everything we log in this monitor application
        /// is an error.
        /// </summary>
        /// <param name="message"></param>
        public void LogError(string message, params object[] replacements)
        {
            WriteEntry(false, message, replacements, EventLogEntryType.Error);
        }

        public void LogInfo(bool verbose, string message, params object[] replacements)
        {
            WriteEntry(verbose, message, replacements, EventLogEntryType.Information);
        }

        public void LogWarning(bool verbose, string message, params object[] replacements)
        {
            WriteEntry(verbose, message, replacements, EventLogEntryType.Warning);
        }

        public void LogException(Exception e)
        {
            StringBuilder sb = new StringBuilder();
            dumpException(e, sb);
            // Exceptions are never verbose, and this is important information that needs to be recored in the logs!
            WriteEntry(false, sb.ToString(), null, EventLogEntryType.Error);
        }

        protected void dumpException(Exception e, StringBuilder sb)
        {
            if (sb == null)
            {
                sb = new StringBuilder();
            }

            sb.AppendLine("Exception HResult: '" + Convert.ToString((uint) e.HResult) + "'");
            sb.AppendLine("Exception Type: '" + e.GetType().FullName);
            sb.AppendLine("ExceptionMessage:" + e.Message);
            sb.AppendLine("Stack Trace:" + e.StackTrace);

            if (e.InnerException != null)
            {
                dumpException(e.InnerException, sb);
            }
        }
    }
}
