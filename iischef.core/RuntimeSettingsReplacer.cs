﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iischef.core
{
    /// <summary>
    /// Use this class to replace patterns from 
    /// configuration settings
    /// </summary>
    public class RuntimeSettingsReplacer
    {
        /// <summary>
        /// The settings.
        /// </summary>
        Dictionary<string, string> settings;

        /// <summary>
        /// Get an instance of RuntimeSettingsReplacer
        /// </summary>
        /// <param name="settings"></param>
        public RuntimeSettingsReplacer(Dictionary<string, string> settings)
        {
            this.settings = settings;
        }

        /// <summary>
        /// Do the replacements...
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public string doReplace(string contents)
        {
            foreach (var kvpsetting in settings)
            {
                contents = contents.Replace("{" + kvpsetting.Key + "}", kvpsetting.Value);
            }

            // New support for filters and strong verification....
            // string a = "{@smykey.myvalue.thisisit|arg1-0, arg1-1|arg2@}";
            var matches = System.Text.RegularExpressions.Regex.Matches(contents, "{@([^@]*)@}");
            foreach (System.Text.RegularExpressions.Match match in matches)
            {
                string textMatch = match.Groups[1].Value;

                var parts = textMatch.Split("|".ToCharArray()).ToList();

                // Primera parte es la variable de configuración y es obligatorio
                string configKey = parts[0];
                if (!settings.ContainsKey(configKey))
                {
                    throw new Exception(string.Format("Missing key in settings: " + configKey));
                }

                string value = settings[configKey];

                parts.RemoveAt(0);

                foreach (string p in parts)
                {
                    var pos = p.IndexOf(":");
                    var operation = p.Substring(0, pos);
                    pos++;
                    string arguments = p.Substring(pos, p.Length - pos);
                    switch (operation)
                    {
                        case "filter":
                            value = operationFilter(value, arguments);
                            break;
                        default:
                            throw new NotImplementedException("Operation not implemented: " + operation);
                    }
                }

                contents = contents.Replace(match.Value, value);
            }

            return contents;
        }

        /// <summary>
        /// Filters...
        /// </summary>
        /// <param name="input"></param>
        /// <param name="arguments"></param>
        /// <returns></returns>
        protected string operationFilter(string input, string arguments)
        {
            var args = arguments.Split(",".ToCharArray());
            foreach (string arg in args)
            {
                switch (arg)
                {
                    // Filtro de poner todo como "forwardslash"
                    case "allforward":
                        input = input.Replace("\\", "/");
                        break;
                    case "trimpath":
                        input = input.Trim(" \\//".ToCharArray());
                        break;
                    default:
                        throw new Exception("Filter not found: " + arg);
                }
            }

            return input;
        }
    }
}
