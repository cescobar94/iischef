﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iischef.core.Configuration
{
    public class DeploymentWindow
    {
        /// <summary>
        /// The time zone for this deployment window
        /// </summary>
        public string timezone { get; set; }

        /// <summary>
        /// The start time
        /// </summary>
        public string start { get; set; }

        /// <summary>
        /// The end time
        /// </summary>
        public string end { get; set; }
    }
}
