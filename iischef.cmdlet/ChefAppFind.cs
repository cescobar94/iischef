﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Management.Automation;

namespace iischef.cmdlet
{
    [Cmdlet(VerbsLifecycle.Invoke, "ChefAppFind")]
    [OutputType(typeof(List<iischef.core.Configuration.InstalledApplication>))]
    public class ChefAppFind : Cmdlet
    {
        [Parameter(Position = 1, ValueFromPipelineByPropertyName = true)]
        public string Id { get; set; }

        protected override void ProcessRecord()
        {
            var app = ConsoleUtils.getApplicationForConsole();

            var apps = app.getInstalledApps();
            apps = apps.OrderBy((i) => i.getId()).ToList();

            if (Id == null)
            {
                WriteObject(apps);

                foreach (var a in apps)
                {
                    Console.WriteLine(a.getId());
                }

                return;
            }

            foreach (var a in apps)
            {
                if (a.getId() == Id)
                {
                    WriteObject(a);
                    return;
                }
            }

            throw new Exception("Application not found: " + Id);
        }
    }
}
