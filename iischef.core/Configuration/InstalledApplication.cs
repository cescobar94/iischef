﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using iischef.utils;
using iischef.core.Downloaders;

namespace iischef.core.Configuration
{
    /// <summary>
    /// Configuration files used for file-based persistent deployment (AKA "From Template").
    /// </summary>
    public class InstalledApplication : YamlConfigurationFile
    {
        /// <summary>
        /// We can override runtime settings for an application
        /// (i.e. to make it point to a specific storage that does
        /// not match the default pattern assigned by chef).
        /// 
        /// Not all settings accept overriding, see the declaring component
        /// specification for support.
        /// </summary>
        public Dictionary<string, string> getRuntimeSettingsOverrides()
        {
            if (configuration["runtime_overrides"] == null)
            {
                return new Dictionary<string, string>();
            }

            return configuration["runtime_overrides"].ToObject<Dictionary<string, string>>();
        }

        /// <summary>
        /// The application uniqueId
        /// </summary>
        /// <returns></returns>
        public string getId()
        {
            return configuration["id"].ToString();
        }

        /// <summary>
        /// How to mount the artifact once obtained from the 
        /// downloader:
        /// * move (fastest - good for production, destroys original source)
        /// * copy (the default)
        /// * symlink (use symlink)
        /// * junction (use junction)
        /// * origin (mount the application directly to the path provided by the downloader)
        /// </summary>
        /// <returns></returns>
        public string getApplicationMountStrategy()
        {
            if (configuration["mount_strategy"] == null)
            {
                return "copy";
            }

            return configuration["mount_strategy"].ToString();
        }

        /// <summary>
        /// TTL for this application (will be removed). In HOURS.
        /// </summary>
        /// <returns></returns>
        public Double getExpires()
        {
            if (configuration["expires"] == null)
            {
                return 0;
            }

            return Double.Parse((string)configuration["expires"]);
        }

        protected List<string> getInternalTags()
        {
            if (!configuration["tags"].IsNullOrDefault())
            {
                var tags = System.Text.RegularExpressions.Regex.Split((string)configuration["tags"], ",");

                return (from t in tags
                    where !string.IsNullOrWhiteSpace(t)
                    select t.ToLower().Trim()).Distinct().ToList();
            }

            return new List<string>();
        }

        /// <summary>
        /// Tags for this installation.
        /// </summary>
        /// <returns></returns>
        public List<string> getTags()
        {
            List<string> applicationTags = new List<string>();
            applicationTags.AddRange(this.getInternalTags());
            applicationTags.Add($"app-id-{this.getId()}");
            return applicationTags;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="stringTags"></param>
        public void mergeTags(string stringTags)
        {
            var tags = System.Text.RegularExpressions.Regex.Split(stringTags, ",");
            var currentTags = this.getInternalTags();
            currentTags.AddRange(tags);
            currentTags = currentTags.Select((i) => i.ToLower().Trim()).Distinct().ToList();
            configuration["tags"] = string.Join(", ", currentTags);
        }

        /// <summary>
        /// The downloader
        /// </summary>
        public Downloaders.DownloaderInterface getDownloader(SystemConfiguration.EnvironmentSettings globalSettings, logger.LoggerInterface logger)
        {
            JObject downloader = (JObject)configuration["downloader"];

            string type = (string)downloader["type"];

            switch (type)
            {
                case "appveyor":
                    AppVeyorDownloaderSettings settings = downloader.castTo<AppVeyorDownloaderSettings>();
                    return new AppVeyorDownloader(settings, globalSettings, logger);
                case "localpath":
                    LocalPathDownloaderSettings settings2 = downloader.castTo<LocalPathDownloaderSettings>();
                    return new LocalPathDownloader(settings2, globalSettings, logger);
                default:
                    throw new Exception("Unkown downloader type:" + type);
            }

        }

        /// <summary>
        /// 
        /// </summary>
        public string getInherit()
        {
            if (configuration["inherit"] == null)
            {
                return "";
            }
            return configuration["inherit"].ToString();
        }

        public InstalledApplication()
        {
        }
    }
}
