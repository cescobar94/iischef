﻿using iischef.core.Configuration;
using iischef.utils;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using iischef.core.Services;
using Newtonsoft.Json;

namespace iischef.core
{
    /// <summary>
    /// Represents a deployment. Used to unmount previous applications (and to know what is currently deployed here...)
    /// </summary>
    public class Deployment
    {
        /// <summary>
        /// Load a deployement from a serialized file.
        /// </summary>
        /// <param name="filePath">The file path.</param>
        /// <param name="globalSettings">The global settings (might have changed sin object was serialized)</param>
        /// <returns></returns>
        public static Deployment InstanceFromPath(string filePath, SystemConfiguration.EnvironmentSettings globalSettings)
        {
            if (globalSettings == null)
            {
                throw new Exception("To deserialize a deployment you must provide updated global settings.");
            }

            var contents = File.ReadAllText(filePath);

            var result = JsonConvert.DeserializeObject<Deployment>(
                contents,
                new JsonSerializerSettings
                {
                    TypeNameHandling = TypeNameHandling.Auto
                });

            result.loadedFromPath = filePath;
            result.globalSettings = globalSettings;
            return result;
        }

        public Deployment()
        {
        }

        protected Deployment previousDeployment;

        /// <summary>
        /// Get the previous deployment instance, if any.
        /// </summary>
        /// <returns></returns>
        public Deployment getPreviousDeployment()
        {
            return previousDeployment;
        }

        /// <summary>
        /// Set the previous deployment
        /// </summary>
        /// <param name="previousDeployments"></param>
        public void setPreviousDeployment(Deployment previousDeployments)
        {
            this.previousDeployment = previousDeployments;
        }

        /// <summary>
        /// Expand runtime paths
        /// </summary>
        /// <returns></returns>
        public string expandPaths(string value)
        {
            value = value.Replace("%APP%", this.appPath);
            value = value.Replace("%RUNTIME%", this.runtimePath);
            value = value.Replace("%LOG%", this.logPath);
            value = value.Replace("%TEMP%", this.tempPath);
            value = value.Replace("%DEPLOYMENTID%", this.shortid);
            return value;
        }

        public Deployment(ApplicationSettings appSettings,
             SystemConfiguration.EnvironmentSettings globalSettings,
             Artifact source,
             InstalledApplication installedApplicationSettings,
             InstalledApplication parentInstalledApplicationSettings,
             Application app)
        {
            this.artifact = source;
            this.id = (Guid.NewGuid()).ToString();
            this.privateData = new Dictionary<string, object>();
            this.appSettings = appSettings;
            this.globalSettings = globalSettings;
            this.installedApplicationSettings = installedApplicationSettings;

            // We have a prefix to allow services to easily identify
            // chef bound resources.
            this.shortid = this.GetShortIdPrefix() + shortHash(id, 4).ToLower();
            if (!this.IsShortId(this.shortid))
            {
                throw new Exception("Invalid shortId generated ????");
            }

            this.runtimeSettings = new Dictionary<string, string>();
            this.parentInstalledApplicationSettings = parentInstalledApplicationSettings;
            this.DeploymentUnixTime = (long)DateTime.UtcNow.ToUnixTimestamp();
            deployRuntimeSettings();
        }

        /// <summary>
        /// Get the prefix used for this deployment shortid.
        /// </summary>
        /// <returns></returns>
        public string GetShortIdPrefix()
        {
            return "chf_" + installedApplicationSettings.getId() + "_";
        }

        public bool IsShortId(string id)
        {
            return System.Text.RegularExpressions.Regex.IsMatch(id, String.Format("^{0}([A-Za-z]{{4}}$)", System.Text.RegularExpressions.Regex.Escape(this.GetShortIdPrefix())));

        }

        public void deployRuntimeSettings()
        {
            var deployment = this;

            deployment.setRuntimeSetting("deployment.shortId", deployment.getShortId());
            deployment.setRuntimeSetting("deployment.time", DateTime.UtcNow.ToString("dd/MM/yyyy HH-mm-ss.fff"));
            deployment.setRuntimeSetting("deployment.environment.MachineName", Environment.MachineName);

            deployment.setRuntimeSetting("deployment.artifact.branch", deployment.artifact.artifactSettings.branch);
            deployment.setRuntimeSetting("deployment.artifact.id", deployment.artifact.id);
            deployment.setRuntimeSetting("deployment.artifact.commit_sha", deployment.artifact.artifactSettings.commit_sha);
            deployment.setRuntimeSetting("deployment.artifact.type", deployment.GetType().Name);

            deployment.setRuntimeSetting("installedApp.id", deployment.installedApplicationSettings.getId());
        }

        /// <summary>
        /// Grab the deployers for an application
        /// </summary>
        /// <returns></returns>
        public DeployerCollection grabDeployers(logger.LoggerInterface logger)
        {
            Dictionary<string, Type> deployerTypes = new Dictionary<string, Type>() {
                { "php", typeof(Php.PhpDeployer)},
                { "iis", typeof(IIS.IISDeployer)},
                { "app", typeof(Storage.AppBaseStorageDeployer)},
            };

            var deployers = new DeployerCollection(globalSettings, this, logger, parentInstalledApplicationSettings);

            foreach (var d in this.appSettings.getDeployers())
            {
                var type = (string)d.Value["type"];

                if (!deployerTypes.ContainsKey(type))
                {
                    throw new Exception("Deployer type not found:" + type);
                }

                Type deployertype = deployerTypes[type];

                deployers.AddItem(deployertype, (JObject)d.Value);
            }

            return deployers;
        }

        public DeployerCollection grabServices(logger.LoggerInterface logger)
        {
            Dictionary<string, Type> serviceTypes = new Dictionary<string, Type>() {
                { "sqlsrv", typeof(Services.SQLService)},
                { "disk", typeof(Services.DiskService)},
                { "couchbase", typeof(Services.CouchbaseService)},
                { "scheduler", typeof(Services.ScheduleService) }
            };

            var services = new DeployerCollection(globalSettings, this, logger, parentInstalledApplicationSettings);

            foreach (var d in this.appSettings.getServices())
            {

                var type = (string)d.Value["type"];

                if (!serviceTypes.ContainsKey(type))
                {
                    throw new Exception("Service type not found:" + type);
                }

                Type serviceType = serviceTypes[type];
                services.AddItem(serviceType, (JObject)d.Value);
            }

            return services;
        }


        /// <summary>
        /// Store a serialized version of this in a path
        /// </summary>
        /// <param name="path"></param>
        public void StoreInPath(string path)
        {
            UtilsSystem.ensureDirectoryExists(path);

            var serialized = JsonConvert.SerializeObject(
                this,
                Formatting.Indented,
                new JsonSerializerSettings
                {
                    TypeNameHandling = TypeNameHandling.Auto
                });

            File.WriteAllText(path, serialized);
        }

        public Configuration.ApplicationSettings getAppSettings()
        {
            return appSettings;
        }

        #region Default directories

        // All applications must have at least
        // 4 paths:
        // appPath: Application path. This is where the artifact is deployed. I.e. c:\_webs\mydeploymentslot\app
        // runtimePath: Runtime path. Any runtime specifics (such as PHP runtime, libraries, etc). I.e. c:\_webs\mydeploymentslot\runtime
        // logPath: Path for log files. Usually in a disk that can be filled without affecting application. I.e. e:\volatile\appid\logs
        // tempPath: Path for temp files. Usually in a disk that can be filled without affecting application. I.e. e:\volatile\appid\temp
        // Log and temp paths are preserved between builds, but are not guaranteed to be persistent.

        /// <summary>
        /// If this was loaded from a path, the path.
        /// </summary>
        public string loadedFromPath { get; set; }

        /// <summary>
        /// Application path
        /// </summary>
        public string appPath { get; set; }

        /// <summary>
        /// Every application must have a unique system identity
        /// in order to manage integrations and permissions.
        /// </summary>
        public string windowsUsername { get; set; }

        /// <summary>
        /// Get the password. This is generated realtime as a hash of the
        /// username itself.
        /// </summary>
        /// <returns></returns>
        public string getWindowsPassword()
        {
            // On some setups there is policy enforcement...
            // https://technet.microsoft.com/en-us/library/hh994562(v=ws.11).aspx
            var pwd = "#" + UtilsEncryption.GetMD5(windowsUsername).Substring(0, 10)
                + UtilsEncryption.GetMD5(windowsUsername).Substring(10, 10).ToUpper();
            return pwd;
        }

        /// <summary>
        /// Path for the runtime
        /// </summary>
        public string runtimePath { get; set; }

        /// <summary>
        /// Path for the logs
        /// </summary>
        public string logPath { get; set; }

        /// <summary>
        /// Path for temp files
        /// </summary>
        public string tempPath { get; set; }

        /// <summary>
        /// Singature for the environment... might change.
        /// </summary>
        public string environmentSignature { get; set; }

        #endregion

        /// <summary>
        /// A copy of the configuration used for the deployment...
        /// </summary>
        public Configuration.ApplicationSettings appSettings { get; set; }

        /// <summary>
        /// Global settings, for the record.
        /// </summary>
        public SystemConfiguration.EnvironmentSettings globalSettings { get; set; }

        /// <summary>
        /// Time the deploy job started
        /// </summary>
        public DateTime? jobStart { get; set; }

        /// <summary>
        /// Time the deploy job ended
        /// </summary>
        public DateTime? jobEnd { get; set; }

        /// <summary>
        /// Storage for random data from modules/components.
        /// </summary>
        public Dictionary<string, object> privateData { get; set; }

        /// <summary>
        /// The deployment id, unique for each deployment.
        /// </summary>
        public string id { get; set; }

        /// <summary>
        /// short id
        /// </summary>
        public string shortid { get; set; }

        /// <summary>
        /// The installed application settings
        /// </summary>
        public Configuration.InstalledApplication installedApplicationSettings { get; set; }

        /// <summary>
        /// The exact unis time this deployment was made at
        /// </summary>
        public long? DeploymentUnixTime { get; set; }

        /// <summary>
        /// The exact artifact this deplyoyment came from.
        /// </summary>
        public Artifact artifact { get; set; }

        /// <summary>
        /// When the user has enforced deployment of a specific version
        /// through the UI, automatic updates should not be pushed.
        /// </summary>
        public string enforceBuildId { get; set; }

        /// <summary>
        /// Settings passed on to the application at runtime
        /// with information about directories, databases, etc..
        /// </summary>
        public Dictionary<string, string> runtimeSettings { get; set; }

        /// <summary>
        /// Inhert application.
        /// 
        /// </summary>
        public Configuration.InstalledApplication parentInstalledApplicationSettings { get; set; }

        /// <summary>
        /// We can have runtime overrides at the deployedApplicationLevel
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, string> getRuntimeSettingsToDeploy()
        {
            // The priority is:
            //
            // 0. Settings defined by the application itself
            // 1. Per installed application settings
            // 2. Server level settings
            // 3. Settings generated during install by services

            Dictionary<string, string> result = new Dictionary<string, string>();

            foreach (var kvp in this.appSettings.getRuntimeSettingsOverrides())
            {
                if (!result.ContainsKey(kvp.Key))
                {
                    result.Add(kvp.Key, kvp.Value);
                }
            }

            foreach (var kvp in this.installedApplicationSettings.getRuntimeSettingsOverrides())
            {
                if (!result.ContainsKey(kvp.Key))
                {
                    result.Add(kvp.Key, kvp.Value);
                }
            }

            foreach (var kvp in this.globalSettings.getRuntimeOverrides())
            {
                if (!result.ContainsKey(kvp.Key))
                {
                    result.Add(kvp.Key, kvp.Value);
                }
            }

            foreach (var kvp in runtimeSettings)
            {
                if (!result.ContainsKey(kvp.Key))
                {
                    result.Add(kvp.Key, kvp.Value);
                }
            }

            return result;
        }

        /// <summary>
        /// Components should use this to register runtime related
        /// settings for the application to consume.
        /// </summary>
        public void setRuntimeSetting(string name, string value)
        {
            runtimeSettings[name] = value;
        }

        /// <summary>
        /// Get a value for a runtime setting
        /// </summary>
        /// <param name="name"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public string getRuntimeSetting(string name, string defaultValue)
        {
            if (!runtimeSettings.ContainsKey(name))
                return defaultValue;

            return (string)runtimeSettings[name];
        }

        /// <summary>
        /// Used for stuff that needs to be maintained between deployments
        /// to allow for rollbacks, such as database name and credentials.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public string getOrSetRuntimeSettingPersistent(string name, string defaultValue)
        {
            string result = getRuntimeSetting(name, defaultValue);

            if (previousDeployment != null)
            {
                result = previousDeployment.getRuntimeSetting(name, defaultValue);
            }

            // Look for any overrides....
            if (installedApplicationSettings.getRuntimeSettingsOverrides().ContainsKey(name))
            {
                result = installedApplicationSettings.getRuntimeSettingsOverrides()[name];
            }

            setRuntimeSetting(name, result);

            return result;
        }

        public string getShortId()
        {
            return shortid;
        }

        /// <summary>
        /// Get a replacer to deploy runtime settings in templates.
        /// </summary>
        /// <returns></returns>
        public RuntimeSettingsReplacer getSettingsReplacer()
        {
            return new RuntimeSettingsReplacer(this.getRuntimeSettingsToDeploy());
        }

        /// <summary>
        /// Get a setting
        /// </summary>
        /// <typeparam name="TType"></typeparam>
        /// <param name="name"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public Dictionary<string, TType> getSettingCollection<TType>(string collection)
        {
            if (privateData == null || !privateData.ContainsKey(collection))
            {
                privateData[collection] = new Dictionary<string, TType>();
            }

            if (privateData[collection] is Newtonsoft.Json.Linq.JObject)
            {
                privateData[collection] = (privateData[collection] as Newtonsoft.Json.Linq.JObject).castTo<Dictionary<string, TType>>();
            }

            return (Dictionary<string, TType>)privateData[collection];
        }

        /// <summary>
        /// Set a setting.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="value"></param>
        public void setSettingCollection<TType>(string collection, string name, TType value)
        {
            if (privateData == null || !privateData.ContainsKey(collection))
            {
                privateData[collection] = new Dictionary<string, TType>();
            }

            ((Dictionary<string, TType>)privateData[collection])[name] = value;
        }

        /// <summary>
        /// Get a setting
        /// </summary>
        /// <typeparam name="TType"></typeparam>
        /// <param name="name"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public TType getSetting<TType>(string name, TType defaultValue, logger.LoggerInterface logger, bool isEnum = false)
        {
            TType result = defaultValue;

            if (!privateData.ContainsKey(name))
            {
                return defaultValue;
            }

            try
            {
                if (isEnum)
                {
                    result = (TType)Enum.Parse(typeof(TType), Convert.ToString(privateData[name]));
                }
                else
                {
                    result = (TType)privateData[name];
                }
            }
            catch (Exception e)
            {
                logger.LogInfo(false, "source value: '" + Convert.ToString(privateData[name]) + "'");
                logger.LogException(e);
            }

            return result;
        }

        /// <summary>
        /// Set a setting.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="value"></param>
        public void setSetting(string name, object value)
        {
            privateData[name] = value;
        }

        /// <summary>
        /// TODO: Move this to utils library.
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        protected string shortHash(string input, int length = 3)
        {
            using (SHA1Managed sha1 = new SHA1Managed())
            {
                var hash = sha1.ComputeHash(Encoding.UTF8.GetBytes(input));

                //make sure the hash is only alpha numeric to prevent charecters that may break the url
                return string.Concat(Convert.ToBase64String(hash).ToCharArray().Where(x => char.IsLetter(x)).Take(length));
            }
        }

    }
}
