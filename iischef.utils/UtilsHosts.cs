﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace iischef.utils
{
    public class UtilsHosts
    {
        /// <summary>
        /// Devuelve la ubicación del fichero HOSTS
        /// </summary>
        private static string HostsFile
        {
            get
            {
                return Environment.SystemDirectory + @"\drivers\etc\hosts";
            }
        }

        /// <summary>
        /// Devuelve el fichero de hosts como un conjunto de lineas
        /// </summary>
        /// <returns></returns>
        private static List<string> GetHosts()
        {
            var content = File.ReadAllText(HostsFile);
            return System.Text.RegularExpressions.Regex.Split(content, Environment.NewLine).ToList();
        }

        /// <summary>
        /// Añade un mapping al fichero hosts, o lo actualiza en función del hostname.
        /// Evita añadir duplicados o conflictivos.
        /// </summary>
        /// <param name="address"></param>
        /// <param name="hostname"></param>
        public static void AddHostsMapping(string address, string hostname, string id)
        {
            var lines = GetHosts();
            var line = address + " " + hostname;
            var found = false;

            for (int x = 0; x < lines.Count(); x++)
            {
                // Pasamos d elos comentarios.
                if (lines[x].Trim().StartsWith("#"))
                {
                    continue;
                }

                // Si no tiene exactamente dos partes, es que hay algo raro....
                var items = lines[x].Split(" ".ToCharArray());
                if (items.Count() != 2)
                {
                    continue;
                }

                // Lo que hace que dos entradas sean iguales es que tengan
                // el mismo hostname.
                if (String.Equals(items.Last(), hostname, StringComparison.InvariantCultureIgnoreCase))
                {
                    lines[x] = line;
                    found = true;
                    break;
                }
            }

            if (!found)
            {
                lines.Add("# Automated PHP Deployment Host Binding [" + id + "]");
                lines.Add(line);
                File.WriteAllLines(HostsFile, lines, Encoding.Default);
            }
        }
    }
}
