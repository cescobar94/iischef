﻿using System.Collections.Generic;
using System.Management.Automation;

namespace iischef.cmdlet
{
    /// <summary>
    /// Sometimes deployements are tough (specially on IIS)
    /// so we might get cases of "old" stuck websites.
    /// 
    /// Use this to trigger several types of cleanup.
    /// </summary>
    [Cmdlet(VerbsLifecycle.Invoke, "ChefAppCleanup")]
    [OutputType(typeof(List<iischef.core.Configuration.InstalledApplication>))]
    public class ChefAppCleanup : Cmdlet
    {
        protected override void ProcessRecord()
        {
            var app = ConsoleUtils.getApplicationForConsole();
            app.ExecuteCleanup();
        }
    }
}
