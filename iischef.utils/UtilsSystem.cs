﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.IO;
using System.Timers;
using System.Diagnostics;
using System.Security.Principal;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Net;
using System.Net.Security;

namespace iischef.utils
{
    public class UtilsSystem
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="byteCount"></param>
        /// <returns></returns>
        public static String BytesToString(long byteCount)
        {
            string[] suf = { "B", "KB", "MB", "GB", "TB", "PB", "EB" }; //Longs run out around EB
            if (byteCount == 0)
                return "0" + suf[0];
            long bytes = Math.Abs(byteCount);
            int place = Convert.ToInt32(Math.Floor(Math.Log(bytes, 1024)));
            double num = Math.Round(bytes / Math.Pow(1024, place), 1);
            return (Math.Sign(byteCount) * num).ToString() + suf[place];
        }

        public static string AssemblyDirectory
        {
            get
            {
                string codeBase = Assembly.GetExecutingAssembly().CodeBase;
                UriBuilder uri = new UriBuilder(codeBase);
                string path = Uri.UnescapeDataString(uri.Path);
                return Path.GetDirectoryName(path);
            }
        }

        /// <summary>
        /// Load a string resource copied as content for current executiona assembly
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static string getResourceFileAsString(string path)
        {
            return File.ReadAllText(AssemblyDirectory + "\\" + path);
        }

        /// <summary>
        /// Get the runtime path to a resource file.
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static string getResourceFileAsPath(string path)
        {
            return AssemblyDirectory + "\\" + path;
        }

        /// <summary>
        /// Ensure a directory exist.
        /// </summary>
        /// <param name="dir"></param>
        /// <returns></returns>
        public static string DirectoryCreateIfNotExists(string dir)
        {
            if (!System.IO.Directory.Exists(dir))
            {
                System.IO.Directory.CreateDirectory(dir);
            }
            return dir;
        }

        public static void embededResourceToFile(Assembly assembly, string resourceName, string fileName)
        {
            if (File.Exists(fileName))
            {
                throw new Exception("Destination file sould not exist.");
            }

            resourceName = assembly.GetName().Name + "." + resourceName;

            using (Stream stream = assembly.GetManifestResourceStream(resourceName))
            {
                using (var fileStream = File.Create(fileName))
                {
                    stream.CopyTo(fileStream);
                }
            }

        }

        public static string getEmbededResourceAsString(Assembly assembly, string resourceName)
        {

            resourceName = assembly.GetName().Name + "." + resourceName;

            using (Stream stream = assembly.GetManifestResourceStream(resourceName))
            {
                using (StreamReader reader = new StreamReader(stream))
                {
                    return reader.ReadToEnd();
                }
            }
        }

        /// <summary>
        /// Ensure a directory exists. Returns path.
        /// </summary>
        /// <param name="path"></param>
        /// <param name="isDir"></param>
        public static string ensureDirectoryExists(string path, bool isDir = false)
        {
            var dir = isDir ? path : Path.GetDirectoryName(path);
            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }

            return path;
        }

        /// <summary>
        /// Check if we are running in continuous integration
        /// </summary>
        /// <returns></returns>
        public static bool RunningInContinuousIntegration()
        {
            return "True".Equals(Environment.GetEnvironmentVariable("CI"), StringComparison.CurrentCultureIgnoreCase);
        }

        public static void moveDirectory(string source, string destination, string ignoreOnDeployPattern = null)
        {

            try
            {
                System.IO.Directory.Move(source, destination);
            }
            catch (System.IO.IOException e)
            {
                if (e.HResult != -2146232800)
                {
                    throw;
                }

                CopyFilesRecursivelyFast(new DirectoryInfo(source), new DirectoryInfo(destination), false, ignoreOnDeployPattern);
                System.IO.Directory.Delete(source, true);
            }
        }

        public static void CopyFilesRecursively(DirectoryInfo source, DirectoryInfo target, bool overwrite = false)
        {
            foreach (DirectoryInfo dir in source.GetDirectories())
            {
                CopyFilesRecursively(dir, target.CreateSubdirectory(dir.Name));
            }

            foreach (FileInfo file in source.GetFiles())
            {
                file.CopyTo(Path.Combine(target.FullName, file.Name), overwrite);
            }
        }

        /// <summary>
        /// Copy files recursively.
        /// </summary>
        /// <param name="source"></param>
        /// <param name="target"></param>
        /// <param name="overwrite"></param>
        public static void CopyFilesRecursivelyFast(DirectoryInfo source,
            DirectoryInfo target,
            bool overwrite,
            string ignoreOnDeployPattern)
        {
            var files = source.EnumerateFiles("*", SearchOption.AllDirectories);
            ParallelOptions pop = new ParallelOptions();

            pop.MaxDegreeOfParallelism = (int)Math.Ceiling(Environment.ProcessorCount * 0.75);

            Parallel.ForEach(files, pop, (i) =>
            {
                var dir = i.Directory.FullName;

                var relativeDir =
                 dir.Substring(source.FullName.Length, dir.Length - source.FullName.Length)
                    .TrimStart("\\".ToCharArray());

                var relativeFile = Path.Combine(relativeDir, i.Name);

                var destDir = System.IO.Path.Combine(target.FullName, relativeDir);
                var destFile = new FileInfo(Path.Combine(destDir, i.Name));

                if (!String.IsNullOrWhiteSpace(ignoreOnDeployPattern))
                {
                    if (System.Text.RegularExpressions.Regex.IsMatch(relativeFile, ignoreOnDeployPattern))
                    {
                        return;
                    }
                }

                if (!Directory.Exists(destDir))
                {
                    Directory.CreateDirectory(destDir);
                }

                if ((!destFile.Exists) || (destFile.Exists && overwrite))
                {
                    i.CopyTo(destFile.FullName, true);
                }
            });
        }

        /// <summary>
        /// Returns a temprorary path that DOES exist. Some
        /// environment settings might generate per-session temp
        /// paths that are not initialized.
        /// </summary>
        /// <returns></returns>
        public static string GetTempPath(params string[] parts)
        {
            var pathParts = parts.ToList();
            pathParts.Insert(0, Path.GetTempPath());

            var path = CombinePaths(pathParts.ToArray<string>());

            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            return path;
        }

        /// <summary>
        /// Combine several path parts.
        /// </summary>
        /// <param name="paths"></param>
        /// <returns></returns>
        public static string CombinePaths(params string[] paths)
        {
            for (int x = 0; x < paths.Count(); x++)
            {
                if (paths[x] != null)
                {
                    paths[x] = paths[x].Replace("/", "\\");
                }
            }

            string result = paths.First();

            for (int x = 1; x < paths.Count(); x++)
            {
                string variable = paths[x];

                if (variable == null)
                    continue;

                //Eliminamos los trailing "\\" para evitar  que solo devuelva el segundo path
                while (variable.Length > 0 && variable.Substring(0, 1) == "\\")
                    variable = variable.Substring(1, variable.Length - 1);

                if (variable != string.Empty)
                    result = System.IO.Path.Combine(result, variable);
            }

            return result;
        }

        public static bool isAdministrator()
        {
            var identity = WindowsIdentity.GetCurrent();
            var principal = new WindowsPrincipal(identity);
            return principal.IsInRole(WindowsBuiltInRole.Administrator);
        }


        /// <summary>
        /// Delete a directory
        /// </summary>
        /// <param name="strDir">Directory to delete</param>
        /// <param name="forceCloseProcess">Force a process close if it cannot be deleted (i.e. in use)</param>
        /// <param name="waitTimeIfInUse">If in-use, time to waitPauseMs before either failing or closing all processes if forceCloseProcesses is true.</param>
        public static void deleteDirectory(string strDir,
            bool forceCloseProcess = false,
            int waitTimeIfInUse = 4)
        {
            if (!Directory.Exists(strDir))
            {
                return;
            }

            Stopwatch sw = new Stopwatch();
            sw.Start();

            Exception lastException = null;

            while (sw.ElapsedMilliseconds < (waitTimeIfInUse * 1000))
            {
                try
                {
                    deleteDirectoryAndRemovePermissionsIfNeeded(strDir, true);
                    break;
                }
                catch (Exception e)
                {
                    uint errorHResult = (uint)e.HResult;

                    // Code for the error "The process cannot access the file XXX"
                    if (errorHResult != 0x80070020
                        // Code for "Access to the path is denied"
                        && errorHResult != 0x80070005
                        && errorHResult != 0x7FF8FFFB)
                    {
                        throw;
                    }

                    lastException = e;
                    System.Threading.Thread.Sleep(200);
                }
            }

            if (Directory.Exists(strDir) && forceCloseProcess)
            {
                // Maybe the files where "in use"??
                UtilsProcess.ClosePathProcesses(strDir);
                deleteDirectoryAndRemovePermissionsIfNeeded(strDir, true);
            }

            if (Directory.Exists(strDir))
            {
                throw new Exception("Could not completely delete directory: " + strDir, lastException);
            }

        }

        /// <summary>
        /// Make sure that the directory exists.
        /// </summary>
        /// <param name="path"></param>
        public static void ensureDir(string path)
        {
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
        }

        /// <summary>
        /// Cambia los atributos de un fichero quitándole el atributo "ReadOnly"
        /// </summary>
        /// <param name="strFile"></param>
        public static void SetNotReadOnlyFile(string strFile)
        {

            FileInfo oFile = new FileInfo(strFile);
            if (oFile.Exists)
            {

                if ((oFile.Attributes & FileAttributes.ReadOnly) != 0)
                {
                    oFile.Attributes -= FileAttributes.ReadOnly;
                }

                File.SetAttributes(strFile, File.GetAttributes(strFile) & ~FileAttributes.System);
                File.SetAttributes(strFile, File.GetAttributes(strFile) & ~FileAttributes.Hidden);
                File.SetAttributes(strFile, File.GetAttributes(strFile) & ~(FileAttributes.Archive | FileAttributes.ReadOnly));
            }

            oFile = null;

        }

        public static void SetNotReadOnlyDirectory(string strDir)
        {

            DirectoryInfo oDir = new DirectoryInfo(strDir);

            if (oDir.Exists)
            {

                if ((oDir.Attributes & FileAttributes.ReadOnly) != 0)
                {
                    oDir.Attributes -= FileAttributes.ReadOnly;
                }

                File.SetAttributes(strDir, File.GetAttributes(strDir) & ~FileAttributes.System);
                File.SetAttributes(strDir, File.GetAttributes(strDir) & ~FileAttributes.Hidden);
                File.SetAttributes(strDir, File.GetAttributes(strDir) & ~(FileAttributes.Archive | FileAttributes.ReadOnly));
            }

            oDir = null;
        }

        /// <summary>
        /// Cambia los atributos de un directorio quitándole el atributo "ReadOnly"
        /// </summary>
        /// <param name="strDir"></param>
        public static void deleteDirectoryAndRemovePermissionsIfNeeded(string strDir, bool delete = false)
        {
            DirectoryInfo oDirBase = new DirectoryInfo(strDir);

            try
            {
                // Try a recursive delete, if we are lucky it might work
                // without the super slow permission crawl
                Directory.Delete(strDir, true);
            }
            catch { }

            if (!oDirBase.Exists)
            {
                return;
            }

            foreach (FileInfo oFile in oDirBase.EnumerateFiles("*.*"))
            {
                try
                {
                    SetNotReadOnlyFile(oFile.FullName);
                }
                catch { }

                try { File.Delete(oFile.FullName); } catch { }
            }

            foreach (DirectoryInfo oDir in oDirBase.EnumerateDirectories("*.*"))
            {
                try
                {
                    SetNotReadOnlyDirectory(oDir.FullName);
                }
                catch { }

                try
                {
                    deleteDirectoryAndRemovePermissionsIfNeeded(oDir.FullName, delete);
                }
                catch (Exception e)
                {
                    try
                    {
                        Directory.Delete(oDir.FullName);
                    }
                    catch { }
                }
            }

            try
            {
                SetNotReadOnlyDirectory(strDir);
            }
            catch { }

            // If this still exists... then throw the
            // exception if this still fails...
            if (Directory.Exists(strDir))
            {
                Directory.Delete(strDir, true);
            }

        }

        /// <summary>
        /// Total free space for the given drive of a path in BYTES
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static long GetTotalFreeSpace(string path)
        {
            string driveName = Path.GetPathRoot(path);

            foreach (DriveInfo drive in DriveInfo.GetDrives())
            {
                if (drive.IsReady && string.Equals(drive.Name, driveName, StringComparison.OrdinalIgnoreCase))
                {
                    return drive.TotalFreeSpace;
                }
            }

            return -1;
        }

        /// <summary>
        /// Download the contents of a remote URL as plain text.
        /// * Does not fail on SSL validation errors (for self signed certificates, etc..)
        /// * Retries the download to deal with connection glitches
        /// </summary>
        /// <param name="uri"></param>
        /// <returns></returns>
        public static string DownloadUriAsText(string uri)
        {
            string response;

            WebClient client = new WebClient();

            //Change SSL checks so that all checks pass
            ServicePointManager.ServerCertificateValidationCallback =
                new RemoteCertificateValidationCallback(
                    delegate
                    { return true; }
                );

            try
            {
                response = client.DownloadString(uri);
            }
            catch
            {
                // Sometimes requests made inmediately after an IIS
                // reconfiguration can fail as it takes some time for
                // IIS to materialize the changes
                System.Threading.Thread.Sleep(500);
                response = client.DownloadString(uri);
            }
            finally
            {
                // Restore SSL validation behaviour
                ServicePointManager.ServerCertificateValidationCallback = null;
            }

            return response;
        }
    }
}
