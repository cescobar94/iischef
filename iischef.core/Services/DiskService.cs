﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iischef.utils;
using System.IO;

using Microsoft.Synchronization;
using Microsoft.Synchronization.Files;

namespace iischef.core.Services
{
    /// <summary>
    /// Servicio de aprovisionamiento de almacenaje
    /// persistente para las aplicaciones
    /// </summary>
    public class DiskService : DeployerBase, DeployerInterface
    {
        /// <summary>
        /// All disk storage for this application is pointed to this directory.
        /// </summary>
        /// <param name="settings"></param>
        /// <returns></returns>
        protected string GetStoragePath(DiskServiceSettings settings)
        {
            var storage = GlobalSettings.GetDefaultContentStorage();

            // Generate a unique virtual disk for this application
            var baseStoragePath = UtilsSystem.ensureDirectoryExists(UtilsSystem.CombinePaths(storage.path,
                "store_" + Deployment.installedApplicationSettings.getId()));

            return baseStoragePath;
        }

        public void deploy()
        {
            var diskSettings = this.DeployerSettings.castTo<DiskServiceSettings>();

            var baseStoragePath = this.GetStoragePath(diskSettings);

            if (diskSettings.mounts == null || !diskSettings.mounts.Any())
            {
                throw new Exception("You must specify at least a mount for a disk service.");
            }

            // Each one of these is to be mounted as a symlink/junction
            foreach (var mount in diskSettings.mounts)
            {
                if (string.IsNullOrWhiteSpace(diskSettings.id))
                {
                    throw new Exception("Disk settings must have an id");
                }

                if (string.IsNullOrWhiteSpace(mount.Value.id))
                {
                    throw new Exception("All mounts in disk configuration must have an id");
                }

                Logger.LogInfo(true, "Mounting disk: " + mount.Value.id);

                // Expand the local path..
                var mountDestination = UtilsSystem.ensureDirectoryExists(UtilsSystem.CombinePaths(baseStoragePath, mount.Value.path), true);

                var settingkey = $"services.{diskSettings.id}.mount.{mount.Value.id}.path";

                // We might sometimes need to force a specific path in an environment...
                if (Deployment.installedApplicationSettings.getRuntimeSettingsOverrides().ContainsKey(settingkey))
                {
                    string newMountDestination = Deployment.installedApplicationSettings.getRuntimeSettingsOverrides()[settingkey];
                    if (Directory.Exists(newMountDestination))
                    {
                        Logger.LogInfo(false, "Default mount for '{0}' overriden with '{1}' from a defavult value of '{2}'.", settingkey, newMountDestination, mountDestination);
                        mountDestination = newMountDestination;
                    }
                    else
                    {
                        Logger.LogInfo(false, "Tried to override mount path ({0}) with a non-existent directory: '{1}'", settingkey, newMountDestination);
                    }
                }

                // Ensure proper permissions
                Logger.LogInfo(true, "Ensure mount has proper user permissions for account '{0}'", Deployment.windowsUsername);
                UtilsWindowsAccounts.AddPermissionToDirectoryIfMissing(Deployment.windowsUsername, mountDestination);

                string mountPath = null;
                if (!String.IsNullOrWhiteSpace(mount.Value.mountpath))
                {
                    mountPath = UtilsSystem.CombinePaths(Deployment.appPath, mount.Value.mountpath);
                    bool mounted = UtilsJunction.EnsureJuction(mountPath, mountDestination, Logger, mount.Value.persist_on_deploy);
                    if (!mounted)
                    {
                        throw new Exception("Could not mount junction:" + mountPath);
                    }
                }

                Deployment.setRuntimeSetting(settingkey, mountDestination);

                Deployment.setSettingCollection($"service.{diskSettings.id}", settingkey, new DiskStore()
                {
                    path = mountDestination,
                    junction = mountPath,
                    junctionRealPath = UtilsJunction.resolvePath(mountPath)
                });
            }
        }

        public void sync()
        {
            base.syncCommon<DiskService>();
        }

        public override void _sync(Object input)
        {
            DiskService other = (DiskService)input;
            var diskSettings = this.DeployerSettings.castTo<DiskServiceSettings>();
            var storage = GlobalSettings.GetDefaultContentStorage();

            // Generate a unique virtual disk for this application
            DiskServiceSettings otherSettings = other.DeployerSettings.castTo<DiskServiceSettings>();
            foreach (var mount in diskSettings.mounts)
            {
                string pathOri = UtilsSystem.ensureDirectoryExists(other.Deployment.getRuntimeSettingsToDeploy()["services." + otherSettings.id + ".mount.files.path"]);
                string pathDest = UtilsSystem.ensureDirectoryExists(Deployment.getRuntimeSettingsToDeploy()["services." + diskSettings.id + ".mount.files.path"]);
                FileSyncProvider ori = new FileSyncProvider(pathOri);
                FileSyncProvider dest = new FileSyncProvider(pathDest);

                SyncOrchestrator agent = new SyncOrchestrator();
                agent.LocalProvider = ori;
                agent.RemoteProvider = dest;
                agent.Direction = SyncDirectionOrder.Upload;

                SyncOperationStatistics syncStats = agent.Synchronize();
                Logger.LogInfo(true, "Synchronization stats \n\n local provider {0} to remote {1}\n upload changes applied {2}\n {3} upload changes failed",
                    pathOri,
                    pathDest,
                    syncStats.UploadChangesApplied,
                    syncStats.UploadChangesFailed);
                ori.Dispose();
                dest.Dispose();
            }
        }

        public void undeploy(bool isUninstall = false)
        {
            // Not that we want to accidentally delete contents and files
            // for an application...
            if (!isUninstall)
            {
                return;
            }

            var diskSettings = this.DeployerSettings.castTo<DiskServiceSettings>();
            var mounts = Deployment.getSettingCollection<DiskStore>($"service.{diskSettings.id}");

            foreach (var m in mounts.Values)
            {
                // Most of the time this directory will not exist, as the base storage deployer will already have
                // deleted the application folder. But for "local" installed applications, this removes
                // the symlinks.
                if (!String.IsNullOrWhiteSpace(m.junctionRealPath) && Directory.Exists(m.junctionRealPath))
                {
                    UtilsJunction.RemoveJunction(m.junctionRealPath);
                }

                if (Directory.Exists(m.path))
                {
                    utils.UtilsSystem.deleteDirectoryAndRemovePermissionsIfNeeded(m.path, true);
                }
            }

            var baseStoragePath = this.GetStoragePath(diskSettings);
            if (!string.IsNullOrWhiteSpace(baseStoragePath) &&  Directory.Exists(baseStoragePath))
            {
                UtilsSystem.deleteDirectoryAndRemovePermissionsIfNeeded(baseStoragePath, true);
            }
        }

        public void start()
        {

        }

        public void stop()
        {

        }

        public void deploySettings(string jsonSettings,
            string jsonSettingsNested,
            RuntimeSettingsReplacer replacer)
        {

        }
    }
}
