﻿namespace iischef.cmdlet
{
    class ConsoleUtils
    {
        /// <summary>
        /// Get an application configured for usage through console (with a console logger and verbose mode)
        /// </summary>
        /// <returns></returns>
        public static core.Application getApplicationForConsole(bool initialize = true)
        {
            var logger = new logger.ConsoleLogger();
            logger.setVerbose(true);
            var app = new core.Application(logger);

            if (initialize)
            {
                app.initialize();
                app.useParentLogger();
            }

            return app;
        }
    }
}
