﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Net;
using System.IO;
using System.IO.Compression;
using Newtonsoft.Json.Linq;
using iischef.utils;
using System.Text.RegularExpressions;

namespace iischef.utils.AppVeyor
{
    /// <summary>
    /// Cliente para trabajar sobre la API de AppVeyor
    /// </summary>
    public class Client
    {
        /// <summary>
        /// </summary>
        protected string Token;

        /// <summary>
        /// Base URI for the API i.e. (https://ci.appveyor.com)
        /// </summary>
        protected string BaseUri;

        /// <summary>
        /// The logger
        /// </summary>
        protected logger.LoggerInterface Logger;

        /// <summary>
        /// Get an instance of AppVeyorClient
        /// </summary>
        /// <param name="token">API Token</param>
        /// <param name="baseUri">Base URI</param>
        /// <param name="logger"></param>
        public Client(
            string token,
            string baseUri,
            logger.LoggerInterface logger)
        {
            this.Token = token;
            this.Logger = logger;
            this.BaseUri = baseUri;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="job"></param>
        /// <param name="build"></param>
        /// <param name="artifactRegex"></param>
        /// <returns></returns>
        protected Artifact FindDefaultArtifactForBuild(
            Job job,
            Build build,
            string artifactRegex)
        {
            var artifacts = GetArtifactListFromJob(job);

            if (!artifacts.Any())
            {
                throw new Exception($"Requested job '{job.jobId}' from version '{build.version}' has no artifacts.");
            }

            // Try to find a suitable artifact...
            Artifact artifact = null;

            // Search by regex.
            if (!string.IsNullOrWhiteSpace(artifactRegex))
            {
                foreach (var arf in artifacts)
                {
                    string arfname = (string)arf.fileName;

                    if (Regex.IsMatch(arfname, artifactRegex))
                    {
                        if (artifact != null)
                            throw new Exception($"Ambiguous artifact match for regex: '{artifactRegex}'.");

                        artifact = arf;
                    }
                }
            }

            if (artifact != null)
            {
                return artifact;
            }

            // If there is only one artifact use that one.
            if (artifacts.Count() == 1)
            {
                return artifacts.First();
            }

            // Use an artifact whose name defaults to the project's name
            artifact = (from p in artifacts
                        where String.Equals(p.name, build.project.name, StringComparison.InvariantCultureIgnoreCase)
                        select p).FirstOrDefault();

            if (artifact != null)
            {
                return artifact;
            }

            // Now use the file name
            artifact = (from p in artifacts
                        where String.Equals(p.fileName, build.project.name + ".zip", StringComparison.InvariantCultureIgnoreCase)
                        select p).FirstOrDefault();

            if (artifact != null)
            {
                return artifact;
            }

            // Our best bet is to find the artifact with the biggest size...
            artifact = artifacts.OrderByDescending((i) => i.size).Where((i) => string.Equals(i.type, "zip", StringComparison.CurrentCultureIgnoreCase)).FirstOrDefault();

            if (artifact == null)
            {
                throw new Exception($"Could not find suitable artifact. Regex: '{artifactRegex}'.");
            }

            return artifact;
        }

        /// <summary>
        /// Downloads (And extracts) single artifacts from jobs.
        /// </summary>
        /// <param name="build"></param>
        /// <param name="artifactRegex"></param>
        /// <param name="path"></param>
        /// <param name="logger"></param>
        public void DownloadSingleArtifactFromBuild(
            Build build,
            string artifactRegex,
            string path,
            logger.LoggerInterface logger)
        {
            UtilsSystem.ensureDirectoryExists(path, true);

            // Use the first job in the build...
            var job = build.jobs.First();
            var artifact = FindDefaultArtifactForBuild(job, build, artifactRegex);

            var filename = artifact.fileName;
            var extension = Path.GetExtension(filename);

            var tmpDir = UtilsSystem.GetTempPath("chefappveyor");

            // Delete all stale temporary files.
            foreach (var f in System.IO.Directory.EnumerateFiles(tmpDir).ToList())
            {
                var fifo = new FileInfo(f);
                if ((DateTime.UtcNow - fifo.LastAccessTimeUtc).TotalHours > 8)
                {
                    // Make this fail proof, it's just a cleanup.
                    try
                    {
                        fifo.Delete();
                    }
                    catch { }
                }
            }

            // Use a short has as the temporary file name, because long paths can have issues...
            var tmpFile = UtilsSystem.CombinePaths(
                tmpDir, UtilsEncryption.getShortHash(Newtonsoft.Json.JsonConvert.SerializeObject(build) + filename) + extension);

            // TODO: Corrupt files will live in the temporary cache until it is cleared...
            if (!File.Exists(tmpFile))
            {
                var url = $"/api/buildjobs/{job.jobId}/artifacts/{filename}";
                logger.LogInfo(false, "Downloading artifact from: '{0}' to '{1}'", url, tmpFile);
                ExecuteApiCallToFile(url, tmpFile);
            }
            else
            {
                logger.LogInfo(true, "Skipping artifact download, already in local cache: {0}", tmpFile);
            }

            if (Path.GetExtension(tmpFile)?.ToLower() != ".zip")
            {
                throw new NotImplementedException("AppVeyor artifacts should only be Zip Files.");
            }

            logger.LogInfo(true, "Unzipping file....");
            ZipFile.ExtractToDirectory(tmpFile, path);
            logger.LogInfo(true, "Unzipping finished....");
        }

        /// <summary>
        /// Get a build object from it's id.
        /// </summary>
        /// <param name="version"></param>
        /// <param name="user"></param>
        /// <param name="project"></param>
        /// <returns></returns>
        public Build GetBuildFromVersion(string version, string user, string project)
        {
            var path = String.Format("/api/projects/{0}/{1}/build/{2}",
                System.Web.HttpUtility.UrlEncode(user),
                System.Web.HttpUtility.UrlEncode(project),
                System.Web.HttpUtility.UrlEncode(version));

            // Cache this call for 30 minutes... once a build is
            // finished it won't change!
            var buildObj = ExecuteApiCall(path, 120);

            var build = Newtonsoft.Json.JsonConvert.DeserializeObject<Build>(buildObj["build"].ToString());
            build.project = Newtonsoft.Json.JsonConvert.DeserializeObject<Project>(buildObj["project"].ToString());

            return build;
        }

        /// <summary>
        /// Find the last build job that succeeded. Maximum history introspection of 50 builds.
        /// </summary>
        /// <param name="user"></param>
        /// <param name="project"></param>
        /// <param name="branch"></param>
        /// <param name="maxHistory"></param>
        /// <param name="buildVersionRequested"></param>
        /// <param name="exp"></param>
        /// <param name="maxResults">Maximum number of results to resturn</param>
        /// <returns></returns>
        public List<Build> FindLastSuccessfulBuilds(
            string user,
            string project,
            string branch = null,
            int maxHistory = 50,
            string buildVersionRequested = null,
            string exp = null,
            int maxResults = 1)
        {
            // TODO: This API endpoint has a lastbuild argument, that could be more reliable and optimum (to keep
            // track of last build artifact ID and only introspect any new builds since then).
            string path = $"/api/projects/{user}/{System.Web.HttpUtility.UrlEncode(project)}/history";

            List<string> queryStringParts = new List<string>();

            queryStringParts.Add($"recordsNumber={maxHistory}");

            if (!string.IsNullOrWhiteSpace(branch))
            {
                queryStringParts.Add($"branch={HttpUtility.UrlEncode(branch)}");
            }

            // We add this here to ensure that no intermediate caches mess up with the data.
            queryStringParts.Add($"random={Guid.NewGuid()}");

            path += "?" + string.Join("&", queryStringParts);

            var items = ExecuteApiCall(path);

            List<Build> builds = new List<Build>();

            foreach (var jBuild in (JArray)items["builds"])
            {
                var build = Newtonsoft.Json.JsonConvert.DeserializeObject<Build>(jBuild.ToString());

                if (!string.Equals(build.status, "success", StringComparison.CurrentCultureIgnoreCase))
                {
                    // this.Logger.LogInfo(true, $"Skipped build '{build.version}' with invalid status '{build.status}'");
                    continue;
                }

                // Only pass through specific version/buildid
                if (build.version != buildVersionRequested && !string.IsNullOrWhiteSpace(buildVersionRequested))
                {
                    // this.Logger.LogInfo(true, $"Skipped build '{build.version}' with unrequested version '{build.status}'");
                    continue;
                }

                if (!String.IsNullOrWhiteSpace(exp))
                {
                    System.Text.RegularExpressions.Regex reg = new System.Text.RegularExpressions.Regex(exp);
                    if (!reg.IsMatch(build.message))
                    {
                        // this.Logger.LogInfo(true, $"Skipped build '{build.version}' with unmatching build message regex '{exp}' '{build.message}'");
                        continue;
                    }
                }

                // The previous build object does not contain job information.
                build = this.GetBuildFromVersion(build.version, user, project);
                builds.Add(build);

                // This build has successful jobs... great.
                if (builds.Count >= maxResults)
                {
                    break;
                }
            }

            return builds;
        }

        /// <summary>
        /// Get the list of artifacts for a specific job
        /// </summary>
        /// <param name="job"></param>
        /// <returns></returns>
        public List<Artifact> GetArtifactListFromJob(Job job)
        {
            string path = $"/api/buildjobs/{HttpUtility.UrlEncode(job.jobId)}/artifacts";

            JToken apiCallResult = ExecuteApiCall(path, 60);

            return Newtonsoft.Json.JsonConvert.DeserializeObject<List<Artifact>>(apiCallResult.ToString());
        }

        /// <summary>
        /// Prepares a WebClient with the needed auth 
        /// headers to attack the AppVeyor API
        /// </summary>
        /// <returns></returns>
        protected WebClient PrepareWebClient()
        {
            WebClient client = new WebClient();
            client.Headers.Set("Authorization", "Bearer " + this.Token);
            client.Headers.Set("Content-Type", "application/json");
            return client;
        }

        /// <summary>
        /// Execute an API call
        /// </summary>
        /// <param name="uri">The remote URI</param>
        /// <param name="cacheFor">Cache the results for the specified amount of minutes.</param>
        /// <returns></returns>
        protected JToken ExecuteApiCall(
            string uri,
            int cacheFor = 0)
        {
            // this.Logger.LogInfo(true, $"Executing AppVeyor API call with cache of {cacheFor}s: '{uri}'");

            string cacheDir = Path.Combine(Path.GetTempPath(), "AppVeyorClientCache");

            if (!Directory.Exists(cacheDir))
            {
                Directory.CreateDirectory(cacheDir);
            }
            else
            {
                // Remove all files older than 1 day
                foreach (var f in (new DirectoryInfo(cacheDir)).EnumerateFiles())
                {
                    if ((DateTime.UtcNow - f.LastWriteTimeUtc).TotalHours > 24
                        && f.Extension == ".json")
                    {
                        f.Delete();
                    }
                }
            }

            if (cacheFor > 0)
            {
                // Add some variabliy soy the won't expire all at once...
                cacheFor = cacheFor + ((new Random()).Next(0, 5));
            }

            string cacheFile = Path.Combine(cacheDir, utils.UtilsEncryption.GetMD5(uri) + ".json");
            if (cacheFor > 0 && File.Exists(cacheFile))
            {
                var fifo = new FileInfo(cacheFile);
                if ((DateTime.UtcNow - fifo.LastWriteTimeUtc).TotalMinutes > cacheFor)
                {
                    File.Delete(fifo.FullName);
                }

                try
                {
                    var data = File.ReadAllText(cacheFile);
                    return Newtonsoft.Json.Linq.JToken.Parse(data);
                }
                catch
                {
                    // ignored
                }
            }

            var url = this.BaseUri + uri;
            WebClient client = PrepareWebClient();
            string result = client.DownloadString(url);
            var parsed = Newtonsoft.Json.Linq.JToken.Parse(result);

            if (cacheFor > 0)
            {
                File.WriteAllText(cacheFile, result);
            }

            return parsed;
        }

        /// <summary>
        /// Run an API call and output results directly to a file
        /// mostly used to download artifacts...
        /// </summary>
        /// <param name="uri"></param>
        /// <param name="localPath"></param>
        protected void ExecuteApiCallToFile(string uri, string localPath)
        {
            iischef.utils.UtilsSystem.ensureDirectoryExists(localPath);

            var url = this.BaseUri + uri;
            WebClient client = PrepareWebClient();
            client.DownloadFile(url, localPath);
        }
    }
}
