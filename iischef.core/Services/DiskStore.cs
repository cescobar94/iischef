﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iischef.core.Services
{
    public class DiskStore
    {
        /// <summary>
        /// Physical storage path.
        /// </summary>
        public string path { get; set; }

        /// <summary>
        /// Junction path.
        /// </summary>
        public string junction { get; set; }

        /// <summary>
        /// Realpath to the junction, if it was nested
        /// inside another junction...
        /// </summary>
        public string junctionRealPath { get; set; }
    }
}
