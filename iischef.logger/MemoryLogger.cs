﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Security.Cryptography;
using System.Security.Principal;
using System.IO;
using System.Net;

namespace iischef.logger
{
    /// <summary>
    /// Logger (system log) for the application.
    /// </summary>
    public class MemoryLogger : BaseLogger, LoggerInterface
    {
        protected string path;

        protected StringWriter sw;

        public string getLog()
        {
            return sw.ToString();
        }

        /// <summary>
        /// Start a filesystem based logger.
        /// </summary>
        /// <param name="path">File name for the log file.</param>
        public MemoryLogger()
        {
            sw = new StringWriter();
        }

        protected override void doWrite(string content, System.Diagnostics.EventLogEntryType type)
        {
            sw.WriteLine("---log:" + DateTime.Now.ToLongDateString() + " | " + DateTime.Now.ToLongTimeString() + "|" + type.ToString());
            sw.Write(content);
            sw.WriteLine("");
        }
    }
}
