﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iischef.utils;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using smo = Microsoft.SqlServer.Management.Smo;
using Microsoft.SqlServer.Management.Common;

namespace iischef.core.Services
{
    class SQLService : DeployerBase, DeployerInterface
    {
        protected SystemConfiguration.SQLServer getSqlServer()
        {
            // We can have an app_setting configuration
            // to route a whole application to a specific sql server
            string sqlTarget = null;
            if (this.Deployment.installedApplicationSettings.configuration["sqltarget"] != null)
            {
                sqlTarget = Convert.ToString(this.Deployment.installedApplicationSettings.configuration["sqltarget"]);
            }

            var sqlServer = GlobalSettings.GetSqlServer(sqlTarget);
            Logger.LogInfo(true, "SQL Target: " + sqlServer.id);

            return sqlServer;
        }

        public void deploy()
        {
            var sqlSettings = this.DeployerSettings.castTo<SQLServiceSettings>();
            var sqlServer = this.getSqlServer();

            if (string.IsNullOrWhiteSpace(sqlSettings.id))
            {
                throw new Exception("SQL Service request id must have a value.");
            }

            var id = Deployment.installedApplicationSettings.getId() + "_" + sqlSettings.id;

            string keylogin = $"services.{sqlSettings.id}.username";
            string keypassword = $"services.{sqlSettings.id}.password";
            string keydatabase = $"services.{sqlSettings.id}.database";

            // The database name, username and password must remain the same between deployments.
            // If we generated new user/pwd for new deployment, rollback functionality would NOT work as expected
            // as it would require re-deploying the logins.
            var dbLogin = Deployment.getOrSetRuntimeSettingPersistent(
                keylogin,
                "chf_" + id);

            var dbPassword = Deployment.getOrSetRuntimeSettingPersistent(
                keypassword,
                Guid.NewGuid().ToString());

            var dbDatabase = Deployment.getOrSetRuntimeSettingPersistent(
                keydatabase,
                "chf_" + id);

            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder(sqlServer.connectionString);

            var utils = new UtilsSQLServer(this.Logger);

            // Ensure we have database and login.
            Logger.LogInfo(true, "Getting SQL connection '{0}'", sqlServer.connectionString);
            var connection = utils.getServerConnection(sqlServer.connectionString);

            if (connection == null)
            {
                throw new Exception("Could not connect to the server: " + sqlServer.connectionString);
            }

            Logger.LogInfo(true, "Getting SQL database '{0}'", dbDatabase);
            var database = utils.findDatabase(connection, dbDatabase, true);

            if (!database.Status.HasFlag(smo.DatabaseStatus.Normal))
            {
                throw new Exception("Database should be in 'Normal' status. The current database status is not compatible with automated deployments: " +
                    database.Status.ToString());
            }

            Logger.LogInfo(true, "Getting SQL login '{0}'", dbLogin);
            var login = utils.findLogin(connection, dbDatabase, dbLogin, dbPassword, true);
            Logger.LogInfo(true, "Getting SQL user '{0}'", login);
            var user = utils.BindUser(database, login, true);

            Deployment.setRuntimeSetting($"services.{sqlSettings.id}.host", builder.DataSource);
        }

        public void undeploy(bool isUninstall = false)
        {
            if (!isUninstall)
            {
                return;
            }

            var sqlSettings = this.DeployerSettings.castTo<SQLServiceSettings>();
            var sqlServer = this.getSqlServer();

            string keylogin = $"services.{sqlSettings.id}.username";
            string keydatabase = $"services.{sqlSettings.id}.database";

            // The database name, username and password must remain the same between deployments.
            // If we generated new user/pwd for new deployment, rollback functionality would NOT work as expected
            // as it would require re-deploying the logins.
            var dbLogin = Deployment.getRuntimeSetting(keylogin, null);
            var dbDatabase = Deployment.getRuntimeSetting(keydatabase, null);

            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder(sqlServer.connectionString);

            var utils = new UtilsSQLServer(this.Logger);

            var connection = utils.getServerConnection(sqlServer.connectionString);

            if (connection != null)
            {
                utils.deleteDatabase(connection, dbDatabase);
                utils.deleteLogin(connection, dbLogin);
            }
        }

        public void start()
        {

        }

        public void stop()
        {

        }


        public void deploySettings(string jsonSettings,
            string jsonSettingsNested,
            RuntimeSettingsReplacer replacer)
        {

        }

        public void sync()
        {
            base.syncCommon<SQLService>();
        }

        public override void _sync(Object input)
        {
            SQLService parent = (SQLService)input;

            string database = this.Deployment.getRuntimeSettingsToDeploy()["services." + DeployerSettings.castTo<SQLServiceSettings>().id + ".database"];
            string parentDatabase = parent.Deployment.getRuntimeSettingsToDeploy()["services." + parent.DeployerSettings.castTo<SQLServiceSettings>().id + ".database"];

            var sqlServer = this.getSqlServer();

            using (SqlConnection connection = new SqlConnection(sqlServer.connectionString))
            {
                connection.Open();

                ServerConnection serv = new ServerConnection(connection);

                smo.Server serverTemp = new smo.Server(serv);

                string backupDir = serverTemp.BackupDirectory;
                string dataDir = serverTemp.MasterDBPath;

                Logger.LogInfo(true, "SQL Server Version: " + serverTemp.VersionString);
                Logger.LogInfo(true, "SQL Server Edition: " + serverTemp.Edition);

                var backupName = database + DateTime.Now.ToString("yyyyMMddHHmmssffff");
                var backupFile = UtilsSystem.CombinePaths(backupDir, backupName + ".bak");

                SqlCommand cmd;

                // Timeout for long running processes (i.e. backup and restore)
                int longProcessTimeout = 120;

                try
                {
                    string query = null;

                    // EngineEdition Database Engine edition of the instance of SQL Server installed on the server.
                    //  1 = Personal or Desktop Engine(Not available in SQL Server 2005 and later versions.)
                    //  2 = Standard(This is returned for Standard, Web, and Business Intelligence.)
                    //  3 = Enterprise(This is returned for Evaluation, Developer, and both Enterprise editions.)
                    //  4 = Express(This is returned for Express, Express with Tools and Express with Advanced Services)
                    //  5 = SQL Database
                    //  6 - SQL Data Warehouse

                    bool supportsCompression = serverTemp.EngineEdition != smo.Edition.Express;

                    string compressionOption = supportsCompression ? "COMPRESSION," : "";

                    query = String.Format("BACKUP DATABASE [{0}] to disk = '{1}' WITH {3} name = '{2}'",
                        parentDatabase, backupFile, backupName, compressionOption);

                    Logger.LogInfo(true, "CMD: {0}", query);
                    cmd = new SqlCommand(query, connection);
                    cmd.CommandTimeout = longProcessTimeout;
                    cmd.ExecuteNonQuery();

                    query = string.Format(@"DECLARE @kill varchar(8000) = '';  
SELECT @kill = @kill + 'kill ' + CONVERT(varchar(5), session_id) + ';'  
FROM sys.dm_exec_sessions
WHERE database_id  = db_id('{0}')",
database);

                    Logger.LogInfo(true, "CMD: {0}", query);
                    cmd = new SqlCommand(query, connection);
                    cmd.ExecuteNonQuery();

                    query = String.Format("ALTER DATABASE [{0}] SET SINGLE_USER WITH ROLLBACK IMMEDIATE",
                        database);

                    Logger.LogInfo(true, "CMD: {0}", query);
                    cmd = new SqlCommand(query, connection);
                    cmd.ExecuteNonQuery();

                    query = String.Format("RESTORE filelistonly FROM disk='{0}'", backupFile);

                    // Before restoring, print out logical file names
                    cmd = new SqlCommand(query, connection);
                    Dictionary<string, string> info = new Dictionary<string, string>();
                    using (var reader = cmd.ExecuteReader())
                    {
                        int row = 0;

                        while (reader.Read())
                        {
                            for (var i = 0; i < reader.FieldCount; i++)
                            {
                                var columname = reader.GetName(i);
                                var columnvalue = Convert.ToString(reader[i]);
                                info.Add(row.ToString() + "_" + columname, columnvalue);
                            }

                            row++;
                        }
                    }

                    // logger.LogInfo(true, "BACKUP DETAILS: {0}", Newtonsoft.Json.JsonConvert.SerializeObject(info, Newtonsoft.Json.Formatting.Indented));

                    // Hay que extraer los nombres lógicos de bdd y log
                    string dataLogicalName = info["0_LogicalName"];
                    string logLogicalName = info["1_LogicalName"];

                    query = String.Format("RESTORE DATABASE [{0}] FROM DISK = '{1}' WITH REPLACE, MOVE '{2}' TO '{4}\\{0}.mdf', MOVE '{3}' TO '{4}\\{0}.ldf';",
                        database, backupFile, dataLogicalName, logLogicalName, dataDir);

                    Logger.LogInfo(true, "CMD: {0}", query);
                    cmd = new SqlCommand(query, connection);
                    cmd.CommandTimeout = longProcessTimeout;
                    cmd.ExecuteNonQuery();

                    query = String.Format("ALTER DATABASE [{0}] SET MULTI_USER", database);

                    Logger.LogInfo(true, "CMD: {0}", query);
                    cmd = new SqlCommand(query, connection);
                    cmd.ExecuteNonQuery();

                    #region Delete backup file using shell commands from within SQL Server

                    serverTemp.ConnectionContext.ExecuteNonQuery("EXEC sp_configure 'show advanced options', 1");
                    serverTemp.ConnectionContext.ExecuteNonQuery("RECONFIGURE");
                    serverTemp.ConnectionContext.ExecuteNonQuery("EXEC sp_configure 'xp_cmdshell', 1");
                    serverTemp.ConnectionContext.ExecuteNonQuery("RECONFIGURE");
                    serverTemp.ConnectionContext.ExecuteNonQuery(String.Format("xp_cmdshell 'del \"{0}\"'", backupFile));
                    serverTemp.ConnectionContext.ExecuteNonQuery("EXEC sp_configure 'xp_cmdshell', 0");
                    serverTemp.ConnectionContext.ExecuteNonQuery("EXEC sp_configure 'show advanced options', 0");
                    serverTemp.ConnectionContext.ExecuteNonQuery("RECONFIGURE");

                    #endregion

                }
                finally
                {
                    // Make sure that we remove single_user_mode
                    try
                    {
                        string query = null;

                        query = String.Format("ALTER DATABASE [{0}] SET MULTI_USER", database);
                        cmd = new SqlCommand(query, connection);
                        cmd.ExecuteNonQuery();
                    }
                    catch { }
                }

                connection.Close();
            }

            // After doing the SYNC we need to "re-deploy" the database so that
            // user accounts are properly setup for the new application.
            deploy();
        }
    }
}
