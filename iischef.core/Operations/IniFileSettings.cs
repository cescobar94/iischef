﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.IO.Compression;
using System.Web;
using System.Net;

namespace iischef.core.Operations
{
    class IniFileSettings
    {
        /// <summary>
        /// Uri to download this extension from
        /// </summary>
        public string key { get; set; }

        /// <summary>
        /// Solo hay dos tipos:
        /// directo
        /// zip
        /// </summary>
        public string value { get; set; }

        /// <summary>
        /// Section name
        /// </summary>
        public string section { get; set; }

        /// <summary>
        /// If this is a multivalue directive
        /// </summary>
        public bool multivalue { get; set; }

        /// <summary>
        /// Wether or not to comment the entry
        /// </summary>
        public bool comment { get; set; }

        /// <summary>
        /// Host based ini settings
        /// </summary>
        public string host { get; set; }

        /// <summary>
        /// For values that point to a directory or file,
        /// ensure the directory is created.
        /// </summary>
        public string ensureDir { get; set; }

        /// <summary>
        /// Execute the opreation...
        /// </summary>
        /// <param name="destination"></param>
        public void execute(Php.IniFileManager manager, Deployment deployment)
        {
            var val = deployment.expandPaths(value);

            switch (ensureDir)
            {
                case "dir":
                    iischef.utils.UtilsSystem.ensureDirectoryExists(val, true);
                    break;
                case "file":
                    iischef.utils.UtilsSystem.ensureDirectoryExists(val, false);
                    break;
            }

            // If this is a directory or file, make sure we properly quote when
            // writting the PHP.ini, because whitespaces in a path will break
            // most settings
            if (ensureDir == "dir" || ensureDir == "file")
            {
                if (!val.StartsWith("\""))
                {
                    val = "\"" + val + "\"";
                }
            }

            if (multivalue)
            {
                manager.UpdateOrCreateMultivalueDirective(key, val, section != null ? section : "AUTODEPLOY", comment, host);
            }
            else
            {
                manager.UpdateOrCreateDirective(key, val, section != null ? section : "AUTODEPLOY", comment, host);
            }

        }
    }
}
