﻿using System.Collections.Generic;
using System.Management.Automation;

namespace iischef.cmdlet
{
    /// <summary>
    /// Run service loop
    /// </summary>
    [Cmdlet(VerbsLifecycle.Invoke, "ChefAppLoop")]
    [OutputType(typeof(List<iischef.core.Configuration.InstalledApplication>))]
    public class ChefAppLopp : Cmdlet
    {
        protected override void ProcessRecord()
        {
            var app = ConsoleUtils.getApplicationForConsole();
            app.RunServiceLoop();
        }
    }
}
