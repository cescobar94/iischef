﻿using System.Management.Automation;

namespace iischef.cmdlet
{
    /// <summary>
    /// Use for "quick" deployment from paths.... used
    /// during testing builds.
    /// </summary>
    [Cmdlet(VerbsLifecycle.Invoke, "ChefAppDeployPath")]
    [OutputType(typeof(iischef.core.Deployment))]
    public class ChefAppDeployPath : Cmdlet
    {
        [Parameter(Position = 1, ValueFromPipelineByPropertyName = true, Mandatory = true)]
        public string Path { get; set; }

        [Parameter(Position = 2, ValueFromPipelineByPropertyName = true, Mandatory = true)]
        public string Id { get; set; }

        [Parameter(Position = 3, ValueFromPipelineByPropertyName = true)]
        [ValidateSet("link", "copy", "original")]
        public string MountStrategy { get; set; }

        [Parameter(Position = 4, ValueFromPipelineByPropertyName = true)]
        public string Options { get; set; }

        [Parameter(Position = 5, ValueFromPipelineByPropertyName = true)]
        public string inheritAppId { get; set; }

        protected override void ProcessRecord()
        {
            var app = ConsoleUtils.getApplicationForConsole();

            if (MountStrategy == null)
                MountStrategy = "link";

            var configuration = @"id: '{0}'
mount_strategy: '{2}'
downloader:
 type: 'localpath'
 path: '{1}'
inherit: '{3}'
";
            configuration = string.Format(configuration, Id, Path, MountStrategy, inheritAppId);

            var deployment = app.DeploySingleAppFromTextSettings(configuration, true);

            WriteObject(deployment);
        }
    }
}
