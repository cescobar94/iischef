﻿using System;
using YamlDotNet.Serialization;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.IO;
using iischef.utils;

namespace iischef.core.Configuration
{
    public class YamlConfigurationFile
    {
        /// <summary>
        /// Configuración general del deployment
        /// </summary>
        public JObject configuration { get; set; }

        protected string sourcePath { get; set; }

        public string getSourcePath()
        {
            return sourcePath;
        }

        public void parseFromFile(string path)
        {
            this.sourcePath = path;
            parseFromString(File.ReadAllText(path));
        }

        /// <summary>
        /// Parse the source config.
        /// </summary>
        /// <param name="source"></param>
        public void parseFromString(string source)
        {
            this.configuration = UtilsYaml.yamlToJtoken<JObject>(source);
        }

        public string getStringValue(string key, string default_value)
        {
            if (configuration[key] != null)
            {
                return configuration[key].ToString();
            }
            return default_value;
        }

        public int getIntValue(string key, int default_value = 0)
        {
            if (configuration[key] != null)
            {
                return Convert.ToInt32(configuration[key]);
            }
            return default_value;
        }

        public void merge(YamlConfigurationFile source)
        {
            var mergeSettings = new JsonMergeSettings
            {
                MergeArrayHandling = MergeArrayHandling.Merge
            };

            this.configuration.Merge(source.configuration);
        }
    }
}
