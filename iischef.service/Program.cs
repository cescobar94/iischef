﻿using System;
using System.ServiceProcess;
using System.Configuration.Install;
using System.Reflection;
using System.Diagnostics;


namespace iischef.service
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            try
            {
                _Main(args);
            }
            catch (Exception e)
            {
                Console.WriteLine("Error:" + e.Message);
                Console.WriteLine(e.StackTrace);
            }
        }

        static void _Main(string[] args)
        {
            Console.WriteLine("Done");

            if (Environment.UserInteractive)
            {
                string parameter = string.Concat(args);
                Console.WriteLine("arguments: " + parameter);
                switch (parameter)
                {
                    case "--install":
                        ManagedInstallerClass.InstallHelper(new[] { Assembly.GetExecutingAssembly().Location });
                        Console.WriteLine("Install successful");
                        break;
                    case "--uninstall":
                        ManagedInstallerClass.InstallHelper(new[] { "/u", Assembly.GetExecutingAssembly().Location });
                        Console.WriteLine("Uninstall successful");
                        break;
                    case "--run":
                        var app = new iischef.core.ApplicationService();
                        app.Start();
                        break;
                    case "--test":
                        ChefService service1 = new ChefService();
                        service1.TestStartupAndStop(args);
                        break;
                }
            }
            else
            {
                ServiceBase[] ServicesToRun;
                ServicesToRun = new ServiceBase[]
                {
                   new ChefService()
                };
                ServiceBase.Run(ServicesToRun);
            }


        }
    }
}
