﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Xml.Linq;

namespace iischef.utils
{
    public static class XMLtoLINQUtilsExtensions
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="xpath"></param>
        /// <param name="elem"></param>
        /// <returns></returns>
        public static XElement GetAndEnsureXpath(this XElement elem, string xpath)
        {
            List<string> parts = Regex.Split(xpath, "/").ToList();
            foreach (var p in parts)
            {
                if (!elem.Elements(p).Any())
                {
                    elem.Add(new XElement(p));
                }

                elem = elem.Elements(p).First();
            }

            return elem;
        }

        public static IEnumerable<XNode> NodeFlattened(this XElement node,
            Type NodeType, Func<XNode, bool> condition)
        {
            List<XNode> result = new List<XNode>();

            foreach (var s in node.Descendants())
                result.AddRange(NodeFlattened(s, NodeType, condition));

            foreach (var n in node.Nodes())
            {
                if (n.GetType() == NodeType
                    && condition(n))
                {
                    result.Add(n);
                }
            }

            return result;
        }

        public static XAttribute AttributeExtended(this XElement source, XName name)
        {
            return (from p in source.Attributes()
                    where p.Name.LocalName == name
                    select p).FirstOrDefault();
        }

        public static IEnumerable<XElement> ElementsExtended(this XElement source, XName name)
        {
            return (from p in source.Elements()
                    where p.Name.LocalName == name
                    select p);
        }

        public static IEnumerable<XElement> DescendantsExtended(this XElement source, XName name)
        {
            return (from p in source.Descendants()
                    where p.Name.LocalName.ToLower() == name.LocalName.ToLower()
                    select p);
        }

        public static XAttribute AtributeExtended(this XElement source, XName name)
        {
            return (from p in source.Attributes()
                    where p.Name.LocalName == name.LocalName.ToLower()
                    select p).FirstOrDefault();
        }

        public static IEnumerable<XElement> DescendantsExtended(this IEnumerable<XElement> source, XName name)
        {
            List<XElement> results = new List<XElement>();
            foreach (var r in source)
            {
                var r2 = (from p in r.Descendants()
                          where p.Name.LocalName.ToLower() == name.LocalName.ToLower()
                          select p);

                results.AddRange(r2);
            }
            return results;
        }

        public static IEnumerable<XElement> DescendantsAndSelfExtended(this XElement source, XName name)
        {
            var prefix = string.Empty;
            if (source.Document.Root.Attributes("xmlsn").Count() > 0)
                prefix = source.Document.Root.Attribute("xmlns").Value;


            return source.DescendantsAndSelf(name).Union(source.DescendantsAndSelf("{" + prefix + "}" + name.LocalName));
        }

        public static IEnumerable<XElement> ElementsExtended(this XDocument source, XName name)
        {
            return source.Root.ElementsExtended(name);
        }

        public static IEnumerable<XElement> DescendantsExtended(this XDocument source, XName name)
        {
            return source.Root.DescendantsExtended(name);
        }

        public static IEnumerable<XElement> DescendantsAndSelfExtended(this XDocument source, XName name)
        {
            return source.Root.DescendantsAndSelfExtended(name);
        }

        public static IEnumerable<XElement> ElementsAfterSelfExtended(this XElement source, XName name)
        {
            return source.ElementsAfterSelf(name).Union(source.ElementsAfterSelf("{http://www.w3.org/1999/xhtml}" + name));
        }

        public static IEnumerable<XElement> ElementsBeforeSelfExtended(this XElement source, XName name)
        {
            return source.ElementsBeforeSelf(name).Union(source.ElementsBeforeSelf("{http://www.w3.org/1999/xhtml}" + name));
        }


        public static string OutterXml(this XElement source)
        {
            var reader = source.CreateReader();
            reader.MoveToContent();
            return reader.ReadOuterXml();

        }

        public static string InnerXml(this XElement source)
        {
            var reader = source.CreateReader();
            reader.MoveToContent();
            return reader.ReadInnerXml();

        }

        public static XElement FirstParent(this XElement source, XName name)
        {
            XElement parent = source.Parent;
            while (parent != null)
            {
                if (parent.Name == name || parent.Name == "{http://www.w3.org/1999/xhtml}" + name)
                    return parent;
                parent = parent.Parent;
            }

            return null;
        }

        public static HtmlAgilityPack.HtmlNode FirstParent(this HtmlAgilityPack.HtmlNode source, XName name)
        {
            HtmlAgilityPack.HtmlNode parent = source.ParentNode;
            while (parent != null)
            {
                if (parent.NodeType == HtmlAgilityPack.HtmlNodeType.Element)
                {

                    if (parent.Name == name || parent.Name == "{http://www.w3.org/1999/xhtml}" + name)
                        return parent;
                }

                parent = parent.ParentNode;
            }

            return null;
        }

        public static HtmlAgilityPack.HtmlNode PreviousSiblingElement(this HtmlAgilityPack.HtmlNode source)
        {
            HtmlAgilityPack.HtmlNode parent = source.PreviousSibling;
            while (parent != null)
            {
                if (parent.NodeType == HtmlAgilityPack.HtmlNodeType.Element)
                    return parent;

                parent = parent.PreviousSibling;
            }

            return null;
        }

        public static string GetOutputHTML(this HtmlAgilityPack.HtmlDocument source)
        {
            StringWriter stream = new StringWriter();
            source.Save(stream);
            return stream.ToString();
        }

        public static HtmlAgilityPack.HtmlNode NextSiblingElement(this HtmlAgilityPack.HtmlNode source)
        {
            HtmlAgilityPack.HtmlNode parent = source.NextSibling;
            while (parent != null)
            {
                if (parent.NodeType == HtmlAgilityPack.HtmlNodeType.Element)
                    return parent;

                parent = parent.NextSibling;
            }

            return null;
        }

        public static HtmlAgilityPack.HtmlNode NextSiblingElementWithoutText(this HtmlAgilityPack.HtmlNode source)
        {
            HtmlAgilityPack.HtmlNode parent = source.NextSibling;
            while (parent != null)
            {
                if (parent.NodeType == HtmlAgilityPack.HtmlNodeType.Element)
                    return parent;

                if (parent.InnerText.Trim() != string.Empty)
                    return null;

                parent = parent.NextSibling;
            }

            return null;
        }

        public static IEnumerable<HtmlAgilityPack.HtmlNode> DescendantsElements(this HtmlAgilityPack.HtmlNode source, string name = null)
        {
            return source.Descendants().Where((i) => i.NodeType == HtmlAgilityPack.HtmlNodeType.Element && (name != null ? i.Name == name : true));
        }

        public static HtmlAgilityPack.HtmlNode FirstPreviousSibling(this HtmlAgilityPack.HtmlNode source, XName name)
        {
            HtmlAgilityPack.HtmlNode parent = source.PreviousSibling;
            while (parent != null)
            {
                if (parent.NodeType == HtmlAgilityPack.HtmlNodeType.Element)
                {

                    if (parent.Name == name || parent.Name == "{http://www.w3.org/1999/xhtml}" + name)
                        return parent;
                }

                parent = parent.PreviousSibling;
            }

            return null;
        }

        public static HtmlAgilityPack.HtmlNode FirstNextSibling(this HtmlAgilityPack.HtmlNode source, XName name)
        {
            HtmlAgilityPack.HtmlNode parent = source.NextSibling;
            while (parent != null)
            {
                if (parent.NodeType == HtmlAgilityPack.HtmlNodeType.Element)
                {

                    if (parent.Name == name || parent.Name == "{http://www.w3.org/1999/xhtml}" + name)
                        return parent;
                }

                parent = parent.NextSibling;
            }

            return null;
        }

        public static XElement FirstParentWithCondition(this XElement source, XName name, Func<XElement, bool> condition)
        {
            XElement parent = source.Parent;
            while (parent != null)
            {
                if (parent.Name == name || parent.Name == "{http://www.w3.org/1999/xhtml}" + name)
                {
                    if (condition(parent))
                        return parent;
                }

                parent = parent.Parent;
            }

            return null;
        }

    }

    public class XMLtoLINQutils
    {
        public void RemoveTags(string tagname, XElement element)
        {
            List<XElement> elems = new List<XElement>();
            foreach (XElement elem in element.DescendantsExtended(tagname))
                elems.Add(elem);
            foreach (XElement elem in elems)
                elem.Remove();
        }

        public void RemoveTagsWithCondition(string tagname, XElement element, Func<XElement, bool> condicion)
        {
            List<XElement> elems = new List<XElement>();
            foreach (XElement elem in element.DescendantsExtended(tagname))
            {
                if (condicion(elem))
                    elems.Add(elem);
            }

            foreach (XElement elem in elems)
                elem.Remove();
        }




        public void RemoveAllAttributes(string name, XElement parent)
        {
            var atts = (from s in parent.DescendantsAndSelf()
                        where s.Attribute(name) != null
                        select s.Attribute(name));
            foreach (XAttribute elem in atts)
            {
                elem.Remove();

            }

        }

        public void AddAttributeToElements(XElement parent, string type, string name, string value)
        {
            foreach (XElement elem in parent.DescendantsExtended(type))
            {
                if (elem.Attribute(name) != null)
                    elem.Attribute(name).Remove();

                XAttribute att = new XAttribute(name, value);
                elem.Add(att);
            }
        }

        public void AlterTagTextContents(XElement parent, string TagType, Func<string, string> modifier)
        {
            foreach (XElement elem in parent.DescendantsExtended(TagType))
            {
                var txt = from p in elem.DescendantNodes()
                          where p is XText
                          select p;
                if (txt.Count() != 0)
                {
                    XText el = ((XText)txt.First());
                    el.Value = modifier(el.Value);
                }
            }
        }




        public void ReplaceAllElementsWithContents(XElement elem, string type)
        {
            while (elem.DescendantsExtended(type).Count() > 0)
            {
                List<XElement> elems = new List<XElement>();
                foreach (XElement e in elem.DescendantsExtended(type))
                    elems.Add(e);
                foreach (XElement e in elems)
                    ReplaceElementWithContents(e);
            }
        }

        public void ReplaceAllElementsWithContentsCondition(XElement elem, string type, Func<XElement, bool> condicion)
        {
            while ((from p in elem.DescendantsExtended(type)
                    where condicion(p)
                    select p).Count() > 0)
            {

                List<XElement> elems = new List<XElement>();
                foreach (XElement e in elem.DescendantsExtended(type))
                    elems.Add(e);
                foreach (XElement e in elems)
                {
                    if (condicion(e))
                        ReplaceElementWithContents(e);
                }
            }
        }

        public void ReplaceTagsWithOtherTag(XElement elem, string originaltag, string destinationtag)
        {
            foreach (XElement e in elem.DescendantsAndSelfExtended(originaltag).ToList())
            {
                XElement destination = new XElement(destinationtag);
                List<XNode> nodos = new List<XNode>();
                foreach (XNode n in e.Nodes())
                    nodos.Add(n);
                foreach (XNode n in nodos)
                    destination.Add(n);
                e.AddBeforeSelf(destination);
                e.Remove();
            }
        }



        public void ReplaceElementWithContents(XElement elem)
        {
            try
            {
                List<XNode> nodos = new List<XNode>();
                foreach (XNode n in elem.Nodes())
                    nodos.Add(n);
                foreach (XNode n in nodos)
                    elem.AddBeforeSelf(n);
                elem.Remove();
            }
            catch { }
        }


        /// <summary>
        /// NOS DA EL ÚLTIMO PADRE DE LA CADENA DE HERENCIA QUE TIENE LOS MISMOS CONTENIDOS QUE EL NODO ACTUAL
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        public HtmlAgilityPack.HtmlNode LastEquivalentContentParent(HtmlAgilityPack.HtmlNode node)
        {
            var parent = node;

            while (node.ParentNode != null)
            {
                if (node.ParentNode.InnerText.ToLower().Trim() == node.InnerText.Trim())
                    parent = node.ParentNode;
            }

            return parent;
        }

        public bool HasChildrenStructureWithIndependentContent(HtmlAgilityPack.HtmlNode node)
        {
            var parent = node;

            foreach (var n in node.DescendantsElements())
            {
                if (n.InnerText.Trim() != node.InnerText.Trim())
                    return false;

                if (HasChildrenStructureWithIndependentContent(n))
                    return false;
            }

            return true;
        }

        public void ReplaceElementWithContents(HtmlAgilityPack.HtmlNode elem)
        {
            try
            {
                List<HtmlAgilityPack.HtmlNode> nodos = new List<HtmlAgilityPack.HtmlNode>();
                foreach (HtmlAgilityPack.HtmlNode n in elem.ChildNodes)
                    nodos.Add(n);
                foreach (HtmlAgilityPack.HtmlNode n in nodos)
                    elem.ParentNode.InsertBefore(n, elem);

                elem.Remove();
            }
            catch { }
        }

        public void ReplaceTagsWithOtherTag(HtmlAgilityPack.HtmlNode elem, string originaltag, string destinationtag)
        {
            foreach (HtmlAgilityPack.HtmlNode e in elem.Descendants(originaltag).ToList())
            {
                e.Name = destinationtag;
            }
        }


    }
}
