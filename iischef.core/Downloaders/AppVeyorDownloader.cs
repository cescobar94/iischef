﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using System.IO;
using iischef.utils;
using iischef.utils.AppVeyor;
using System.IO.Compression;

namespace iischef.core.Downloaders
{
    /// <summary>
    /// Download artifacts from AppVeyor
    /// </summary>
    public class AppVeyorDownloader : DownloaderInterface
    {
        protected AppVeyorDownloaderSettings Settings;

        protected iischef.utils.AppVeyor.Client Client;

        protected logger.LoggerInterface Logger;

        protected SystemConfiguration.EnvironmentSettings GlobalSettings;

        public AppVeyorDownloader(AppVeyorDownloaderSettings settings,
            SystemConfiguration.EnvironmentSettings globalSettings,
            logger.LoggerInterface logger)
        {
            this.Settings = settings;
            this.Logger = logger;
            this.GlobalSettings = globalSettings;
            this.Client = new utils.AppVeyor.Client(settings.apitoken, "https://ci.appveyor.com", logger);
        }

        /// <summary>
        /// Pull the next available artifact.
        /// </summary>
        /// <returns></returns>
        public string GetNextId(string buildId = null)
        {
            // Bring all successful jobs
            var lastBuilds = Client.FindLastSuccessfulBuilds(
                Settings.username,
                Settings.project,
                Settings.branch,
                buildVersionRequested: buildId,
                exp: Settings.publish_regex_filter,
                maxResults: 2);

            if (!lastBuilds.Any())
            {
                throw new Exception(
                    $"No suitable successful last build found for project {Settings.project} on branch {Settings.branch}");
            }

            // The build ID uniquely identifies this build.
            return lastBuilds.First().version;
        }

        /// <inheritdoc />
        /// <summary>
        /// Pull an artifact using it's ID
        /// </summary>
        /// <param name="version"></param>
        /// <returns></returns>
        public Artifact PullFromId(string version)
        {
            Artifact artifact = new Artifact
            {
                id = version,
                localPath = UtilsSystem.ensureDirectoryExists(
                    UtilsSystem.CombinePaths(GlobalSettings.GetArtifactTempPath(),
                        UtilsEncryption.getShortHash(version)), true),
                isRemote = true
            };

            // Use artifact temp path, or local system temporary directory.
            if (Directory.Exists(artifact.localPath))
            {
                UtilsSystem.deleteDirectory(artifact.localPath, false);
            }

            // Use the build version to pull the build information.
            Build build = Client.GetBuildFromVersion(version, Settings.username, Settings.project);
            Client.DownloadSingleArtifactFromBuild(build, Settings.artifact_regex, artifact.localPath, Logger);

            artifact.artifactSettings = new ArtifactSettings();
            artifact.artifactSettings.populateFromSettingsFile(artifact.localPath, this.Logger);

            if (String.IsNullOrWhiteSpace(artifact.artifactSettings.branch))
            {
                artifact.artifactSettings.branch = Convert.ToString(build.branch);
            }

            if (String.IsNullOrWhiteSpace(artifact.artifactSettings.commit_sha))
            {
                artifact.artifactSettings.commit_sha = Convert.ToString(build.commitId);
            }

            return artifact;
        }
    }
}
