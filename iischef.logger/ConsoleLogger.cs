﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Security.Cryptography;
using System.Security.Principal;
using System.IO;
using System.Net;

namespace iischef.logger
{
    /// <summary>
    /// Logger (system log) for the application.
    /// </summary>
    public class ConsoleLogger : BaseLogger, LoggerInterface
    {
        /// <summary>
        /// Start a system based logger.
        /// </summary>
        /// <param name="name">Name for the service log group</param>
        public ConsoleLogger()
        {

        }

        protected override void doWrite(string content, System.Diagnostics.EventLogEntryType type)
        {
            Console.Error.WriteLine(DateTime.Now.ToLongTimeString() + ": " + content.Replace("{", "{{").Replace("}", "}}"), type);
        }
    }
}
