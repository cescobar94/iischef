﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Management.Automation;
using iischef.core;

namespace iischef.cmdlet
{
    /// <summary>
    /// Sync an application using it's ID, or sync ALL app's!
    /// </summary>
    [Cmdlet(VerbsLifecycle.Invoke, "ChefAppSync")]
    [OutputType(typeof(iischef.core.Deployment))]
    public class ChefAppSyncId : Cmdlet
    {
        [Parameter(Position = 1, ValueFromPipelineByPropertyName = true)]
        public string Id { get; set; }

        protected override void ProcessRecord()
        {
            var app = ConsoleUtils.getApplicationForConsole();
            var logger = app.getLogger();
            var deployment = app.SyncInstalledApplication(Id);
            WriteObject(deployment);
        }
    }
}
