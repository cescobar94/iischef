﻿using iischef.core.Configuration;
using iischef.core.Storage;
using iischef.core.SystemConfiguration;
using iischef.logger;
using iischef.utils;
using Microsoft.Web.Administration;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Security.Cryptography.X509Certificates;
using System.Xml.Linq;

namespace iischef.core.IIS
{
    /// <summary>
    /// Deployer for IIS site
    /// </summary>
    class IISDeployer : DeployerBase, DeployerInterface
    {
        /// <summary>
        /// The settings for this deployer
        /// </summary>
        protected IISSettings IisSettings;

        /// <summary>
        /// We had some bugs that ended up in the same site
        /// redeployed hundreds of times collapsing the servers.
        /// 
        /// This is just a security measure in case future bug
        /// has similar fatal result.
        /// </summary>
        public const int CstMaxIisSites = 300;

        /// <summary>
        /// PATH for the settings of this deployer
        /// </summary>
        public const string CstSettingsWebroot = "iis.runtime.webroot.path";

        /// <summary>
        /// Service to provision SSL certificates
        /// </summary>
        protected SslCertificateProviderService SslProvisioningManager;

        /// <summary>
        /// The name that will be used for the IIS site used to mount the CDN
        /// </summary>
        protected string CstChefCndSiteName = "__chef_cdn";

        /// <summary>
        /// Initialize this deployer
        /// </summary>
        /// <param name="globalSettings"></param>
        /// <param name="deployerSettings"></param>
        /// <param name="deployment"></param>
        /// <param name="logger"></param>
        /// <param name="inhertApp"></param>
        public override void initialize(
            EnvironmentSettings globalSettings,
            JObject deployerSettings,
            Deployment deployment,
            LoggerInterface logger,
            InstalledApplication inhertApp)
        {
            base.initialize(globalSettings, deployerSettings, deployment, logger, inhertApp);

            this.IisSettings = deployerSettings.castTo<IISSettings>();
            this.IisSettings.InitializeDefaults();
        }

        protected string GetCdnWebConfigPath()
        {
            var basedir =
                UtilsSystem.ensureDirectoryExists(
                UtilsSystem.CombinePaths(GlobalSettings.GetDefaultApplicationStorage().path, "__chef_cdn"), true);

            var webconfigfilepath = UtilsSystem.CombinePaths(basedir, "web.config");

            // Si no hay un web.config plantilla, crearlo ahora.
            if (!File.Exists(webconfigfilepath))
            {
                File.WriteAllText(webconfigfilepath, @"
                        <configuration>
                          <system.webServer>
                            <rewrite>
                              <rules>
                              </rules>
                              <outboundRules>
                              </outboundRules>
                            </rewrite>
                          </system.webServer>
                        </configuration>
                        ");
            }

            return webconfigfilepath;
        }

        /// <summary>
        /// Make sure that we have not reached the limit
        /// of sites deployed in IIS
        /// </summary>
        protected void EnsureIisHasSpaceAndSiteNotRepeated()
        {
            using (ServerManager manager = new ServerManager())
            {
                var siteCount = manager.Sites.Count;

                if (siteCount > CstMaxIisSites)
                {
                    throw new Exception(
                        $"Maximum number of IIS sites ({siteCount}) reached. Cannot deploy new application.");
                }

                this.Logger.LogInfo(true,
                    $"Currently deployed {siteCount} of {CstMaxIisSites} maximum iis websites.");

                // Check that this site is not deployed more than N times (something is wrong!!!)
                int limit = 3;

                var matches = (from p in manager.Sites
                               where this.Deployment.IsShortId(p.Name)
                               select p);

                if (matches.Count() > limit)
                {
                    throw new Exception("Cannot redeploy site, other sites are using the same site pattern (possible bug), please cleanup stuck sites with prefix: " + this.Deployment.GetShortIdPrefix());
                }
            }
        }

        /// <summary>
        /// Not the best place but... make sure we have a daily
        /// backup of ApplicationHosts.config for the last 10 days.
        /// 
        /// This is not part of the deployment process or any resiliency
        /// meassure... it's just here to manually troubleshot iis in case
        /// of failure.
        /// </summary>
        protected void BackupApplicationHosts()
        {
            IntPtr wow64Value = IntPtr.Zero;

            UtilsIis.Wow64DisableWow64FsRedirection(ref wow64Value);

            try
            {
                var path = Path.GetFullPath(Environment.ExpandEnvironmentVariables(@"%windir%\System32\inetsrv\config\applicationHost.config"));

                // Today's zipped copy.
                var todaysCopy = Path.GetFullPath(Environment.ExpandEnvironmentVariables(
                    $@"%windir%\System32\inetsrv\config\{DateTime.Now:yyyyMMdd}_applicationHost.bak"));

                if (!File.Exists(todaysCopy))
                {
                    File.Copy(path, todaysCopy);
                }

                // Remove all backups that are more than 30 days old...
                var dir = Path.GetDirectoryName(path);
                foreach (var f in Directory.EnumerateFiles(dir))
                {
                    var fifo = new FileInfo(f);

                    if (fifo.Extension == ".bak" && fifo.Name.Contains("_applicationHost") && (DateTime.UtcNow - fifo.LastWriteTimeUtc).TotalDays > 10)
                    {
                        fifo.Delete();
                    }
                }
            }
            catch (Exception e)
            {
                this.Logger.LogException(e);
            }
            finally
            {
                UtilsIis.Wow64RevertWow64FsRedirection(wow64Value);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        protected void EnsureSslCertificateProviderInitialized()
        {
            if (this.SslProvisioningManager != null)
            {
                return;
            }

            this.SslProvisioningManager = new SslCertificateProviderService(
                this.Logger,
                // The application Id
                this.Deployment.appSettings.getId(),
                // Use the account's password for the certificates
                this.Deployment.getWindowsPassword());
        }

        /// <summary>
        /// 
        /// </summary>
        public void deploy()
        {
            Logger.LogInfo(true, "IIS Version {0}", UtilsIis.GetIisVersion());

            this.EnsureSslCertificateProviderInitialized();
            this.EnsureIisHasSpaceAndSiteNotRepeated();
            this.BackupApplicationHosts();

            // VER ESTO PARA REFERENCIAS SOBRE COMO MANIPULAR CONFIGURACIONES DEL POOL Y SITIOS
            // http://stackoverflow.com/questions/27116530/how-to-programmatically-set-app-pool-identity

            // Only one site per deployment...
            var siteName = Deployment.getShortId();

            // Use an empty temporary folder to mount the site on
            var tempDir = UtilsSystem.ensureDirectoryExists(
                Path.Combine(Deployment.appPath,
                Guid.NewGuid().ToString()),
                true);

            using (ServerManager manager = new ServerManager())
            {
                // Deploy the site, ideally NO site should be found
                // here as it indicates a failed previous deployment.
                var site = (from p in manager.Sites
                            where p.Name == siteName
                            select p).SingleOrDefault();

                if (site != null)
                {
                    throw new Exception("A site already exists with the current deployment id:" + siteName);
                }

                // Deploy the application pools
                foreach (var p in IisSettings.pools)
                {
                    DeployApplicationPool(manager, p.Value);
                }

                Logger.LogInfo(true, "IIS Deployer: adding temporary site: {0}", siteName);

                // Create and reset a new site on a phantom directory
                // only for web.config visibility purposes.
                site = manager.Sites.Add(siteName, tempDir, 80);

                ConfigureAuthenticationForPath(siteName, manager);

                // Remove all bindings, so that the site WONT start (yet)!!!
                site.Bindings.Remove(site.Bindings.First());

                site.LogFile.SetAttributeValue("Directory", UtilsSystem.ensureDirectoryExists(Deployment.logPath + "\\IISLogs", true));

                // Commit the changes
                manager.CommitChanges();
            }

            using (ServerManager manager = new ServerManager())
            {
                var site = (from p in manager.Sites
                            where p.Name == siteName
                            select p).Single();

                // Add a hidden binding to indentify the site uniquely for this deployment...
                var info = string.Join(":", "127.0.0.1", "999", "chefid." + Deployment.getShortId());
                var b = site.Bindings.Add(info, "http");

                // IMPORTANT NOTE: At some point we tried to have environment variable propagated
                // by nesting the actual root directory in a virutal directory itself and using
                // a rewrite rule in the root (see IIS/web.config). This turned out to BREAK many
                // existing applications. I.E. The rewrite module will only work once per request,
                // so any rewrite rule hosted in the virtual directory will not work. This break
                // for example DRUPAL that relies on url rewriting...
                var rootMount = IisSettings.getRootMount();
                var rootPool = IisSettings.getPoolForBinding(rootMount.pool);

                var rootApp = site.Applications.Single();

                // Set the proper root
                var webRootPath = UtilsSystem.CombinePaths(Deployment.appPath, IisSettings.getRootMount().path);
                Deployment.setSetting(CstSettingsWebroot, webRootPath);
                rootApp.VirtualDirectories.First().PhysicalPath = webRootPath;

                // Applications pools must match because the request will actually
                // be processed by the origin application pool...
                rootApp.ApplicationPoolName = GetPoolIdForDeployment(rootPool);

                Logger.LogInfo(true, "Deployed IIS root application with path '{0}' and pool '{1}' for site '{2}' ({3}).",
                    rootApp.Path, rootApp.ApplicationPoolName, site.Name, site.Id);

                // Deploy the remaining mounts
                var mounts = IisSettings.mounts.Where((i) => i.Value.root == false);
                foreach (var m in mounts)
                {
                    var mountPool = IisSettings.getPoolForBinding(m.Value.pool);

                    // Intentar primero con el path absoluto, si no existe tratarlo como relativo
                    // a la raíz del arterfacto.
                    var physicalpath = m.Value.path;
                    if (!Directory.Exists(physicalpath))
                    {
                        physicalpath = UtilsSystem.CombinePaths(Deployment.appPath, m.Value.path);
                    }

                    if (!Directory.Exists(physicalpath))
                    {
                        throw new Exception("Path for IIS mount does not exist: " + physicalpath);
                    }

                    if (!String.IsNullOrWhiteSpace(m.Value.mountpath))
                    {
                        if (!m.Value.mountpath.StartsWith("/"))
                        {
                            throw new Exception(
                                $"Invalid mount path '{m.Value.mountpath}'. IIS mount points must start with a slash.");
                        }
                    }

                    var app = site.Applications.Add(m.Value.mountpath, physicalpath);
                    var pool = IisSettings.getPoolForBinding(m.Value.pool);
                    app.ApplicationPoolName = GetPoolIdForDeployment(pool);

                    Logger.LogInfo(true, "Deployed IIS application with path '{0}' and pool '{1}' for site '{2}' ({3}).",
                        app.Path, app.ApplicationPoolName, site.Name, site.Id);

                    // preloadEnabled is only available on newer versions of IIS.
                    if (UtilsIis.GetIisVersion() >= Version.Parse("8.0"))
                    {
                        app.SetAttributeValue("preloadEnabled", m.Value.preloadEnabled);
                    }
                }

                foreach (var m in mounts)
                {
                    string basekey = $"deployers.{IisSettings.id}.mounts.{m.Value.id}";
                    Deployment.setRuntimeSetting(basekey + ".mountPath", m.Value.mountpath);
                }

                // Currently we have a stopped site... nothing else...
                manager.CommitChanges();
            }

            // Only do this for deployments where original copy is not affected
            var deploymentStrategy = Deployment.installedApplicationSettings.getApplicationMountStrategy();
            if (deploymentStrategy == ApplicationMountStrategy.Move
                || deploymentStrategy == ApplicationMountStrategy.Copy)
            {
                ConfigureNetFrameworkTempFolderForPath(siteName);
            }

            foreach (var b in IisSettings.bindings)
            {
                DeployBinding(null, b.Value, "root", true, true);
            }

            DeployCdn();

            UtilsSystem.deleteDirectory(tempDir, false, 6);
        }

        // This is what a directory based rewrite rule looks like....
        //
        //<configuration>
        //  <system.webServer>
        //    <rewrite>
        //      <rules>
        //                <rule name = "ReverseProxyInboundRule1" stopProcessing="true">
        //                    <match url = "^php_sabentisplus" />
        //                    < action type="Rewrite" url="http://local.sabentisplus.com/{C:2}/" />
        //                    <conditions>
        //                        <add input = "{PATH_INFO}" pattern="(^/php_sabentisplus)(.*)" />
        //                    </conditions>
        //                </rule>
        //      </rules>
        //    </rewrite>
        //  </system.webServer>
        //</configuration>

        /// <summary>
        /// CDN is a shared domain/dns binding accross ALL sites on a server
        /// so that they can be accesed on a sub-url such as sharecdn.com/mysite
        /// this is done like that because most CDN providers charge per-pull-zone
        /// binded to single source domain. In this way we can share many sites
        /// on a single pull-zone.
        /// </summary>
        protected void DeployCdn()
        {
            // Mucho cuidado: intentar configuar la CDN en un servidor
            // que no tiene instalados los paquetes necesarios (IIS-ARR)
            // puede dejar el IIS totalmente roto...
            // choco install iis-arr

            // Para empezar, si no solicitan CDN no hacemos nada...
            bool needsCnd = (IisSettings.cdn_bindings != null && IisSettings.cdn_bindings.Any())
                || (IisSettings.cdn_mounts != null && IisSettings.cdn_mounts.Any());

            if (!needsCnd)
            {
                return;
            }

            try
            {
                // Ensure that proxy is enabled and available at the IIS level.
                // This needs the IIS Application Request Routing extension.

                using (ServerManager manager = new ServerManager())
                {
                    var config = manager.GetApplicationHostConfiguration();

                    // Enable proxy functionality
                    ConfigurationSection proxySection = config.GetSection("system.webServer/proxy");
                    proxySection["enabled"] = true;

                    // Disable disk cache
                    ConfigurationElement cacheElement = proxySection.GetChildElement("cache");
                    cacheElement["enabled"] = false;

                    manager.CommitChanges();
                }
            }
            catch (Exception e)
            {
                throw new Exception("Could not enable server-wide proxy settings for CDN related functionality. Make sure that the chocolatey iis-arr package is installed.", e);
            }

            using (ServerManager manager = new ServerManager())
            {
                var webconfigfilepath = GetCdnWebConfigPath();

                var site = (from p in manager.Sites
                            where p.Name == CstChefCndSiteName
                            select p).FirstOrDefault();

                var cdnappdir = Path.GetDirectoryName(webconfigfilepath);

                if (site == null)
                {
                    site = manager.Sites.Add(CstChefCndSiteName, "http", "127.0.0.1:80:local.chefcdn.com", cdnappdir);
                }
                else
                {
                    site.Applications.First().VirtualDirectories.First().PhysicalPath = Path.GetDirectoryName(webconfigfilepath);
                }

                var crossdomainfilepath = UtilsSystem.CombinePaths(cdnappdir, "crossdomain.xml");
                File.WriteAllText(crossdomainfilepath, UtilsSystem.getEmbededResourceAsString(Assembly.GetExecutingAssembly(), "IIS.crossdomain.xml"));

                if (IisSettings.cdn_bindings != null)
                {
                    foreach (KeyValuePair<string, Binding> binding in IisSettings.cdn_bindings)
                    {
                        if (string.IsNullOrWhiteSpace(binding.Value.id))
                        {
                            throw new Exception("All CDN bindings must have an ID.");
                        }

                        DeployBinding(site, binding.Value, "cdn", true);
                    }
                }

                // List of currently deployed rules...
                var deployrulesid = Deployment.getSettingCollection<string>("iis.cdn.rules");

                if (IisSettings.cdn_mounts != null)
                {
                    // Verify that hostnames defined at cdn_mounts exists in bindings

                    CdnMount msgInfo = null;
                    try
                    {
                        foreach (var cdnmount in IisSettings.cdn_mounts.Values)
                        {
                            msgInfo = cdnmount;
                            IisSettings.bindings.Values.First(b => b.hostname == cdnmount.destination);
                        }
                    }
                    catch (InvalidOperationException e)
                    {
                        throw new Exception(String.Format("cdn_mounts: {0}  has a destination: {1} that does not exist in bindings", msgInfo.id, msgInfo.destination), e);
                    }

                    // Deploy

                    XDocument webcfg = XDocument.Parse(File.ReadAllText(webconfigfilepath));

                    foreach (var mount in IisSettings.cdn_mounts)
                    {
                        string mountname = Deployment.installedApplicationSettings.getId() + "_" + mount.Value.id;

                        Deployment.setSettingCollection("iis.cdn.rules", mountname, mountname);

                        XElement ruleselement = webcfg.Root.GetAndEnsureXpath("system.webServer/rewrite/rules");

                        var rule = (from p in ruleselement.Descendants("rule")
                                    where p.Attribute("name").Value == mountname
                                    select p).FirstOrDefault();

                        if (rule != null)
                        {
                            rule.Remove();
                        }

                        if (String.IsNullOrEmpty(mount.Value.destination))
                        {
                            throw new Exception("CDN mount destination cannot be empty.");
                        }

                        if (String.IsNullOrEmpty(mount.Value.id))
                        {
                            throw new Exception("CDN mount id cannot be empty.");
                        }

                        if (String.IsNullOrEmpty(mount.Value.match))
                        {
                            throw new Exception("CDN mount match cannot be empty.");
                        }

                        var match = String.Format("^{0}(.*)", mount.Value.match);

                        var exists = (from p in ruleselement.Descendants("match")
                                      where p.Attribute("url").Value == match
                                      select p).FirstOrDefault();

                        if (exists != null)
                        {
                            throw new Exception("Another application exists with a conflicting CDN match: " + match);
                        }

                        var template = string.Format(@"
                <rule name=""{2}"" stopProcessing=""true"" >
                    <match url = ""^{0}(.*)"" />
                    <action type = ""Rewrite"" url=""http://{1}/{{R:1}}"" />
                </rule>
                        ", mount.Value.match, mount.Value.destination, mountname);

                        // Do not let installation of two applicaitons with conflicting
                        // rewrite rules


                        // Now add from scratch...
                        ruleselement.Add(XElement.Parse(template));

                    }

                    File.WriteAllText(webconfigfilepath, webcfg.ToString());
                }

                manager.CommitChanges();
            }
        }

        /// <summary>
        /// Remove the CDN bindings
        /// </summary>
        protected void UninstallCdn()
        {
            using (ServerManager manager = new ServerManager())
            {
                var webconfigfilepath = GetCdnWebConfigPath();

                var site = (from p in manager.Sites
                            where p.Name == CstChefCndSiteName
                            select p).FirstOrDefault();

                if (site == null)
                {
                    return;
                }

                // Do not remove bindings, as these are shared among applications
                // and we cannot guarantee another APP is not using same bindings!
                //
                //foreach (Binding binding in iisSettings.cdn_bindings)
                //{
                //    remove??Binding(site, binding, true);
                //}

                XDocument webcfg = XDocument.Parse(File.ReadAllText(webconfigfilepath));

                var rules = Deployment.getSettingCollection<string>("iis.cdn.rules");

                foreach (var mountname in rules)
                {
                    var rule = (from p in webcfg.Descendants("rule")
                                where p.Attribute("name").Value == mountname.Key
                                select p).FirstOrDefault();

                    rule?.Remove();
                }

                File.WriteAllText(webconfigfilepath, webcfg.ToString());

                // This commit changes is not useful now, but was when doing binding removal.
                // Just leave it here for now...
                manager.CommitChanges();
            }
        }

        /// <summary>
        /// The default behaviour for IIS is to have the ANONYMOUS user (that is
        /// used for all request) identified as IUSR. When using FAST-CGI impersonation,
        /// we WANT all permissions to be based on the application pool identity...
        /// </summary>
        /// <param name="siteName">The site name (or path) i.e. site/virtualdirname</param>
        /// <param name="serverManager"></param>
        protected void ConfigureAuthenticationForPath(
            string siteName,
            ServerManager serverManager)
        {
            // fastCgi settings in IIS can only be set at the HOSTS level
            // we found no way to set this at a web.config level.
            Microsoft.Web.Administration.Configuration config = serverManager.GetApplicationHostConfiguration();
            ConfigurationSection section;

            // TODO: The type of authentication and it's configuration should be configurable here...
            // see https://www.iis.net/configreference/system.webserver/security/authentication
            section = config.GetSection("system.webServer/security/authentication/anonymousAuthentication", siteName);
            section["enabled"] = true;
            section["password"] = Deployment.getWindowsPassword();
            section["username"] = Deployment.windowsUsername;
        }

        /// <summary>
        /// Configure the .Net framework temp directory, which should live with the site deployment
        /// exclusively.
        ///
        /// Unfortunately this was the ONLY way to do this (modifying the actual application's web.config)
        /// because this setting cannot be declared at an applicationHosts level.
        /// </summary>
        /// <param name="siteName"></param>
        protected void ConfigureNetFrameworkTempFolderForPath(
            string siteName)
        {
            using (var serverManager = new ServerManager())
            {
                Site site = (from p in serverManager.Sites
                             where p.Name == siteName
                             select p).Single();

                foreach (var app in site.Applications)
                {
                    foreach (var dir in app.VirtualDirectories)
                    {
                        var path = UtilsSystem.ensureDirectoryExists(
                            Path.Combine(this.Deployment.runtimePath, "NetFrameworkTemporaryFiles"), true);

                        Microsoft.Web.Administration.Configuration config = serverManager.GetWebConfiguration(siteName, app.Path);

                        var section = config.GetSection("system.web/compilation");
                        section.SetAttributeValue("tempDirectory", path);
                    }
                }

                serverManager.CommitChanges();
            }
        }

        /// <summary>
        /// Originally we stored the site id in a fixed deployment settings:
        /// 
        ///   deployment.getSetting<long>("iis.siteid", 0);
        ///   
        /// Later on this was upgraded to used a hidden binding to identify
        /// ownership of a site:
        /// 
        ///  var info = string.Join(":", "127.0.0.1", "999", "chefid." + deployment.getShortId());
        ///  var b = site.Bindings.Add(info, "http");
        /// 
        /// </summary>
        /// <returns></returns>
        protected long? FindSiteIdForDeployment(ServerManager manager)
        {
            // Try to find the site using the hidden binding...
            return manager.Sites.Where((i) => i.Bindings.Any((j) => j.Host == "chefid." + Deployment.getShortId()))
                 .Select((i) => (long?)i.Id)
                 .FirstOrDefault();
        }

        /// <summary>
        /// Find the Site id for a deployment
        /// </summary>
        /// <returns></returns>
        protected long? FindSiteIdForDeployment()
        {
            using (ServerManager manager = new ServerManager())
            {
                return FindSiteIdForDeployment(manager);
            }
        }

        /// <summary>
        /// During hot switch deployments we need to stop previous
        /// site prior to undeploying...
        /// </summary>
        public void stop()
        {
            // Asssume no site here... nothing to stop.
            if (Deployment == null)
            {
                return;
            }

            var siteId = FindSiteIdForDeployment();

            using (ServerManager manager = new ServerManager())
            {
                // Deploy the site, ideally NO site should be found
                // here as it indicates a failed previous deployment.
                var site = (from p in manager.Sites
                            where p.Id == siteId
                            select p).SingleOrDefault();

                if (site == null)
                {
                    return;
                }

                UtilsAppPool.WebsiteAction(site.Name, AppPoolActionType.Stop);
            }
        }

        /// <summary>
        /// Executed after previous deployment is stopped.
        ///
        /// Bindings have exclusive in a single IIS instalation so
        /// it is very important that previous deployment is stopped
        /// before we try to deploy thebindings.
        /// </summary>
        public override void beforeDone()
        {
            using (ServerManager manager = new ServerManager())
            {
                var siteId = FindSiteIdForDeployment(manager);
                var site = manager.Sites.Where((i) => i.Id == siteId).SingleOrDefault();

                foreach (var b in this.IisSettings.bindings)
                {
                    DeployBinding(site, b.Value, "root", true);
                }

                manager.CommitChanges();
            }
        }

        public override void done()
        {
            // This here is a workaround for an ISSUE in certificate bindings
            // in IIS:
            // If you remove a binding from a stopped site (or delete the site)
            // and that site has an exact combination of port/hostname/certificate
            // then IIS will internally delete ALL bindings for that combination.
            // We are experiencing thicros when the previous deployment is removed
            // from IIS, so we simply redeploy all bindings here...

            using (ServerManager manager = new ServerManager())
            {
                var siteId = FindSiteIdForDeployment(manager);
                var site = manager.Sites.SingleOrDefault(i => i.Id == siteId);

                foreach (var b in this.IisSettings.bindings)
                {
                    DeployBinding(site, b.Value, "root", true);
                }

                manager.CommitChanges();
            }
        }

        public void deploySettings(string jsonSettings,
            string jsonSettingsArray,
            RuntimeSettingsReplacer replacer)
        {

        }

        /// <summary>
        /// Get the site name from it's ID in IIS.
        /// </summary>
        /// <param name="siteId"></param>
        /// <returns></returns>
        protected string FindSiteNameFromSiteId(long siteId)
        {
            using (ServerManager manager = new ServerManager())
            {
                // Deploy the site, ideally NO site should be found
                // here as it indicates a failed previous deployment.
                var site = (from p in manager.Sites
                            where p.Id == siteId
                            select p.Name).SingleOrDefault();

                return site;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void start()
        {
            if (Deployment == null)
            {
                return;
            }

            long siteId = FindSiteIdForDeployment().Value;
            string siteName = FindSiteNameFromSiteId(siteId);

            UtilsAppPool.WebsiteAction(siteName, AppPoolActionType.Start);
        }

        /// <summary>
        /// Remove a deployed site
        /// </summary>
        public void undeploy(bool isUninstall = false)
        {
            this.EnsureSslCertificateProviderInitialized();

            // Cannot cleanup and undeploy environment.
            if (Deployment == null)
            {
                return;
            }

            // Only one site per deployment...
            long? siteId = FindSiteIdForDeployment();
            var applicationPools = Deployment.getSettingCollection<string>("iis.appplicationpools");

            List<string> userProfilesDirsToRemove = new List<string>();

            using (ServerManager manager = new ServerManager())
            {
                // Deploy the site, ideally NO site should be found
                // here as it indicates a failed previous deployment.
                var site = (from p in manager.Sites
                            where (p.Id == siteId)
                            select p).SingleOrDefault();

                if (site != null)
                {
                    Logger.LogInfo(true, "IIS Deployer: undeploy removing site with name {0} and id {1}", site.Name, site.Id);

                    // Sometimes the site fails with 
                    // Exception Type: 'System.Runtime.InteropServices.COMException
                    // ExceptionMessage: The object identifier does not represent a valid object. (Exception from HRESULT: 0x800710D8)
                    // Stack Trace:   at Microsoft.Web.Administration.Interop.IAppHostProperty.get_Value()
                    // at Microsoft.Web.Administration.ConfigurationElement.GetPropertyValue(IAppHostProperty property)
                    // at Microsoft.Web.Administration.Site.get_State()
                    try
                    {
                        if (site.Bindings.Any() && site.State != ObjectState.Stopped)
                        {
                            UtilsAppPool.WebsiteAction(site.Name, AppPoolActionType.Stop);
                        }
                    }
                    catch
                    {
                        // ignored
                    }

                    manager.Sites.Remove(site);
                }

                // Remove application pools
                foreach (var poolName in applicationPools.Keys)
                {
                    var pool = manager.ApplicationPools.FirstOrDefault(i => i.Name == poolName);

                    if (pool == null)
                    {
                        continue;
                    }

                    Logger.LogInfo(true, "IIS Deployer: undeploy removing application pool {0}", pool.Name);

                    var dir = IISUtils.findStorageFolderForAppPoolWithDefaultIdentity(pool);

                    if (dir != null)
                    {
                        userProfilesDirsToRemove.Add(dir);
                    }

                    manager.ApplicationPools.Remove(pool);
                }

                // Currently we have a stopped site... nothing else...
                manager.CommitChanges();
            }

            // Now remove storage for the users of the application pools...
            // this is "delicate"....
            foreach (var dir in userProfilesDirsToRemove)
            {
                // Use utils system here...
                UtilsSystem.deleteDirectory(dir);
            }

            if (isUninstall)
            {
                UninstallCdn();
            }
        }

        /// <summary>
        /// The .well-know directory is in a centrlized location and shared by
        /// all websites, in order to support multiple web-head deployments
        /// to properly work...
        /// </summary>
        protected string InitializeSharedWellKnownDirectory()
        {
            var dir =
                Path.Combine(this.GlobalSettings.GetDefaultContentStorage().path, "letsencrypt", ".well-known", "acme-challenge");

            UtilsSystem.ensureDirectoryExists(dir, true);

            return dir;
        }

        /// <summary>
        /// Deploy a binding
        /// </summary>
        /// <param name="site"></param>
        /// <param name="binding"></param>
        /// <param name="type"></param>
        /// <param name="overwrite"></param>
        /// <param name="prepareSsl">If set to true, this will not deploy the bindings, but prepare SSL certificates.</param>
        protected void DeployBinding(
            Site site,
            Binding binding,
            string type,
            bool overwrite = false,
            bool prepareSsl = false)
        {
            string hostname = Deployment.getSettingsReplacer().doReplace(binding.hostname);

            if (!prepareSsl)
            {
                if (overwrite)
                {
                    var olds = site.Bindings.Where((i) => i.Host.ToLower() == hostname.ToLower()).ToList();

                    foreach (var old in olds)
                    {
                        site.Bindings.Remove(old);
                    }
                }
            }

            NetworkInterface networkinterface = null;

            // Find the address for the binding, or directly an IP
            string address;
            if (binding.@interface == "*")
            {
                address = "*";
            }
            else
            {
                IPAddress ipAddress;
                if (!IPAddress.TryParse(binding.@interface, out ipAddress))
                {
                    networkinterface = GlobalSettings.FindEndpointAddress(binding.@interface);

                    if (!IPAddress.TryParse(networkinterface.ip, out ipAddress))
                    {
                        throw new Exception("Specified interface does not match any declared adapter and is not a valid IP address: " + binding.@interface);
                    }
                }

                address = ipAddress.ToString();
            }

            string bindingProtocol = binding.type;
            if (string.IsNullOrWhiteSpace(bindingProtocol))
            {
                bindingProtocol = "http";
            }

            var info = string.Join(":", address, binding.port, hostname);
            var infoWithoutSsl = string.Join(":", address, "80", hostname);

            Microsoft.Web.Administration.Binding siteBinding = null;

            // The local needs a hosts file mapping to work... we map the hosts
            // file now because the SSL validation might need this ASAP.
            if (binding.addtohosts || (networkinterface != null && networkinterface.forcehosts))
            {
                this.Logger.LogInfo(true, "Added HOSTS binding {0} {1}", address, hostname);
                UtilsHosts.AddHostsMapping(address, hostname, Deployment.getAppSettings().getId());

                // TODO: These must be removed on every deployment, otherwise
                // they will accumulate. Carefull because removing them might
                // break local usage of the bindings.
                Deployment.setSettingCollection("hosts", info, info);
            }

            bool isSsl = string.Equals(binding.type, "https", StringComparison.CurrentCultureIgnoreCase);

            // If we are in SSL prepare mode and this is not ssl, simply skip.
            if (prepareSsl && !isSsl)
            {
                return;
            }

            // SSL Setup
            if (isSsl)
            {
                if (binding.ssl_letsencrypt)
                {
                    // Preparation only ensure that a valid SSL certificate has been provisioned
                    if (prepareSsl)
                    {
                        // This will throw an exception if it is not able to provision the certificate or find an existing
                        // certificate in the central store
                        this.SslProvisioningManager.ProvisionCertificateInIis(
                            this.InitializeSharedWellKnownDirectory(),
                            binding.hostname,
                            "info@sabentis.com",
                            infoWithoutSsl);

                        return;
                    }

                    siteBinding = site.Bindings.Add(info, null, null, SslFlags.CentralCertStore);
                }
                else
                {
                    // Here we asume that the given certificate is valid and not expired
                    X509Certificate2 cert = null;
                    X509Store store = new X509Store(StoreName.My, StoreLocation.LocalMachine);
                    store.Open(OpenFlags.OpenExistingOnly | OpenFlags.ReadWrite);
                    cert = store.Certificates.OfType<X509Certificate2>()
                        .Where(x => x.FriendlyName == binding.ssl_certificate_friendly_name).FirstOrDefault();

                    if (cert == null)
                    {
                        throw new Exception("SSL Binding will be skipped. Could not find SSL certificate with friendly name: " + binding.ssl_certificate_friendly_name);
                    }

                    if (prepareSsl)
                    {
                        return;
                    }

                    siteBinding = site.Bindings.Add(info, cert.GetCertHash(), store.Name, SslFlags.Sni);
                }
            }
            else
            {
                siteBinding = site.Bindings.Add(info, bindingProtocol);
            }

            this.Logger.LogInfo(true, "Site biding: " + siteBinding);

            string baseurl = $"{bindingProtocol}://{hostname}";

            if (binding.port != 80)
            {
                baseurl += ":" + binding.port;
            }

            string basekey = $"deployers.{IisSettings.id}.bindings.{type}.{binding.id}";

            Deployment.setRuntimeSetting(basekey + ".url", baseurl);
            Deployment.setRuntimeSetting(basekey + ".interface_alias", binding.@interface);
            Deployment.setRuntimeSetting(basekey + ".port", Convert.ToString(binding.port));
            Deployment.setRuntimeSetting(basekey + ".hostname", hostname);
        }

        /// <summary>
        /// The name to use for the application pool
        /// </summary>
        /// <param name="pool"></param>
        /// <returns></returns>
        protected string GetPoolIdForDeployment(Pool pool)
        {
            return Deployment.getShortId() + "_" + pool.id;
        }

        /// <summary>
        /// Deploy a single application pool. Does not commit changes
        /// to the server manager.
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="pool"></param>
        protected void DeployApplicationPool(ServerManager manager, Pool pool)
        {
            // Must be careful with application pools. Creating unique pool
            // names will create a unique identity (with it's own user profile)
            // every time. This can consume disk space in the users profile folder
            // (unless we manually remove the old profiles...) but at the same time
            // it might NOT be advisable to reuse the same pool between deployments
            // as they might not have the same setup and thus doing a rollback
            // of the deployment might not work...

            ApplicationPool p = null;

            string poolname = GetPoolIdForDeployment(pool);

            Deployment.setSettingCollection("iis.appplicationpools", poolname, poolname);

            p = manager.ApplicationPools.Where((i) => i.Name == poolname).FirstOrDefault();

            if (p == null)
            {
                p = manager.ApplicationPools.Add(poolname);
                Logger.LogInfo(true, "Created new application pool '{0}'", p.Name);
            }
            else
            {
                Logger.LogInfo(true, "Found existing application pool '{0}'", p.Name);
            }

            // El PHP es de 32 bits!!
            p.Enable32BitAppOnWin64 = pool.Enable32BitAppOnWin64;

            // Arranque automático.
            p.AutoStart = pool.AutoStart;

            // Modo integrado (en verdad da igual...)
            switch (pool.ManagedPipelineMode)
            {
                case "Integrated":
                case null:
                    p.ManagedPipelineMode = ManagedPipelineMode.Integrated;
                    break;
                case "Classic":
                    p.ManagedPipelineMode = ManagedPipelineMode.Classic;
                    break;
                default:
                    throw new Exception("Unkown pipeline mode:" + pool.ManagedPipelineMode);
            }

            // Managed runtime version needs to be one of the following...
            var admitedruntimeversions = new List<string>() {
                "v4.0",
                "",
                "v2.0"
            };

            if (!admitedruntimeversions.Contains(pool.ManagedRuntimeVersion))
            {
                throw new Exception("Requested pool ManagedRuntimeVersion not supported: " + pool.ManagedRuntimeVersion + ". Use one of the following: " + String.Join(" | ", admitedruntimeversions));
            }

            p.ManagedRuntimeVersion = pool.ManagedRuntimeVersion;

            // Autostart was replaced by startmode in newer
            // versions of IIS. Use SetAttributeValue instead of
            // the dll's attribute because at runtime it crashes
            // on old versions of IIS, even if the call is NOT made.
            if (UtilsIis.GetIisVersion() >= Version.Parse("8.0"))
            {
                switch (pool.StartMode)
                {
                    case "OnDemand":
                    case null:
                        p.SetAttributeValue("StartMode", iischef.core.IIS.StartMode.OnDemand);
                        break;
                    case "AlwaysRunning":
                        p.SetAttributeValue("StartMode", iischef.core.IIS.StartMode.AlwaysRunning);
                        break;
                    default:
                        throw new Exception("Unkown StartMode mode:" + pool.StartMode);
                }
            }

            p.ProcessModel.LoadUserProfile = pool.LoadUserProfile;

            switch (pool.IdentityType)
            {
                case "ChefApp":
                case null:
                    p.ProcessModel.IdentityType = ProcessModelIdentityType.SpecificUser;
                    p.ProcessModel.UserName = Deployment.windowsUsername;
                    p.ProcessModel.Password = Deployment.getWindowsPassword();
                    break;
                case "ApplicationPoolIdentity":
                    p.ProcessModel.IdentityType = ProcessModelIdentityType.ApplicationPoolIdentity;
                    break;
                case "NetworkService":
                    p.ProcessModel.IdentityType = ProcessModelIdentityType.NetworkService;
                    break;
                case "LocalService":
                    p.ProcessModel.IdentityType = ProcessModelIdentityType.LocalService;
                    break;
                case "LocalSystem":
                    p.ProcessModel.IdentityType = ProcessModelIdentityType.LocalSystem;
                    break;
                default:
                    if (pool.IdentityType.StartsWith("SpecificUser:"))
                    {
                        var startIndex = pool.IdentityType.IndexOf(":");
                        var uname = pool.IdentityType.Substring(startIndex, pool.IdentityType.Length - startIndex);

                        var identity = (from a in GlobalSettings.accounts
                                        where a.id == uname
                                        select a).FirstOrDefault();

                        if (identity == null)
                        {
                            throw new Exception("Account identity not found: " + uname);
                        }

                        p.ProcessModel.IdentityType = ProcessModelIdentityType.SpecificUser;
                        p.ProcessModel.UserName = identity.username;
                        p.ProcessModel.Password = identity.password;
                    }
                    else
                    {
                        throw new Exception("Identity type not supported: " + pool.IdentityType);
                    }

                    break;
            }

            // Use a defualt configuration where a pool is never allowed to exceed > 90%
            // of total machine usage!
            if (pool.CpuLimitPercent == 0)
            {
                pool.CpuLimitPercent = 95;
                pool.CpuLimitAction = ProcessorAction.ThrottleUnderLoad.ToString();
            }

            // Deploy CPU limits
            if (pool.CpuLimitPercent > 0)
            {
                if (pool.CpuLimitPercent > 95)
                {
                    throw new Exception($"Cpu pool limit percent cannot exceed 95%. Requested {pool.CpuLimitPercent}%");
                }

                p.Cpu.Limit = pool.CpuLimitPercent * 1000;

                if (string.Equals(pool.CpuLimitAction, ProcessorAction.KillW3wp.ToString()))
                {
                    p.Cpu.Action = ProcessorAction.KillW3wp;
                }
                else if (string.Equals(pool.CpuLimitAction, ProcessorAction.NoAction.ToString()))
                {
                    p.Cpu.Action = ProcessorAction.NoAction;
                }
                else if (string.Equals(pool.CpuLimitAction, ProcessorAction.Throttle.ToString()))
                {
                    p.Cpu.Action = ProcessorAction.Throttle;
                }
                else if (string.Equals(pool.CpuLimitAction, ProcessorAction.ThrottleUnderLoad.ToString()))
                {
                    p.Cpu.Action = ProcessorAction.ThrottleUnderLoad;
                }
                else
                {
                    throw new Exception($"Unrecognized CpuLimitAction: '{pool.CpuLimitAction}'");
                }
            }
        }

        /// <summary>
        /// Global cleanup / cron
        /// </summary>
        public override void cron()
        {
            // TODO: Running this here is to ensure SSL certificates
            // provisioned by chef are up to date.
            return;

            // Make sure that we have up-to-date certificates!
            using (ServerManager manager = new ServerManager())
            {
                var siteId = FindSiteIdForDeployment(manager);
                var site = manager.Sites.Where((i) => i.Id == siteId).SingleOrDefault();

                foreach (var b in this.IisSettings.bindings)
                {
                    DeployBinding(site, b.Value, "root", true);
                }

                // This is here ONLY to re-provision certificates,
                // the APP neds NO CHANGES.

                // manager.CommitChanges();
            }
        }

        /// <summary>
        /// Global cleanup / cron
        /// </summary>
        public override void cleanup()
        {
            using (ServerManager manager = new ServerManager())
            {
                bool changed = false;

                foreach (var site in manager.Sites.ToList())
                {
                    if (!this.Deployment.IsShortId(site.Name))
                    {
                        continue;
                    }

                    // If it's a short ID for this site,
                    // but not the current deployment...
                    // needs to be removed
                    if (site.Id == this.FindSiteIdForDeployment(manager))
                    {
                        continue;
                    }

                    manager.Sites.Remove(site);

                    // Remove!!!
                    foreach (var app in site.Applications.ToList())
                    {
                        var pool = manager.ApplicationPools
                            .Where((i) => i.Name == app.ApplicationPoolName).FirstOrDefault();

                        if (pool != null)
                        {
                            manager.ApplicationPools.Remove(pool);
                        }
                    }

                    changed = true;
                }

                // Now let's just dynamite unused application pools
                // that start with this site's prefix (it would even
                // be safe to dynamite ALL application pools that have no site...)
                foreach (var pool in manager.ApplicationPools.ToList())
                {
                    if (!pool.Name.StartsWith(this.Deployment.GetShortIdPrefix()))
                    {
                        continue;
                    }

                    bool siteFound = false;

                    foreach (var s in manager.Sites)
                    {
                        foreach (var app in s.Applications)
                        {
                            if (app.ApplicationPoolName == pool.Name)
                            {
                                siteFound = true;
                                break;
                            }
                        }

                        if (siteFound)
                        {
                            break;
                        }
                    }

                    if (!siteFound)
                    {
                        changed = true;
                        manager.ApplicationPools.Remove(pool);
                    }
                }

                if (changed)
                {
                    manager.CommitChanges();
                }
            }
        }

        public void sync()
        {

        }
    }
}
